module apworkshop

go 1.16

require (
	github.com/alecthomas/template v0.0.0-20190718012654-fb15b899a751
	github.com/apulis/go-business v0.0.0
	github.com/apulis/sdk/fs-go-client v0.0.0
	github.com/apulis/sdk/go-utils v0.0.0
	github.com/coreos/bbolt v1.3.2 // indirect
	github.com/coreos/etcd v3.3.13+incompatible // indirect
	github.com/coreos/pkg v0.0.0-20180928190104-399ea9e2e55f // indirect
	github.com/gin-contrib/cors v1.3.1
	github.com/gin-contrib/sessions v0.0.3
	github.com/gin-gonic/gin v1.7.2
	github.com/go-playground/validator/v10 v10.4.1
	github.com/gofrs/uuid v3.2.0+incompatible
	github.com/google/uuid v1.3.0
	github.com/gorilla/websocket v1.4.2 // indirect
	github.com/jinzhu/gorm v1.9.16
	github.com/mitchellh/mapstructure v1.4.1
	github.com/prometheus/tsdb v0.7.1 // indirect
	github.com/sirupsen/logrus v1.8.1
	github.com/spf13/viper v1.8.1
	github.com/streadway/amqp v1.0.0
	github.com/swaggo/gin-swagger v1.3.0
	github.com/swaggo/swag v1.7.0
	github.com/tmc/grpc-websocket-proxy v0.0.0-20190109142713-0ad062ec5ee5 // indirect
	github.com/urfave/cli/v2 v2.3.0
	gopkg.in/yaml.v2 v2.4.0
	gorm.io/driver/mysql v1.1.0
	gorm.io/driver/postgres v1.1.0
	gorm.io/gorm v1.21.9
)

replace github.com/apulis/go-business v0.0.0 => ./deps/go-business

replace github.com/apulis/sdk/go-utils v0.0.0 => ./deps/go-utils

replace github.com/apulis/sdk/fs-go-client v0.0.0 => ./deps/fs-go-client
