// Copyright 2020 Apulis Technology Inc. All rights reserved.

package main

import (
	"flag"
	"fmt"
	"github.com/apulis/sdk/go-utils/storage"
	"github.com/apulis/sdk/go-utils/storage/comm"
)

// ./test_get -src="FILE:///data1/123/helloworld.txt" -dst="/data/helloworld.txt"
// ./test_get -src="S3://kefeng-test/123/helloworld.txt" -dst="/data/helloworld.txt"
func main() {
	var srcPathWithSchema = flag.String("src", "", "source file local path")
	var dstLocalPath = flag.String("dst", "", "dst resource schema")
	flag.Parse()

	schemaType, err := comm.GetSchema(*srcPathWithSchema)
	if err != nil {
		fmt.Printf("get schema failed, err = %v\n", err)
		return
	}

	s, err := storage.New(schemaType)
	if err != nil {
		fmt.Printf("create storage failed, err = %v\n", err)
		return
	}

	err = s.Handler.Init(comm.HandlerCfg{
		Debug: false,
	})
	if err != nil {
		fmt.Printf("init storage failed, err = %v\n", err)
		return
	}

	err = s.Handler.GetFile(*srcPathWithSchema, *dstLocalPath, 0644)
	if err != nil {
		fmt.Printf("StoreFile failed, err = %v\n", err)
		return
	}

	fmt.Printf("get file succ, dst file = %s\n", *dstLocalPath)
	return
}
