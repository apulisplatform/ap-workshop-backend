package main

import (
	"fmt"
	"github.com/apulis/sdk/go-utils/broker"
	_ "github.com/apulis/sdk/go-utils/broker/kafka"
	"github.com/apulis/sdk/go-utils/broker/rabbitmq"
	"os"
	"time"
)

var (
	addr = "amqp://admin:apulis123@192.168.1.132:5672/"
)

func main() {
	b := rabbitmq.NewBroker(
		broker.Addrs(addr),
		rabbitmq.ExchangeName("demo"),
	)
	if err := b.Connect(); err != nil {
		fmt.Println(err.Error())
		os.Exit(-1)
	}

	go func() {
		_, err := b.Subscribe("topic_1", func(event broker.Event) error {
			fmt.Println(event.Message().Body)
			return  nil
		}, rabbitmq.DurableQueue())
		if err != nil {
			fmt.Println(err.Error())
			os.Exit(-1)
		}
	}()

	time.Sleep(1 * time.Second)

	for i := 0; i < 5; i++ {
		if err := b.Publish("topic_1", &broker.Message{
			Header: map[string]string{"auth": "ok"},
			Body: []byte("this is from test"),
		}); err != nil {
			fmt.Println(err.Error())
		}
	}

	select {
	}
}
