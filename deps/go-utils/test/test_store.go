// Copyright 2020 Apulis Technology Inc. All rights reserved.

package main

import (
	"flag"
	"fmt"
	"github.com/apulis/sdk/go-utils/storage"
	"github.com/apulis/sdk/go-utils/storage/comm"
)

// ./test_store -src="/tmp/helloworld.txt" -dst="FILE:///data1/123/helloworld.txt"
// ./test_store -src="/tmp/helloworld.txt" -dst="S3://kefeng-test/123/helloworld.txt"
func main() {
	var srcPath = flag.String("src", "", "source file local path")
	var dstPathWithSchema = flag.String("dst", "", "dst resource schema")
	flag.Parse()

	schemaType, err := comm.GetSchema(*dstPathWithSchema)
	if err != nil {
		fmt.Printf("get schema failed, err = %v\n", err)
		return
	}

	s, err := storage.New(schemaType)
	if err != nil {
		fmt.Printf("create storage failed, err = %v\n", err)
		return
	}

	err = s.Handler.Init(comm.HandlerCfg{
		Debug: false,
	})
	if err != nil {
		fmt.Printf("init storage failed, err = %v\n", err)
		return
	}

	err = s.Handler.StoreFile(*srcPath, *dstPathWithSchema, 0644)
	if err != nil {
		fmt.Printf("StoreFile failed, err = %v\n", err)
		return
	}

	dstFile, err := comm.GetSchemaFile(*dstPathWithSchema)
	fmt.Printf("store file succ, dst file = %s\n", dstFile)
	return
}
