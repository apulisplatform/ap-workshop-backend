// Copyright 2020 Apulis Technology Inc. All rights reserved.

package main

import (
	"bufio"
	"flag"
	"fmt"
	"github.com/apulis/sdk/go-utils/storage"
	"github.com/apulis/sdk/go-utils/storage/comm"
	"io/ioutil"
)

// ./test_open -src="FILE:///data1/123/helloworld.txt" -dst="/data/helloworld.txt"
// ./test_open -src="S3://kefeng-test/123/helloworld.txt" -dst="/data/helloworld.txt"
func main() {
	var srcPathWithSchema = flag.String("src", "", "source file local path")
	var dstLocalPath = flag.String("dst", "", "dst resource schema")
	flag.Parse()

	schemaType, err := comm.GetSchema(*srcPathWithSchema)
	if err != nil {
		fmt.Printf("get schema failed, err = %v\n", err)
		return
	}

	s, err := storage.New(schemaType)
	if err != nil {
		fmt.Printf("create storage failed, err = %v\n", err)
		return
	}

	err = s.Handler.Init(comm.HandlerCfg{
		Debug: false,
	})
	if err != nil {
		fmt.Printf("init storage failed, err = %v\n", err)
		return
	}

	f, err := s.Handler.OpenFile(*srcPathWithSchema)
	if err != nil {
		fmt.Printf("OpenFile failed, err = %v\n", err)
		return
	}
	defer f.Close()

	reader := bufio.NewReader(f)
	content, err := ioutil.ReadAll(reader)
	if err != nil {
		fmt.Printf("ReadAll failed, err = %v\n", err)
		return
	}

	err = ioutil.WriteFile(*dstLocalPath, content, 0644)
	if err != nil {
		fmt.Printf("WriteFile failed, err = %v\n", err)
		return
	}

	fmt.Printf("read file succ, dst file = %s\n", *dstLocalPath)
	return
}
