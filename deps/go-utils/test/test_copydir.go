// Copyright 2020 Apulis Technology Inc. All rights reserved.

package main

import (
	"flag"
	"fmt"
	"github.com/apulis/sdk/go-utils/storage"
	"github.com/apulis/sdk/go-utils/storage/comm"
)

// ./test_copydir -src="FILE:///data1/test1" -dst="FILE:///data1/test2"
// ./test_copydir -src="/data1/test1" -dst="FILE:///data1/test2"
// ./test_copydir -src="FILE:///data1/test1" -dst="/data1/test2"
func main() {
	var srcDir = flag.String("src", "", "source path")
	var dstDir = flag.String("dst", "", "dst path")
	flag.Parse()

	var err error
	var schemaType string
	srcRemote := false
	dstRemote := false

	if comm.IsRemoteResource(*srcDir) {
		schemaType, err = comm.GetSchema(*srcDir)
		if err != nil {
			fmt.Printf("get schema failed, err = %v\n", err)
			return
		}
		srcRemote = true
		fmt.Printf("src is remote = %s\n", *srcDir)
	}

	if comm.IsRemoteResource(*dstDir) {
		schemaType, err = comm.GetSchema(*dstDir)
		if err != nil {
			fmt.Printf("get schema failed, err = %v\n", err)
			return
		}
		dstRemote = true
		fmt.Printf("dst is remote = %s\n", *dstDir)
	}

	s, err := storage.New(schemaType)
	if err != nil {
		fmt.Printf("create storage failed, err = %v\n", err)
		return
	}

	err = s.Handler.Init(comm.HandlerCfg{
		Debug: false,
	})
	if err != nil {
		fmt.Printf("init storage failed, err = %v\n", err)
		return
	}

	fmt.Printf("src dir = %s, dst dir = %s\n", *srcDir, *dstDir)
	if srcRemote && dstRemote {
		fmt.Printf("CopyDir\n")
		err = s.Handler.CopyDir(*srcDir, *dstDir)
	} else if !srcRemote && dstRemote {
		fmt.Printf("CopyFromLocalDir\n")
		err = s.Handler.CopyFromLocalDir(*srcDir, *dstDir)
	} else if srcRemote && !dstRemote {
		fmt.Printf("CopyFromRemoteDir\n")
		err = s.Handler.CopyFromRemoteDir(*srcDir, *dstDir)
	} else {
		fmt.Printf("Invalid\n")
		return
	}

	if err != nil {
		fmt.Printf("copy dir failed, err = %v\n", err)
		return
	}

	fmt.Printf("copy dir succ, srcdir = %s, dstdir = %s\n", *srcDir, *dstDir)
	return
}
