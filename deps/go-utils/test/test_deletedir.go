// Copyright 2020 Apulis Technology Inc. All rights reserved.

package main

import (
	"flag"
	"fmt"
	"github.com/apulis/sdk/go-utils/storage"
	"github.com/apulis/sdk/go-utils/storage/comm"
)

// ./test_deletedir -dst="FILE:///data1/321" -force=false
func main() {
	var dstDir = flag.String("dst", "", "dst path")
	var force = flag.Bool("force", false, "force delete")
	flag.Parse()

	schemaType, err := comm.GetSchema(*dstDir)
	if err != nil {
		fmt.Printf("get schema failed, err = %v\n", err)
		return
	}

	s, err := storage.New(schemaType)
	if err != nil {
		fmt.Printf("create storage failed, err = %v\n", err)
		return
	}

	err = s.Handler.Init(comm.HandlerCfg{
		Debug: false,
	})
	if err != nil {
		fmt.Printf("init storage failed, err = %v\n", err)
		return
	}

	fmt.Printf("dst dir = %s\n", *dstDir)
	err = s.Handler.DeleteDir(*dstDir, *force)
	if err != nil {
		fmt.Printf("delete dir failed, err = %v\n", err)
		return
	}

	fmt.Printf("delete dir succ, dstdir = %s\n", *dstDir)
	return
}
