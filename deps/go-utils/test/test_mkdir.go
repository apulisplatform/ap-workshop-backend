// Copyright 2020 Apulis Technology Inc. All rights reserved.

package main

import (
	"flag"
	"fmt"
	"github.com/apulis/sdk/go-utils/storage"
	"github.com/apulis/sdk/go-utils/storage/comm"
)

// ./test_mkdir -dst="FILE:///data1/111"
func main() {
	var dstPathWithSchema = flag.String("dst", "", "dst resource schema")
	flag.Parse()

	schemaType, err := comm.GetSchema(*dstPathWithSchema)
	if err != nil {
		fmt.Printf("get schema failed, err = %v\n", err)
		return
	}

	s, err := storage.New(schemaType)
	if err != nil {
		fmt.Printf("create storage failed, err = %v\n", err)
		return
	}

	err = s.Handler.Init(comm.HandlerCfg{
		Debug: false,
	})
	if err != nil {
		fmt.Printf("init storage failed, err = %v\n", err)
		return
	}

	err = s.Handler.Mkdir(*dstPathWithSchema)
	if err != nil {
		fmt.Printf("Mkdir failed, err = %v\n", err)
		return
	}

	fmt.Printf("mkdir succ, dst = %s\n", *dstPathWithSchema)
	return
}
