// Copyright 2020 Apulis Technology Inc. All rights reserved.

package main

import (
	"flag"
	"fmt"
	"github.com/apulis/sdk/go-utils/storage"
	"github.com/apulis/sdk/go-utils/storage/comm"
)

// ./test_copy -src="FILE:///data1/123/helloworld.txt" -dst="FILE:///data1/321/helloworld.txt"
// ./test_copy -src="S3://kefeng-test/123/helloworld.txt" -dst="S3://kefeng-test/321/helloworld.txt"
func main() {
	var srcPathWithSchema = flag.String("src", "", "source file local path")
	var dstPathWithSchema = flag.String("dst", "", "dst resource schema")
	flag.Parse()

	schemaType, err := comm.GetSchema(*dstPathWithSchema)
	if err != nil {
		fmt.Printf("get schema failed, err = %v\n", err)
		return
	}

	s, err := storage.New(schemaType)
	if err != nil {
		fmt.Printf("create storage failed, err = %v\n", err)
		return
	}

	err = s.Handler.Init(comm.HandlerCfg{
		Debug: false,
	})
	if err != nil {
		fmt.Printf("init storage failed, err = %v\n", err)
		return
	}

	err = s.Handler.CopyFile(*srcPathWithSchema, *dstPathWithSchema)
	if err != nil {
		fmt.Printf("CopyFile failed, err = %v\n", err)
		return
	}

	srcFile, _ := comm.GetSchemaFile(*srcPathWithSchema)
	dstFile, _ := comm.GetSchemaFile(*dstPathWithSchema)
	fmt.Printf("copy file succ, src = %s, dst = %s\n", srcFile, dstFile)
	return
}
