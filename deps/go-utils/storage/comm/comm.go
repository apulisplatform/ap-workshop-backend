// Copyright 2020 Apulis Technology Inc. All rights reserved.

package comm

import (
	"errors"
	"fmt"
	"os"
	"strings"
)

type HandlerCfg struct {
	Debug bool
}

const (
	SCHEMA_TYPE_LOCALFILE = "FILE"
	SCHEMA_TYPE_S3        = "S3"
)

const (
	REMOTE_RESOURCE_SCHEMA_IDENTITY = "://"
	SLASH                           = "/"
)

var (
	ErrTypeInvalidSchema  = errors.New("invalid storage schema")
	ErrTypeInvalidDir     = errors.New("invalid storage dir")
	ErrTypeInvalidParam   = errors.New("invalid param")
	ErrTypeDstFileExist   = errors.New("dst file exist")
	ErrTypeIsDir          = errors.New("is directory")
	ErrTypeNotImplemented = errors.New("method is not implemented")
	ErrGetS3Bucket        = errors.New("get s3 bucket failed")
)

func IsRemoteResource(resource string) bool {
	return strings.Contains(resource, REMOTE_RESOURCE_SCHEMA_IDENTITY)
}

func GetSchema(resource string) (string, error) {
	parts := strings.Split(resource, REMOTE_RESOURCE_SCHEMA_IDENTITY)
	if len(parts) != 2 {
		return "", fmt.Errorf("invalid resource path = [%s]", resource)
	}
	return parts[0], nil
}

func GetSchemaFile(resource string) (string, error) {
	parts := strings.Split(resource, "://")
	if len(parts) != 2 {
		return "", fmt.Errorf("invalid resource path = [%s]", resource)
	}
	return parts[1], nil
}

func IsDir(localFile string) bool {
	info, err := os.Stat(localFile)
	if err != nil {
		return false
	}
	return info.IsDir()
}
