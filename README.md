# APWorkshop

## How to run

There is a template config in this project, modifying for correct information and run by following command:

```shell
( go build -o /tmp/workshop &&chmod +x /tmp/workshop && /tmp/workshop -c {PROJECT_ROOT}/ap-workshop-backend/config_template.yaml )
```

## How to build image

```shell
make build-image
```
