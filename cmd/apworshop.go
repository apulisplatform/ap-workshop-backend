/* ******************************************************************************
* 2019 - present Contributed by Apulis Technology (Shenzhen) Co. LTD
*
* This program and the accompanying materials are made available under the
* terms of the MIT License, which is available at
* https://www.opensource.org/licenses/MIT
*
* See the NOTICE file distributed with this work for additional
* information regarding copyright ownership.
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations
* under the License.
*
* SPDX-License-Identifier: MIT
******************************************************************************/
package main

import (
	"apworkshop/internal/common"
	"apworkshop/internal/configs"
	databases "apworkshop/internal/databases"
	"apworkshop/internal/entity"
	"apworkshop/internal/loggers"
	"apworkshop/internal/mq"
	api "apworkshop/internal/routers"
	"apworkshop/internal/service"
	"apworkshop/internal/storage"
	"apworkshop/internal/tasks"
	validators "apworkshop/internal/validator"
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"

	"github.com/apulis/sdk/go-utils/broker"
	"github.com/apulis/sdk/go-utils/broker/rabbitmq"
	"github.com/urfave/cli/v2"
)

type APWorkshop struct {
	internalApp *cli.App
	flags       []cli.Flag
	configFile  string
	appConfig   configs.APWorkshopConfig
	// clusters     []cluster.ClusterConfig
	// genMinConfig bool
}

var logger = loggers.LogInstance()
var once sync.Once
var instance *APWorkshop

// var url = fmt.Sprintf("%s://%s:%s%s",
// 	configs.Config.FileServerConfig.Scheme,
// 	configs.Config.FileServerConfig.Host,
// 	configs.Config.FileServerConfig.Port,
// 	configs.Config.FileServerConfig.ApiPrefix)
// var url = fmt.Sprintf("%s://%s:%s%s", "http", "192.168.2.174", "444", "/api/v1/endpoints")

var client = &http.Client{
	Timeout: time.Second * 15,
}

func GetInstance() *APWorkshop {
	once.Do(func() {
		instance = &APWorkshop{
			internalApp: cli.NewApp(),
		}
	})
	return instance
}

func GetAppConfig() *configs.APWorkshopConfig {
	return &GetInstance().appConfig
}

func (app *APWorkshop) Init(appName string, appUsage string) error {
	app.flags = []cli.Flag{
		&cli.StringFlag{
			Name:        "config",
			Aliases:     []string{"c"},
			Usage:       "assign config file `PATH`",
			EnvVars:     []string{"APWORKSHOP_CONFIG_PATH"},
			Required:    false,
			Hidden:      false,
			TakesFile:   false,
			Value:       "/etc/apworkshop/config.yaml",
			Destination: &app.configFile,
			HasBeenSet:  false,
		},
	}

	app.internalApp.Name = appName
	app.internalApp.Usage = appUsage
	app.internalApp.Flags = app.flags
	app.internalApp.Action = func(c *cli.Context) error {
		return app.MainLoop(c)
	}
	return nil
}

func (app *APWorkshop) MainLoop(c *cli.Context) error {
	logger.Infoln("App initializing")

	// Init Config
	err := app.InitConfig()
	if err != nil {
		return err
	}

	// Init Logger
	err = app.InitLogger()
	if err != nil {
		return err
	}

	err = app.InitMq()
	if err != nil {
		return err
	}

	// Init Database
	trys := 10
	trysWait := 30 * time.Second
	for i := trys; i > 0; i-- {
		err = app.InitDatabase()
		if err != nil {
			logger.Errorf("InitDatabase failed！ err = %s", err.Error())
			logger.Infof("Left try times = %d", i)
			time.Sleep(trysWait)
		} else {
			break
		}
	}

	// Init Endpoint Policy
	trys = 10
	trysWait = 30 * time.Second
	for i := trys; i > 0; i-- {
		err = app.InitEndpointPolicy()
		if err != nil {
			logger.Errorf("Initial endpoint policy failed！err = %s", err.Error())
			logger.Infof("Left try times = %d", i)
			time.Sleep(trysWait)
		} else {
			logger.Info("Endpoitn Policy registration complete")
			break
		}
	}
	if err != nil {
		return err
	}

	// auto create table for Huawei(松山湖)
	_ = databases.CreateTableIfNotExists(entity.ModelDbStruct{})
	_ = databases.CreateTableIfNotExists(entity.ModelVersion{})
	_ = databases.CreateTableIfNotExists(entity.InferDbStruct{})
	_ = databases.CreateTableIfNotExists(entity.InferNodeDbStruct{})
	_ = databases.CreateTableIfNotExists(entity.ResourceDbStruct{})
	// table for AIStudio
	_ = databases.CreateTableIfNotExists(entity.StudioModel{})
	_ = databases.CreateTableIfNotExists(entity.StudioModelVersion{})
	_ = databases.CreateTableIfNotExists(entity.StudioInferenceNode{})
	_ = databases.CreateTableIfNotExists(entity.StudioInferenceModel{})
	_ = databases.CreateTableIfNotExists(entity.Job{})

	// Init Storage
	err = app.InitStorage()
	if err != nil {
		return err
	}

	// Init Params
	err = app.InitParams()
	if err != nil {
		return err
	}

	// Init Validators
	err = app.InitValidator()
	if err != nil {
		return err
	}

	logger.Infoln("App running")

	// quit when signal notifys
	quit := make(chan os.Signal)
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)

	// start api server
	srv := api.StartApiServer(&app.appConfig)

	rabbitMQBroker := rabbitmq.NewBroker(
		broker.Addrs(fmt.Sprintf("amqp://%s:%s@%s:%s",
			configs.Config.JobScheduler.RabbitMQ.User,
			configs.Config.JobScheduler.RabbitMQ.Password,
			configs.Config.JobScheduler.RabbitMQ.Host,
			configs.Config.JobScheduler.RabbitMQ.Port)),
		rabbitmq.ExchangeName(configs.Config.JobScheduler.RabbitMQ.ExchangeName),
	)
	if err := rabbitMQBroker.Connect(); err != nil {
		logger.Fatal(err)
	}
	_, err = rabbitMQBroker.Subscribe(
		fmt.Sprintf("%d", configs.Config.Job.ModId),
		tasks.RabbitMQMsgHandler,
		rabbitmq.DurableQueue(),
	)
	if err != nil {
		logger.Fatal(err)
	}
	defer rabbitMQBroker.Disconnect()

	select {
	case <-quit:
		api.StopApiServer(srv)
	}
	return nil
}

func (app *APWorkshop) Run(arguments []string) error {
	err := app.internalApp.Run(os.Args)
	if err != nil {
		return err
	}
	return nil
}

func (app *APWorkshop) InitConfig() error {
	logger.Infoln("Config path: " + app.configFile)

	configs.InitConfig(app.configFile, &app.appConfig)
	logger.Infoln("Config init complete: ")
	logger.Infoln(app.appConfig)
	return nil
}

func (app *APWorkshop) InitLogger() error {

	loggers.InitLogger(&app.appConfig.Log)
	logger.Infoln("Log init complete")
	return nil
}

func (app *APWorkshop) InitMq() error {
	mq.InitMq(&app.appConfig)
	logger.Infoln("Mq init complete")
	return nil
}

func (app *APWorkshop) InitDatabase() error {
	err := databases.InitDatabase(&app.appConfig.Database, app.appConfig.DebugMode)
	if err != nil {
		return err
	}
	return nil
}

func (app *APWorkshop) InitEndpointPolicy() error {
	endpoint := entity.EndPointsAndPolicies{}
	scheme := app.appConfig.Endpoint.Scheme
	host := app.appConfig.Endpoint.Host
	port := app.appConfig.Endpoint.Port
	urlPrefix := app.appConfig.Endpoint.UrlPrefix

	url := fmt.Sprintf("%s://%s:%s%s", scheme, host, port, urlPrefix)
	logger.Info("Endpoint registration policy url: " + url)

	// [reserve for latter] use a file to import endpoint policy

	// filePath := "/root/qirenf/configs/apworkshop_endpoints.json"
	// f, err := os.Open(filePath)
	// if err != nil {
	// 	return err
	// }
	// // utils.FailOnError(err, "Failed to read file")
	// reader := bufio.NewReader(f)
	// content, err := ioutil.ReadAll(reader)
	// if err != nil {
	// 	return err
	// }
	// err = json.Unmarshal(content, &endpoint)
	// if err != nil {
	// 	return err
	// }
	// err = json.Unmarshal(content, &endpoint)
	// if err != nil {
	// 	return err
	// }
	// jsonValue, _ := json.Marshal(endpoint)

	validationJsonByte := []byte(common.ValidationJsonString)
	err := json.Unmarshal(validationJsonByte, &endpoint)
	if err != nil {
		return err
	}
	jsonValue, err := json.Marshal(&endpoint)
	if err != nil {
		return err
	}
	req, err := http.NewRequest("POST", url, bytes.NewReader(jsonValue))
	if err != nil {
		return err
	}
	// check for endpoint registration response
	req.Header.Add("Content-Type", "application/json")
	resp, err := client.Do(req)
	bodys, _ := ioutil.ReadAll(resp.Body)
	logger.Info("EndPoint registration response: " + string(bodys))
	if err != nil {
		return err
	}
	return nil
}

func (app *APWorkshop) InitStorage() error {
	err := os.MkdirAll(common.MODEL_STORAGE_DIR, 0777)
	if err != nil {
		return err
	}
	// delete cache
	err = os.RemoveAll(common.MODEL_PACK_TEMP_DIR)
	if err != nil {
		return err
	}
	err = os.MkdirAll(common.MODEL_PACK_TEMP_DIR, 0777)
	if err != nil {
		return err
	}

	storage.InitStorage(&app.appConfig.Storage)
	return nil
}

func (app *APWorkshop) InitParams() error {

	service.TimeoutSeconds = app.appConfig.Storage.TimeoutSeconds

	return nil
}

func (app *APWorkshop) InitValidator() error {
	validators.InitValidator()
	return nil
}
