/* ******************************************************************************
* 2019 - present Contributed by Apulis Technology (Shenzhen) Co. LTD
*
* This program and the accompanying materials are made available under the
* terms of the MIT License, which is available at
* https://www.opensource.org/licenses/MIT
*
* See the NOTICE file distributed with this work for additional
* information regarding copyright ownership.
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations
* under the License.
*
* SPDX-License-Identifier: MIT
******************************************************************************/
package appErr

import "errors"

const (
	SUCCESS_MSG = "OK"
)

const (
	ERR_CODE_PREFIX = 2400 * 100000

	SUCCESS_CODE = 0

	APP_ERROR_CODE              = ERR_CODE_PREFIX + 10000
	NOT_FOUND_ERROR_CODE        = ERR_CODE_PREFIX + 10001
	UNKNOWN_ERROR_CODE          = ERR_CODE_PREFIX + 10002
	SERVER_ERROR_CODE           = ERR_CODE_PREFIX + 10003
	PARAMETER_ERROR_CODE        = ERR_CODE_PREFIX + 10004
	RECORD_NOT_FOUND_ERROR_CODE = ERR_CODE_PREFIX + 10005
	DATABASE_ERROR_CODE         = ERR_CODE_PREFIX + 10006

	// model error codes
	COMMIT_MODEL_ERROR_CODE               = ERR_CODE_PREFIX + 20002
	ROLLBACK_MODEL_ERROR_CODE             = ERR_CODE_PREFIX + 20003
	CHECK_REGISTER_CONTEXT_ERROR_CODE     = ERR_CODE_PREFIX + 20004
	CREATE_NEW_MODEL_ERROR_CODE           = ERR_CODE_PREFIX + 20005
	STORAGE_COMPONENT_RELATE_ERROR_CODE   = ERR_CODE_PREFIX + 20006
	QUERY_DB_ERROR_CODE                   = ERR_CODE_PREFIX + 20007
	COMMIT_TIMEOUT_ERROR_CODE             = ERR_CODE_PREFIX + 20008
	MANIFEST_READING_ERROR_CODE           = ERR_CODE_PREFIX + 20009
	MANIFEST_VALIDATE_ERROR_CODE          = ERR_CODE_PREFIX + 20010
	MANIFEST_DATASET_NOT_VALID_ERROR_CODE = ERR_CODE_PREFIX + 20011
	MANIFEST_TRAIN_NOT_VALID_ERROR_CODE   = ERR_CODE_PREFIX + 20012
	MANIFEST_EVAL_NOT_VALID_ERROR_CODE    = ERR_CODE_PREFIX + 20013

	INFERENCE_READING_ERROR_CODE          = ERR_CODE_PREFIX + 20014
	INFERENCE_VALIDATE_ERROR_CODE         = ERR_CODE_PREFIX + 20015
	INFERENCE_CENTER_NOT_VALID_ERROR_CODE = ERR_CODE_PREFIX + 20016
	INFERENCE_EDGE_NOT_VALID_ERROR_CODE   = ERR_CODE_PREFIX + 20017
	NO_INFERENCE_ERROR_CODE               = ERR_CODE_PREFIX + 20018

	// model version error codes
	STUDIO_MODEL_NOT_FOUND_ERROR_CODE = ERR_CODE_PREFIX + 20101
	STUDIO_MODEL_UPDATE_ERROR_CODE    = ERR_CODE_PREFIX + 20102

	// model job error codes
	STUDIO_MODEL_JOB_ERROR_CODE           = ERR_CODE_PREFIX + 20201
	STUDIO_MODEL_JOB_NOT_FOUND_ERROR_CODE = ERR_CODE_PREFIX + 20202

	// model commit rollback
	STUDIO_MODEL_COMMIT_FAIL         = ERR_CODE_PREFIX + 20301
	STUDIO_MODEL_ROLLBACK_NOT_EXISTS = ERR_CODE_PREFIX + 20302

	// model lock error codes
	STUDIO_MODEL_REF_NOT_EXISTS = ERR_CODE_PREFIX + 20401
	REF_RESOURCE_UNFOUND        = ERR_CODE_PREFIX + 40001
)

var (
	RECORD_NOT_FOUND_ERROR = errors.New("record not found")

	MANIFEST_NOT_VALID_ERROR         = errors.New("manifest not valid")
	MANIFEST_DATASET_NOT_VALID_ERROR = errors.New("manifest.dataset not valid")
	MANIFEST_TRAIN_NOT_VALID_ERROR   = errors.New("manifest.train not valid")
	MANIFEST_EVAL_NOT_VALID_ERROR    = errors.New("manifest.eval not valid")

	NO_INFERENCE_ERROR               = errors.New("no inference")
	INFERENCE_NOT_VALID_ERROR        = errors.New("inference not valid")
	INFERENCE_CENTER_NOT_VALID_ERROR = errors.New("inference.center not valid")
	INFERENCE_EDGE_NOT_VALID_ERROR   = errors.New("inference.edge not valid")
)
