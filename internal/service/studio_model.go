/* ******************************************************************************
* 2019 - present Contributed by Apulis Technology (Shenzhen) Co. LTD
*
* This program and the accompanying materials are made available under the
* terms of the MIT License, which is available at
* https://www.opensource.org/licenses/MIT
*
* See the NOTICE file distributed with this work for additional
* information regarding copyright ownership.
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations
* under the License.
*
* SPDX-License-Identifier: MIT
******************************************************************************/
package service

import (
	"apworkshop/internal/appErr"
	"apworkshop/internal/common"
	"apworkshop/internal/configs"
	"apworkshop/internal/dao"
	"apworkshop/internal/databases"
	"apworkshop/internal/entity"
	"apworkshop/internal/proto"
	appStorage "apworkshop/internal/storage"
	"apworkshop/internal/utils"
	validators "apworkshop/internal/validator"
	"archive/tar"
	"bytes"
	"compress/gzip"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"path"
	"regexp"
	"strconv"
	"strings"
	"syscall"
	"time"

	"github.com/apulis/sdk/fs-go-client/pkg"
	"github.com/google/uuid"
	"gopkg.in/yaml.v2"
)

var client = &http.Client{
	Timeout: time.Second * 15,
}

// service for AIStudio starts here
func CreateStudioModelsByPack(scope, modelName, modelDescription string, targetDir, auth, userName, userId, userGroup string) (int, error) {
	// === tidy directory first,because it's hard to operate remote dir in prepare and commit process
	// read manifest
	var manifest entity.StudioModel
	errCode, err := readStudioManifest(&manifest, targetDir+"/manifest.yaml")
	if err != nil {
		logger.Debugln("read manifest yaml error")
		return errCode, err
	}

	// get remote directory
	if modelName == "" {
		modelName = manifest.Name
	}
	if modelDescription == "" {
		modelDescription = manifest.Description
	}
	// TODO: use file md5 as context
	emptyUserName := ""
	isTmpFalse := 0
	_, _, dstPath, locker, errCode, err := PrepareStudioModel(uuid.New().String(), scope, modelName, modelDescription, emptyUserName, isTmpFalse, true)
	if err != nil {
		return errCode, err
	}
	// save to remote
	err = saveToLoader(targetDir, dstPath)
	if err != nil {
		return appErr.APP_ERROR_CODE, err
	}
	errCode, err = CommitStudioModel(&proto.StudioCommitModelReq{
		Context: locker,
	}, auth, userName, userId, userGroup)
	if err != nil {
		return errCode, err
	}

	return appErr.SUCCESS_CODE, nil
}

func StudioModelGetTmp(userName string, modelId, modelVersionId int) (*entity.StudioModelVersion, int, error) {
	tmpStoragePath, err := appStorage.Handler.GetUploadTmpPath(modelId, modelVersionId, userName)
	if err != nil {
		return &entity.StudioModelVersion{}, appErr.STUDIO_MODEL_NOT_FOUND_ERROR_CODE, err
	}
	realPath, err := appStorage.Handler.GetTranslatePath(tmpStoragePath)
	if err != nil {
		return &entity.StudioModelVersion{}, appErr.STUDIO_MODEL_NOT_FOUND_ERROR_CODE, err
	}
	oldMask := syscall.Umask(0)
	defer syscall.Umask(oldMask)
	err = os.MkdirAll(realPath, 0777)
	if err != nil {
		return &entity.StudioModelVersion{}, appErr.STUDIO_MODEL_NOT_FOUND_ERROR_CODE, err
	}
	return &entity.StudioModelVersion{TmpStoragePath: tmpStoragePath}, appErr.SUCCESS_CODE, nil
}

// create new model and model version, it will detect request context, same context will do nothing
func PrepareStudioModel(ctx, scope, modelName, modelDescription, userName string, isTmp int, isPreload bool) (*entity.StudioModel, *entity.StudioModelVersion, string, string, int, error) {
	var contextCount int64
	var modelVersion entity.StudioModelVersion
	var versionResult *entity.StudioModelVersion

	// check ctx not same
	err := databases.Db.
		Model(&entity.StudioModelVersion{}).
		Where("register_ctx = ?", ctx).
		Count(&contextCount).
		Error
	if err != nil {
		return nil, nil, "", "", appErr.QUERY_DB_ERROR_CODE, err
	}
	if contextCount != 0 {
		logger.Debugln("context found")
		err := databases.Db.
			Where("register_ctx = ?", ctx).
			First(&modelVersion).
			Error
		if err != nil {
			return nil, nil, "", "", appErr.QUERY_DB_ERROR_CODE, err
		}
		versionResult = &modelVersion
	}

	manifest := entity.StudioModel{}
	if scope != "" {
		manifest.Scope = scope
	}
	if modelName != "" {
		manifest.Name = modelName
	}
	if modelDescription != "" {
		manifest.Description = modelDescription
	}

	// check if model exist, if not, create a new model, else create a new version
	modelDb, err := dao.GetStudioModelByNameAndScope(modelName, scope)
	if err != nil {
		return nil, nil, "", "", appErr.QUERY_DB_ERROR_CODE, err
	}
	if modelDb == nil {
		// save to db
		modelDb, err = dao.CreateStudioModel(&manifest, isPreload)
		if err != nil {
			return nil, nil, "", "", appErr.CREATE_NEW_MODEL_ERROR_CODE, err
		}
	}

	if contextCount == 0 {
		versionResult, err = dao.CreateNewVersionForStudioModel(ctx, modelDb.ID)
		if err != nil {
			return nil, nil, "", "", appErr.CREATE_NEW_MODEL_ERROR_CODE, err
		}
		fileDir := common.MODEL_STORAGE_DIR
		uploadPath, err := appStorage.Handler.GetUploadPath(modelDb.ID, versionResult.ID, fileDir)
		if err != nil {
			return nil, nil, "", "", appErr.STORAGE_COMPONENT_RELATE_ERROR_CODE, err
		}
		localPath, err := appStorage.Handler.GetTranslatePath(uploadPath)
		if err != nil {
			return nil, nil, "", "", appErr.STORAGE_COMPONENT_RELATE_ERROR_CODE, err
		}
		err = os.RemoveAll(localPath)
		if err != nil {
			return nil, nil, "", "", appErr.STORAGE_COMPONENT_RELATE_ERROR_CODE, err
		}
		oldMask := syscall.Umask(0)
		err = os.MkdirAll(localPath, 0777)
		syscall.Umask(oldMask)
		if err != nil {
			return nil, nil, "", "", appErr.STORAGE_COMPONENT_RELATE_ERROR_CODE, err
		}

		versionResult.StoragePath = uploadPath
		tmpStoragePath, err := appStorage.Handler.GetUploadTmpPath(modelVersion.ModelID, modelVersion.ID, userName)
		if err != nil {
			return nil, nil, "", "", appErr.CREATE_NEW_MODEL_ERROR_CODE, err
		}
		tmpStoragePath = fmt.Sprintf("%s/%s", tmpStoragePath, ctx)
		realTmpPath, err := appStorage.Handler.GetTranslatePath(tmpStoragePath)
		if err != nil {
			return nil, nil, "", "", appErr.CREATE_NEW_MODEL_ERROR_CODE, err
		}
		oldMasks := syscall.Umask(0)
		err = os.MkdirAll(realTmpPath, 0777)
		syscall.Umask(oldMasks)
		if err != nil {
			return nil, nil, "", "", appErr.CREATE_NEW_MODEL_ERROR_CODE, err
		}
		versionResult.Locker = ctx
		versionResult.Description = modelDescription
		versionResult.Status = common.MODEL_VERSION_STATUS_PREPARE
		if isTmp == 1 {
			versionResult.TmpStoragePath = tmpStoragePath
		}
		_, err = dao.UpdateStudioModelVersion(versionResult)
		if err != nil {
			return nil, nil, "", "", appErr.CREATE_NEW_MODEL_ERROR_CODE, err
		}
	}
	modelDb.Description = modelDescription
	dao.UpdateStudioModel(modelDb)
	if isTmp == 1 {
		return modelDb, versionResult, versionResult.TmpStoragePath, versionResult.Locker, 0, nil
	}

	return modelDb, versionResult, versionResult.StoragePath, versionResult.Locker, 0, nil
}

func CommitStudioModel(req *proto.StudioCommitModelReq, auth, userName, userId, userGroup string) (int, error) {
	//check locker
	if req.Context == "" {
		return appErr.PARAMETER_ERROR_CODE, errors.New("commit needs lock")
	}
	// get model Version by locker
	version, err := dao.GetStudioModelVersionByLockerStatusPrepare(req.Context)
	if err != nil {
		return appErr.QUERY_DB_ERROR_CODE, err
	}

	// check if timeout
	nowTimeStamp := time.Now().Unix()
	if int(nowTimeStamp-version.CreatedAt.Unix()) > TimeoutSeconds*1000 {
		return appErr.COMMIT_TIMEOUT_ERROR_CODE, errors.New("commit timeout")
	}
	// check if commit already
	if version.Status == common.MODEL_VERSION_STATUS_FREE {
		return appErr.SUCCESS_CODE, nil
	}
	// check if temp storage is empty
	if version.TmpStoragePath != "" {
		err = saveToLoader(version.TmpStoragePath, version.StoragePath)
		if err != nil {
			return appErr.APP_ERROR_CODE, err
		}
		os.RemoveAll(version.TmpStoragePath)
	}
	// get real path
	localPath, err := appStorage.Handler.GetTranslatePath(version.StoragePath)
	if err != nil {
		return appErr.STORAGE_COMPONENT_RELATE_ERROR_CODE, err
	}

	// read manifest and inference yaml
	var model entity.StudioModel
	var inferenceManifest entity.StudioInference
	var evalResult entity.StudioVersionEvalResult
	var codeFileJson entity.StudioCodeFile

	// check if manifest sub directory is empty
	var errCode int
	errCode, err = readStudioManifest(&model, localPath+"/manifest.yaml")
	if err != nil {
		logger.Debugln("read manifest yaml error")
		return errCode, err
	}

	// // check if harbor has engine
	// logger.Info("--------------------------------")
	// logger.Info(model.Engine)
	// logger.Info(auth)
	// if strings.Contains(model.Engine, "#") {
	// 	ifImageExists, err := checkIfEngineInHarbor(model.Engine, auth)
	// 	if err != nil {
	// 		return appErr.APP_ERROR_CODE, err
	// 	}
	// 	if !ifImageExists {
	// 		return appErr.APP_ERROR_CODE, errors.New("image does not exists")
	// 	}
	// }

	// check if code store in git
	codePath := localPath + common.MANIFEST_SUB_DIR
	codeFile, err := os.Stat(codePath)
	if err != nil {
		return appErr.APP_ERROR_CODE, err
	}
	if !codeFile.IsDir() {
		jsonFile, err := ioutil.ReadFile(codePath)
		if err != nil {
			return appErr.APP_ERROR_CODE, err
		}
		err = json.Unmarshal(jsonFile, &codeFileJson)
		if err != nil {
			return appErr.APP_ERROR_CODE, err
		}
		model.CommitId = codeFileJson.CommitId
		model.RepoId = codeFileJson.RepoId
	}

	// only validate file existence when it stores in local
	if model.CommitId == "" && model.RepoId == "" {
		// check for eval.py & train.py
		trainFile, err := os.Stat(localPath + common.TRAIN_SUB_DIR + "/" + model.Train.Entry)
		if os.IsExist(err) || trainFile == nil {
			logger.Debugln("cannot find " + model.Train.Entry)
			return appErr.APP_ERROR_CODE, err
		}

		if trainFile.Size() <= 0 {
			logger.Debugln(model.Train.Entry + " is empty")
			return appErr.APP_ERROR_CODE, errors.New(model.Train.Entry + " is empty")
		}

		if model.Eval != nil {
			evalFile, err := os.Stat(localPath + common.TRAIN_SUB_DIR + "/" + model.Eval.Entry)
			if os.IsExist(err) || evalFile == nil {
				logger.Debugln("cannot find " + model.Eval.Entry)
				return appErr.APP_ERROR_CODE, err
			}
			if evalFile.Size() <= 0 {
				logger.Debugln(model.Eval.Entry + " is empty")
				return appErr.APP_ERROR_CODE, errors.New(model.Eval.Entry + " is empty")
			}
		}

		// validate datasets existence
		for _, v := range model.Datasets {
			datasetFile, err := os.Stat(localPath + common.TRAIN_SUB_DIR + "/" + v.Path)
			if os.IsExist(err) || datasetFile == nil {
				logger.Debugln("cannot find datasets file: " + v.Path)
				return appErr.APP_ERROR_CODE, err
			}
			if datasetFile.Size() <= 0 {
				logger.Debugln(v.Path + " is empty")
				return appErr.APP_ERROR_CODE, errors.New(v.Path + " is empty")
			}
		}
	}

	// check for eval result
	haveEvalResult := false
	_, err = os.Stat(localPath + "/infer/eval_result.json")
	if err != nil {
		if os.IsExist(err) {
			logger.Debugln("eval_result json not found")
		} else {
			logger.Debugln("unknown eval_result json error:" + err.Error())
		}
	} else {
		haveEvalResult = true
	}

	if haveEvalResult {
		errCode, err = readStudioEvalResult(&evalResult, localPath+"/infer/eval_result.json")
		if err != nil {
			logger.Debugln("read eval_result json error")
			return errCode, err
		}
	}

	// check for infer.yaml
	canInfer := false
	_, err = os.Stat(localPath + "/infer.yaml")
	if err != nil {
		if os.IsExist(err) {
			logger.Debugln("Inference yaml not found")
		} else {
			logger.Debugln("unknown inference yaml error:" + err.Error())
		}
	} else {
		canInfer = true
	}

	if canInfer {
		errCode, err = readStudioInferenceManifest(&inferenceManifest, localPath+"/infer.yaml")
		if err != nil {
			logger.Debugln("read inference yaml error")
			return errCode, err
		}
	}
	readMe, err := reaStudioReadMe(localPath + "/README.md")
	if err != nil {
		return appErr.APP_ERROR_CODE, err
	}
	// user can modify model name prior to manifest,so before update, check model name has not been set in prepare process.
	checkModel, err := dao.GetStudioModelById(version.ModelID)
	if err != nil {
		return appErr.QUERY_DB_ERROR_CODE, err
	}
	if checkModel.Name != "" {
		model.Name = ""
	}
	if checkModel.Description != "" {
		model.Description = ""
	}
	// if checkModel.Description != "" {
	// 	model.Description = ""
	// }
	// save manifest and inferenceManifest
	model.ID = version.ModelID
	model.ReadMe = readMe
	model.UserGroup = userGroup
	savedModel, err := dao.UpdateStudioModel(&model)
	if err != nil {
		return appErr.QUERY_DB_ERROR_CODE, err
	}
	if canInfer {
		inferenceManifest.ModelVersionID = version.ID
		err = dao.CreateStudioInference(model.ID, version.ID, &inferenceManifest)
		if err != nil {
			return appErr.QUERY_DB_ERROR_CODE, err
		}
	}

	//update model version info
	version.Engine = savedModel.Engine
	version.ModelName = checkModel.Name
	version.Description = savedModel.Description
	version.ImagePath = savedModel.Logo
	version.Devices = savedModel.Devices
	version.VerifyCommand = savedModel.VerifyCommand
	version.Evaluation = evalResult.Evaluation
	version.Backbone = savedModel.Backbone
	version.Platform = savedModel.Platform
	version.Source = savedModel.Source
	version.AutoDL = savedModel.AutoDL
	version.Logo = savedModel.Logo
	version.Redevelop = savedModel.Redevelop
	version.Props = savedModel.Props
	version.JobId = savedModel.JobId
	version.Framework = savedModel.Framework
	version.DistributedFramework = savedModel.DistributedFramework
	version.Datasets = savedModel.Datasets
	version.Field = savedModel.Field
	version.Task = savedModel.Task
	version.RepoId = savedModel.RepoId
	version.CommitId = savedModel.CommitId
	version.Train = savedModel.Train
	version.Eval = savedModel.Eval
	version.ReadMe = readMe
	version.Tags = savedModel.Tags
	version.Publisher = userName
	version.PublishTime = savedModel.UpdatedAt
	version.DeviceTypeNvidiaGPU = savedModel.DeviceTypeNvidiaGPU
	version.DeviceTypeHuaweiNPU = savedModel.DeviceTypeHuaweiNPU
	version.DeviceTypeCPU = savedModel.DeviceTypeCPU
	version.UserGroup = userGroup

	if savedModel.Tags != nil {
		version.Dataset = savedModel.Tags["datasetName"]
	}

	if version.RepoId != "" && version.CommitId != "" {
		err = refRepo(version, userName, userId)
		if err != nil {
			return appErr.APP_ERROR_CODE, err
		}
	}
	dao.UpdateStudioModelVersion(version)

	// TODO: now can use context
	err = dao.CommitStudioVersion(req.Context)
	if err != nil {
		return appErr.QUERY_DB_ERROR_CODE, err
	}

	return appErr.SUCCESS_CODE, nil
}

func readStudioManifest(manifest *entity.StudioModel, manifestPath string) (int, error) {
	// check if manifest exist
	logger.Debugln(manifestPath)
	_, err := os.Stat(manifestPath) //os.Stat获取文件信息
	if err != nil {
		if os.IsExist(err) {
			return appErr.MANIFEST_READING_ERROR_CODE, errors.New("not found manifest config")
		}
		return appErr.MANIFEST_READING_ERROR_CODE, err
	}

	// read manifest
	yamlfile, err := ioutil.ReadFile(manifestPath)
	if err != nil {
		return appErr.MANIFEST_READING_ERROR_CODE, err
	}
	err = yaml.Unmarshal(yamlfile, manifest)
	if err != nil {
		return appErr.MANIFEST_READING_ERROR_CODE, err
	}
	logger.Debugln(manifest)
	errCode, err := validators.ValidateStudioManifest(manifest)
	if err != nil {
		return errCode, err
	}

	if manifest.Train != nil {
		manifest.Ability = append(manifest.Ability, entity.STUDIO_MODEL_ABILITY_TRAIN)
		manifest.CanTrain = true
	}

	for _, device := range manifest.Devices {
		if device.Type == "huawei_npu" {
			manifest.DeviceTypeHuaweiNPU = true
		} else if device.Type == "nvidia_gpu" {
			manifest.DeviceTypeNvidiaGPU = true
		} else if device.Type == "cpu" {
			manifest.DeviceTypeCPU = true
		}
	}

	return appErr.SUCCESS_CODE, nil
}

func checkIfEngineInHarbor(engine, auth string) (bool, error) {
	scheme := configs.Config.Harbor.Scheme
	host := configs.Config.Harbor.Host
	port := configs.Config.Harbor.Port
	urlPrefix := configs.Config.Harbor.UrlPrefix

	url := fmt.Sprintf("%s://%s:%s%s?%s", scheme, host, port, urlPrefix, "imageFullName="+strings.Replace(engine, "#", "", 1))
	logger.Info("Check if engine in harbor url: " + url)

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return false, err
	}
	req.Header.Add("Authorization", auth)
	logger.Info("------------------------------------------")
	logger.Info(req)
	logger.Info(req.Header)
	resp, err := client.Do(req)
	bodys, _ := ioutil.ReadAll(resp.Body)
	logger.Debug("Harbor check response: " + string(bodys))
	if err != nil {
		return false, err
	}
	var respObject proto.HarborSuccessResp
	err = json.Unmarshal(bodys, &respObject)
	if err != nil {
		return false, err
	}
	return respObject.Data.IfExist, nil
}

func refRepo(version *entity.StudioModelVersion, userName, userId string) error {
	scheme := configs.Config.AiLab.Scheme
	host := configs.Config.AiLab.Host
	port := configs.Config.AiLab.Port
	urlPrefix := configs.Config.AiLab.RefUrlPrefix

	url := fmt.Sprintf("%s://%s:%s%s", scheme, host, port, urlPrefix)
	logger.Info("Ref AI lab url: " + url)

	bind := "apworkshop-" + uuid.New().String()
	intUserId, err := strconv.Atoi(userId)
	if err != nil {
		return err
	}
	output := entity.JsonB{
		"bind":    bind,
		"repoId":  version.RepoId,
		"creator": userName,
		"userId":  intUserId,
	}

	outputJson, err := json.Marshal(output)
	if err != nil {
		return err
	}
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(outputJson))
	if err != nil {
		return err
	}
	// check for ref responses
	req.Header.Add("Content-Type", "application/json")
	resp, err := client.Do(req)
	if err != nil {
		return err
	}
	bodys, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	logger.Debug("Ref repo response: " + string(bodys))
	if err != nil {
		return err
	}

	version.Bind = bind
	return nil
}

func DepressStudioModelPack(filePath, fileName string) (string, int, error) {
	var targetDir string

	// decompress file
	// check file format
	checkZipAndTarRe := `.*\.(zip|tar\.gz|tar)$`
	r := regexp.MustCompile(checkZipAndTarRe)
	logger.Info(fileName)
	if !(r.MatchString(fileName)) {
		return targetDir, appErr.APP_ERROR_CODE, errors.New("can't support file format")
	}

	targetDir = common.MODEL_PACK_TEMP_DIR + "/" + uuid.New().String()
	err := os.MkdirAll(targetDir, 0777)
	if err != nil {
		return targetDir, appErr.APP_ERROR_CODE, err
	}
	os.Chmod(targetDir, 0777)
	logger.Debugln("================== depress ======================")
	isZipRe := `.*\.zip$`
	isTarRe := `.*\.tar$`
	rZip := regexp.MustCompile(isZipRe)
	rTar := regexp.MustCompile(isTarRe)
	if rZip.MatchString(fileName) {
		logger.Info("------------------------------------------1")
		err = zipHandler(filePath, targetDir)
		if err != nil {
			return targetDir, appErr.APP_ERROR_CODE, err
		}
	} else if rTar.MatchString(fileName) {
		tarHandler(filePath, targetDir)
		if err != nil {
			return targetDir, appErr.APP_ERROR_CODE, err
		}
	} else {
		logger.Info("------------------------------------------2")
		tarGzHandler(filePath, targetDir)
		if err != nil {
			return targetDir, appErr.APP_ERROR_CODE, err
		}
	}
	logger.Debugln("================== depress end ======================")

	return targetDir, appErr.SUCCESS_CODE, nil
}

func readStudioInferenceManifest(inference *entity.StudioInference, manifestPath string) (int, error) {
	// check if manifest exist
	_, err := os.Stat(manifestPath) //os.Stat获取文件信息
	if err != nil {
		if os.IsExist(err) {
			return appErr.INFERENCE_READING_ERROR_CODE, errors.New("not found inference config")
		}
		return appErr.INFERENCE_READING_ERROR_CODE, err
	}

	// read manifest
	yamlfile, err := ioutil.ReadFile(manifestPath)
	if err != nil {
		return appErr.INFERENCE_READING_ERROR_CODE, err
	}
	err = yaml.Unmarshal(yamlfile, inference)
	if err != nil {
		return appErr.INFERENCE_READING_ERROR_CODE, err
	}

	var inferDb entity.StudioInferenceModel
	err = yaml.Unmarshal(yamlfile, &inferDb)
	if err != nil {
		return appErr.INFERENCE_READING_ERROR_CODE, err
	}
	inference.StudioInferenceModel = inferDb
	logger.Debugln("=== found inference in :" + manifestPath + ", show below")
	logger.Debugln(inference)
	if inference.Center != nil {
		logger.Debugln(inference.Center)
	}
	if inference.Edge != nil {
		logger.Debugln(inference.Edge)
	}
	logger.Debugln("=== found inference manifest")

	errCode, err := validators.ValidateStudioInferenceManifest(inference)
	if err != nil {
		return errCode, err
	}

	return appErr.SUCCESS_CODE, nil
}

func reaStudioReadMe(readMePath string) (string, error) {
	_, err := os.Stat(readMePath) //os.Stat获取文件信息
	if err != nil {
		return "", nil
	}
	// read README.md
	readMeContent, err := ioutil.ReadFile(readMePath)
	if err != nil {
		return "", err
	}
	return string(readMeContent), nil
}

func readStudioEvalResult(evalResult *entity.StudioVersionEvalResult, evalReaultPath string) (int, error) {
	// check if evaluation result exist
	_, err := os.Stat(evalReaultPath) //os.Stat获取文件信息
	if err != nil {
		if os.IsExist(err) {
			return appErr.INFERENCE_READING_ERROR_CODE, errors.New("not found inference config")
		}
		return appErr.INFERENCE_READING_ERROR_CODE, err
	}

	// read eval result
	yamlfile, err := ioutil.ReadFile(evalReaultPath)
	if err != nil {
		return appErr.INFERENCE_READING_ERROR_CODE, err
	}
	err = json.Unmarshal(yamlfile, evalResult)
	if err != nil {
		return appErr.INFERENCE_READING_ERROR_CODE, err
	}

	logger.Debugln("=== found evaluation result in :" + evalReaultPath + ", show below")
	logger.Debugln(evalResult)

	logger.Debugln("=== found evaluation result")

	return appErr.SUCCESS_CODE, nil
}

func GetStudioModelList(req *proto.GetStudioModelListReq, userGroup string) ([]entity.StudioModel, int, int, error) {
	var total int
	list, total, err := dao.GetStudioModelList(req.PageNum, req.PageSize, req.Scope, req.Name, req.Sort, req.Source, req.Framework, req.DeviceType, req.Task, req.Field, req.CanEdgeInfer, req.CanCenterInfer, req.CanTrain, req.IsPreload, req.StartTime, req.EndTime, userGroup)
	if err != nil {
		return nil, total, appErr.APP_ERROR_CODE, err
	}

	return list, total, appErr.SUCCESS_CODE, nil
}

func GetStudioModelVersionCount(modelId int) (int, int, error) {
	var count int
	var errCode int
	var err error

	count, err = dao.GetStudioVersionCommitCountByModelId(modelId)
	if err != nil {
		return count, appErr.APP_ERROR_CODE, err
	}

	return count, errCode, err
}

func GetStudioModelLatestVersion(modelId int) (*entity.StudioModelVersion, int, error) {
	version, err := dao.GetLatestStudioVersionByModelId(modelId)
	if err != nil {
		return nil, appErr.APP_ERROR_CODE, err
	}

	return version, appErr.SUCCESS_CODE, nil
}

func DescribeStudioModel(reqContent proto.DescribeModelReq) (*entity.StudioModel, int, error) {
	model, err := dao.GetStudioModelById(reqContent.ModelId)
	if err != nil {
		return nil, appErr.APP_ERROR_CODE, err
	}
	return model, appErr.SUCCESS_CODE, err
}

func RollbackStudioModelCommit(req *proto.StudioCommitModelReq) (int, error) {
	//check locker
	if req.Context == "" {
		return appErr.ROLLBACK_MODEL_ERROR_CODE, errors.New("rollback needs lock")
	}
	// get model Version
	version, err := dao.GetStudioModelVersionByLockerStatusPrepare(req.Context)
	if err != nil {
		return appErr.APP_ERROR_CODE, err
	}
	err = dao.RollbackStudioVersionById(int(version.ModelID))
	if err != nil {
		return appErr.APP_ERROR_CODE, err
	}

	return appErr.SUCCESS_CODE, nil
}

func DeleteStudioModel(reqContent proto.DeleteModelReq) (int, error) {
	var err error

	// delete model
	err = dao.DeleteStudioModelById(reqContent.ModelId)
	if err != nil {
		return appErr.APP_ERROR_CODE, err
	}

	// delete model versions
	err = dao.DeleteStudioVersionsByModelId(reqContent.ModelId)
	if err != nil {
		return appErr.APP_ERROR_CODE, err
	}

	return appErr.SUCCESS_CODE, err
}

func GetStudioOverview() (int, int, int, int, error) {
	preset, publish, userDefine, err := dao.GetStudioModelOverview()
	if err != nil {
		return preset, publish, userDefine, appErr.APP_ERROR_CODE, err
	}

	return preset, publish, userDefine, appErr.SUCCESS_CODE, nil
}

func GetStudioModelVarietyTypes(types []string) (map[string][]string, int, error) {
	var typeMap = map[string][]string{}
	for _, currType := range types {
		output, err := dao.GetStudioModelVarietyTypes(currType)
		if err != nil {
			return map[string][]string{}, appErr.APP_ERROR_CODE, err
		}
		if output == nil {
			typeMap[currType] = []string{}
		} else {
			typeMap[currType] = output
		}
	}

	return typeMap, appErr.SUCCESS_CODE, nil
}

func GetStudioModelFieldTypes() ([]string, int, error) {
	output, err := dao.GetStudioModelFieldTypes()
	if err != nil {
		return []string{}, appErr.APP_ERROR_CODE, err
	}
	return output, appErr.SUCCESS_CODE, nil
}

func GetStudioModelOtherTypes(field string) (map[string][]string, int, error) {
	output, err := dao.GetStudioModelOtherTypes(field)
	if err != nil {
		return map[string][]string{}, appErr.APP_ERROR_CODE, err
	}
	deviceTyps, err := dao.GetStudioModelRelatedDeviceType(field)
	if err != nil {
		return map[string][]string{}, appErr.APP_ERROR_CODE, err
	}
	output["device_type"] = deviceTyps

	return output, appErr.SUCCESS_CODE, nil
}

func StudioValidateManifest(inferFilePath, manifestFilePath string) (int, error) {
	// read manifest and inference yaml
	var model entity.StudioModel
	var inferenceManifest entity.StudioInference

	// check if manifest sub directory is empty
	var errCode int
	errCode, err := readStudioManifest(&model, manifestFilePath)
	if err != nil {
		logger.Debugln("validate manifest yaml error")
		return errCode, err
	}

	errCode, err = readStudioInferenceManifest(&inferenceManifest, inferFilePath)
	if err != nil {
		logger.Debugln("validate inference yaml error")
		return errCode, err
	}

	return appErr.SUCCESS_CODE, nil
}

func StudioCheckInferenceModel(modelId, modelVersionId int, deviceType string) (bool, bool, int, error) {
	version, err := dao.GetStudioModelVersionById(modelVersionId)
	if err != nil {
		return false, false, appErr.STUDIO_MODEL_NOT_FOUND_ERROR_CODE, err
	}
	inference, err := dao.GetStudioInferenceByVersionlId(modelVersionId)
	if err != nil {
		return false, false, appErr.NO_INFERENCE_ERROR_CODE, err
	}

	localStorage, err := appStorage.Handler.GetTranslatePath(version.StoragePath)
	if err != nil {
		return false, false, appErr.APP_ERROR_CODE, err
	}

	if inference.Center == nil {
		return false, false, appErr.APP_ERROR_CODE, errors.New("inference center does not exists")
	}

	inferPath := localStorage + "/infer/" + inference.Center.ModelPath
	file, err := os.Stat(inferPath)
	if os.IsExist(err) || file == nil {
		return false, false, appErr.APP_ERROR_CODE, err
	}

	logger.Info("device Type: " + deviceType)
	logger.Info("Center Format: " + inference.Center.Format)
	if deviceType == "huawei_npu" {
		if inference.Center.Format == "air" {
			return true, true, appErr.SUCCESS_CODE, nil
		} else if inference.Center.Format == "om" {
			return false, true, appErr.SUCCESS_CODE, nil
		} else {
			return false, false, appErr.APP_ERROR_CODE, errors.New("cannot Inference on current device")
		}
	}

	if deviceType == "nvidia_gpu" {
		if inference.Center.Format == "saved_model" {
			return true, true, appErr.SUCCESS_CODE, nil
		} else if inference.Center.Format == "onnx" {
			return true, true, appErr.SUCCESS_CODE, nil
		} else if inference.Center.Format == "pbtxt" {
			return false, true, appErr.SUCCESS_CODE, nil
		} else {
			return false, false, appErr.APP_ERROR_CODE, errors.New("cannot Inference on current device")
		}
	}

	return false, true, appErr.SUCCESS_CODE, nil
}

func StudioDownLoadModel(modelId, modelVersionId int) (*proto.StudioDownLoadModelRsp, int, error) {
	rsp := proto.StudioDownLoadModelRsp{}
	var err error
	version, err := dao.GetStudioModelVersionById(modelVersionId)
	if err != nil {
		return &rsp, appErr.APP_ERROR_CODE, err
	}
	localStorage, err := appStorage.Handler.GetTranslatePath(version.StoragePath)
	if err != nil {
		return &rsp, appErr.APP_ERROR_CODE, err
	}

	dst := ZipModelToTmp(localStorage)

	f, err := os.Open(dst)
	if err != nil {
		logger.Errorf("cannot open %s: %s", dst, err.Error())
	}
	defer f.Close()

	logger.Info("-------------------------------")
	logger.Info(dst)
	logger.Info("-------------------------------")
	meta := pkg.Metadata{
		"moduleName": "2400",
	}
	logger.Info(f.Name())
	// create an upload from a file.
	upload, err := pkg.NewUploadFromFile(f, meta)
	if err != nil {
		logger.Errorf("init upload file %s error: %s", dst, err.Error())
	}
	// create the tus client.
	url := fmt.Sprintf("%s://%s:%s%s",
		configs.Config.FileServer.Scheme,
		configs.Config.FileServer.Host,
		configs.Config.FileServer.Port,
		configs.Config.FileServer.UploadUrlPrefix)

	ep := url
	client, err := pkg.NewClient(ep, &pkg.TusConfig{
		ChunkSize:           2 * 1024 * 1024,
		Resume:              false,
		OverridePatchMethod: false,
		Store:               nil,
		Header:              make(http.Header),
		HttpClient:          nil,
	})
	if err != nil {
		logger.Errorf("new tus client error: %s", err.Error())
	}
	// create the uploader.
	uploader, err := client.CreateUpload(upload)
	if err != nil {
		logger.Errorf("create uploader error: %s", err.Error())
	}

	// start the uploading process.
	if err := uploader.Upload(); err != nil {
		logger.Errorf("upload %s occurs error: %s", dst, err.Error())
	}
	logger.Printf("upload %s file success", dst)
	urlSplit := strings.Split(uploader.Url(), "/")
	id := urlSplit[len(urlSplit)-1]
	url = fmt.Sprintf("%s://%s:%s%s/%s",
		configs.Config.FileServer.Scheme,
		configs.Config.FileServer.Host,
		configs.Config.FileServer.Port,
		configs.Config.FileServer.InfoUrlPrefix,
		id)
	logger.Info("Check file server info url: " + url)

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return &rsp, appErr.APP_ERROR_CODE, err
	}

	resp, err := client.Do(req)
	bodys, _ := ioutil.ReadAll(resp.Body)
	logger.Debug("File server check response: " + string(bodys))
	if err != nil {
		return &rsp, appErr.APP_ERROR_CODE, err
	}

	var respObject proto.FileServerSuccessResp
	err = json.Unmarshal(bodys, &respObject)
	if err != nil {
		return &rsp, appErr.APP_ERROR_CODE, err
	}

	rsp.Dir = respObject.Data.Dir
	rsp.DownloadLink = respObject.Data.DownloadLink
	rsp.StoragePath = respObject.Data.StoragePath
	return &rsp, appErr.SUCCESS_CODE, nil
}

func StudioModelNameDuplicationCheck(name string) (bool, int, error) {
	return false, 0, nil
}

func ZipModelToTmp(src string) string {
	TarGz(src, src+".tar.gz")
	return src + ".tar.gz"
}

func TarGz(srcDirPath string, destFilePath string) {
	os.RemoveAll(destFilePath)
	fw, err := os.Create(destFilePath)
	utils.HandleError(err)
	defer fw.Close()

	// Gzip writer
	gw := gzip.NewWriter(fw)
	defer gw.Close()

	// Tar writer
	tw := tar.NewWriter(gw)
	defer tw.Close()

	// Check if it's a file or a directory
	f, err := os.Open(srcDirPath)
	utils.HandleError(err)
	fi, err := f.Stat()
	utils.HandleError(err)
	if fi.IsDir() {
		// handle source directory
		fmt.Println("Cerating tar.gz from directory...")
		tarGzDir(srcDirPath, path.Base(srcDirPath), tw)
	} else {
		// handle file directly
		fmt.Println("Cerating tar.gz from " + fi.Name() + "...")
		tarGzFile(srcDirPath, fi.Name(), tw, fi)
	}
	logger.Info("compression completed file: " + destFilePath)
}

// Deal with directories
// if find files, handle them with tarGzFile
// Every recurrence append the base path to the recPath
// recPath is the path inside of tar.gz
func tarGzDir(srcDirPath string, recPath string, tw *tar.Writer) {
	// Open source diretory
	dir, err := os.Open(srcDirPath)
	utils.HandleError(err)
	defer dir.Close()

	// Get file info slice
	fis, err := dir.Readdir(0)
	utils.HandleError(err)
	for _, fi := range fis {
		// Append path
		curPath := srcDirPath + "/" + fi.Name()
		// Check it is directory or file
		if fi.IsDir() {
			// Directory
			// (Directory won't add unitl all subfiles are added)
			fmt.Printf("Adding path...%s\\n", curPath)
			tarGzDir(curPath, recPath+"/"+fi.Name(), tw)
		} else {
			// File
			fmt.Printf("Adding file...%s\\n", curPath)
		}

		tarGzFile(curPath, recPath+"/"+fi.Name(), tw, fi)
	}
}

// Deal with files
func tarGzFile(srcFile string, recPath string, tw *tar.Writer, fi os.FileInfo) {
	if fi.IsDir() {
		// Create tar header
		hdr := new(tar.Header)
		// if last character of header name is '/' it also can be directory
		// but if you don't set Typeflag, error will occur when you untargz
		hdr.Name = recPath + "/"
		hdr.Typeflag = tar.TypeDir
		hdr.Size = 0
		//hdr.Mode = 0755 | c_ISDIR
		hdr.Mode = int64(fi.Mode())
		hdr.ModTime = fi.ModTime()

		// Write hander
		err := tw.WriteHeader(hdr)
		utils.HandleError(err)
	} else {
		// File reader
		fr, err := os.Open(srcFile)
		utils.HandleError(err)
		defer fr.Close()

		// Create tar header
		hdr := new(tar.Header)
		hdr.Name = recPath
		hdr.Size = fi.Size()
		hdr.Mode = int64(fi.Mode())
		hdr.ModTime = fi.ModTime()

		// Write hander
		err = tw.WriteHeader(hdr)
		utils.HandleError(err)

		// Write file data
		_, err = io.Copy(tw, fr)
		utils.HandleError(err)
	}
}

// // 打包成zip文件
// func Zip(src_dir string, zip_file_name string) {

// 	// 预防：旧文件无法覆盖
// 	os.RemoveAll(zip_file_name)

// 	// 创建：zip文件
// 	zipfile, _ := os.Create(zip_file_name)
// 	defer zipfile.Close()

// 	// 打开：zip文件
// 	archive := zip.NewWriter(zipfile)
// 	defer archive.Close()

// 	// 遍历路径信息
// 	filepath.Walk(src_dir, func(path string, info os.FileInfo, _ error) error {

// 		// 如果是源路径，提前进行下一个遍历
// 		if path == src_dir {
// 			return nil
// 		}

// 		// 获取：文件头信息
// 		header, _ := zip.FileInfoHeader(info)
// 		header.Name = strings.TrimPrefix(path, src_dir+`\`)

// 		// 判断：文件是不是文件夹
// 		if info.IsDir() {
// 			header.Name += `/`
// 		} else {
// 			// 设置：zip的文件压缩算法
// 			header.Method = zip.Deflate
// 		}

// 		// 创建：压缩包头部信息
// 		writer, _ := archive.CreateHeader(header)
// 		if !info.IsDir() {
// 			file, _ := os.Open(path)
// 			defer file.Close()
// 			io.Copy(writer, file)
// 		}
// 		return nil
// 	})
// }
