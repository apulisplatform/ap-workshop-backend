/* ******************************************************************************
* 2019 - present Contributed by Apulis Technology (Shenzhen) Co. LTD
*
* This program and the accompanying materials are made available under the
* terms of the MIT License, which is available at
* https://www.opensource.org/licenses/MIT
*
* See the NOTICE file distributed with this work for additional
* information regarding copyright ownership.
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations
* under the License.
*
* SPDX-License-Identifier: MIT
******************************************************************************/
package service

import (
	"apworkshop/internal/appErr"
	"apworkshop/internal/entity"
	"apworkshop/internal/proto"
	"errors"
)

func GetModelVersionListByModel(reqContent proto.GetModelVersionListByModelReq) ([]entity.ModelVersion, int, error) {
	list, err := entity.GetModelVersionListByModel(reqContent.ModelId, reqContent.PageNum, reqContent.PageSize)
	if err != nil {
		return nil, appErr.APP_ERROR_CODE, err
	}

	return list, appErr.SUCCESS_CODE, nil
}

func DescribeModelVersion(reqContent proto.DescribeModelVersionReq) (*entity.ModelVersion, int, error) {
	modelVersion, err := entity.GetModelVersion(reqContent.ModelVersionId)
	if err != nil {
		if errors.Is(err, appErr.RECORD_NOT_FOUND_ERROR) {
			return nil, appErr.RECORD_NOT_FOUND_ERROR_CODE, err
		}
		return nil, appErr.APP_ERROR_CODE, err
	}

	return modelVersion, appErr.SUCCESS_CODE, nil
}

func DeleteModelVersion(reqContent proto.DeleteModelVersionReq) (int, error) {
	err := entity.DeleteModelVersion(reqContent.ModelVersionId)
	if err != nil {
		return appErr.APP_ERROR_CODE, err
	}

	return appErr.SUCCESS_CODE, nil
}

func GetModelLatestVersion(modelId int) (*entity.ModelVersion, int, error) {
	version, err := entity.GetLatestVersionByModelId(modelId)
	if err != nil {
		return nil, appErr.APP_ERROR_CODE, err
	}

	return version, appErr.SUCCESS_CODE, nil
}
