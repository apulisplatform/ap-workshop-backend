/* ******************************************************************************
* 2019 - present Contributed by Apulis Technology (Shenzhen) Co. LTD
*
* This program and the accompanying materials are made available under the
* terms of the MIT License, which is available at
* https://www.opensource.org/licenses/MIT
*
* See the NOTICE file distributed with this work for additional
* information regarding copyright ownership.
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations
* under the License.
*
* SPDX-License-Identifier: MIT
******************************************************************************/
package service

import (
	"apworkshop/internal/appErr"
	"apworkshop/internal/common"
	"apworkshop/internal/configs"
	"apworkshop/internal/dao"
	"apworkshop/internal/entity"
	"apworkshop/internal/proto"
	appStorage "apworkshop/internal/storage"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"strings"
)

func DeleteStudioModelVersion(reqContent proto.DeleteModelVersionReq) (int, error) {
	version, err := dao.GetStudioModelVersion(reqContent.ModelVersionId)
	if err != nil {
		return appErr.APP_ERROR_CODE, err
	}

	err = dao.DeleteStudioModelVersion(reqContent.ModelVersionId)
	if err != nil {
		return appErr.APP_ERROR_CODE, err
	}

	localStorage, err := appStorage.Handler.GetTranslatePath(version.StoragePath)
	if err != nil {
		return appErr.APP_ERROR_CODE, err
	}
	err = os.RemoveAll(localStorage)
	if err != nil {
		return appErr.APP_ERROR_CODE, err
	}

	count, err := dao.GetStudioModelVersionCount(version.ModelID)
	if err != nil {
		return appErr.APP_ERROR_CODE, err
	}
	if count == 0 {
		err = dao.DeleteStudioModelById(version.ModelID)
		if err != nil {
			return appErr.APP_ERROR_CODE, err
		}
	}
	if version.RepoId != "" && version.CommitId != "" {
		err = unRefRepo(version)
		if err != nil {
			return appErr.APP_ERROR_CODE, err
		}
	}
	return appErr.SUCCESS_CODE, nil
}

func DescribeStudioModelVersion(reqContent proto.DescribeModelVersionReq) (*entity.StudioModelVersion, int, error) {
	modelVersion, err := dao.GetStudioModelVersion(reqContent.ModelVersionId)
	if err != nil {
		if errors.Is(err, appErr.RECORD_NOT_FOUND_ERROR) {
			return nil, appErr.RECORD_NOT_FOUND_ERROR_CODE, err
		}
		return nil, appErr.APP_ERROR_CODE, err
	}

	return modelVersion, appErr.SUCCESS_CODE, nil
}

func GetModelVersionYaml(modelVersionId int) (*proto.GetStudioModelVersionYamlRsp, int, error) {
	modelVersion, err := dao.GetStudioModelVersion(modelVersionId)
	if err != nil {
		if errors.Is(err, appErr.RECORD_NOT_FOUND_ERROR) {
			return nil, appErr.RECORD_NOT_FOUND_ERROR_CODE, err
		}
		return nil, appErr.APP_ERROR_CODE, err
	}

	localPath, err := appStorage.Handler.GetTranslatePath(modelVersion.StoragePath)
	if err != nil {
		return nil, appErr.APP_ERROR_CODE, err
	}

	manifest, err := readManifestYaml(localPath + "/manifest.yaml")
	if err != nil {
		logger.Debugln("read manifest yaml error")
	}

	infer, err := readInferYaml(localPath + "/infer.yaml")
	if err != nil {
		logger.Debugln("read infer yaml error")
	}

	rsp := proto.GetStudioModelVersionYamlRsp{ManifestYaml: manifest, InferYaml: infer}

	return &rsp, appErr.SUCCESS_CODE, nil
}

func GetStudioModelVersionListByModel(reqContent proto.GetStudioModelVersionListByModelReq) ([]entity.StudioModelVersion, int, error) {
	list, err := dao.GetStudioModelVersionListByModel(reqContent.ModelId, reqContent.PageNum, reqContent.PageSize, reqContent.Sort)
	if err != nil {
		return nil, appErr.APP_ERROR_CODE, err
	}

	return list, appErr.SUCCESS_CODE, nil
}

func LockStudioModelVersion(modelVersionId int, context string) (*entity.StudioModelVersion, int, error) {
	version, err := dao.GetStudioModelVersionById(modelVersionId)
	if err != nil {
		return version, appErr.STUDIO_MODEL_NOT_FOUND_ERROR_CODE, err
	}
	version.Locker = context
	version.Status = common.MODEL_VERSION_STATUS_LOCK
	version, err = dao.UpdateStudioModelVersion(version)
	if err != nil {
		return version, appErr.REF_RESOURCE_UNFOUND, err
	}

	return version, appErr.SUCCESS_CODE, err
}

func UnlockStudioModelVersion(modelVersionId int, context string) (*entity.StudioModelVersion, int, error) {
	version, err := dao.GetStudioModelVersionByLockerStatusLock(context)
	if err != nil {
		return version, appErr.STUDIO_MODEL_NOT_FOUND_ERROR_CODE, err
	}
	version.RegisterCtx = context
	version.Status = common.MODEL_VERSION_STATUS_FREE
	version, err = dao.UpdateStudioModelVersion(version)
	if err != nil {
		return version, appErr.REF_RESOURCE_UNFOUND, err
	}

	return version, appErr.SUCCESS_CODE, err
}

func unRefRepo(version *entity.StudioModelVersion) error {
	scheme := configs.Config.AiLab.Scheme
	host := configs.Config.AiLab.Host
	port := configs.Config.AiLab.Port
	urlPrefix := configs.Config.AiLab.UnrefUrlPrefix

	urlString := fmt.Sprintf("%s://%s:%s%s/%s", scheme, host, port, urlPrefix, version.Bind)
	logger.Info("UnRef AI lab url: " + urlString)

	data := url.Values{}
	data.Set("repoId", version.RepoId)

	req, err := http.NewRequest("DELETE", urlString, strings.NewReader(data.Encode()))
	if err != nil {
		return err
	}
	// check for ref responses
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	resp, err := client.Do(req)
	bodys, _ := ioutil.ReadAll(resp.Body)
	logger.Debug("Ref repo response: " + string(bodys))
	if err != nil {
		return err
	}

	return nil
}

func readManifestYaml(manifestPath string) (string, error) {
	_, err := os.Stat(manifestPath)
	if err != nil {
		if os.IsExist(err) {
			logger.Debugln("manifest yaml not found")
		} else {
			logger.Debugln("unknown manifest yaml error:" + err.Error())
		}
		return "", err
	}
	yamlfile, err := ioutil.ReadFile(manifestPath)
	if err != nil {
		return "", err
	}
	return string(yamlfile), nil
}

func readInferYaml(inferPath string) (string, error) {
	_, err := os.Stat(inferPath)
	if err != nil {
		if os.IsExist(err) {
			logger.Debugln("Inference yaml not found")
		} else {
			logger.Debugln("unknown inference yaml error:" + err.Error())
		}
		return "", err
	}
	yamlfile, err := ioutil.ReadFile(inferPath)
	if err != nil {
		return "", err
	}
	return string(yamlfile), nil
}
