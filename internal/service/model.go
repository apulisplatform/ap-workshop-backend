/* ******************************************************************************
* 2019 - present Contributed by Apulis Technology (Shenzhen) Co. LTD
*
* This program and the accompanying materials are made available under the
* terms of the MIT License, which is available at
* https://www.opensource.org/licenses/MIT
*
* See the NOTICE file distributed with this work for additional
* information regarding copyright ownership.
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations
* under the License.
*
* SPDX-License-Identifier: MIT
******************************************************************************/
package service

import (
	"apworkshop/internal/appErr"
	"apworkshop/internal/common"
	"apworkshop/internal/databases"
	"apworkshop/internal/entity"
	"apworkshop/internal/loggers"
	"apworkshop/internal/proto"
	appStorage "apworkshop/internal/storage"
	validators "apworkshop/internal/validator"
	"archive/tar"
	"archive/zip"
	"compress/gzip"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"path"
	"path/filepath"
	"regexp"
	"strconv"
	"strings"
	"syscall"
	"time"

	storageComm "github.com/apulis/sdk/go-utils/storage/comm"
	"github.com/gofrs/uuid"

	"github.com/apulis/sdk/go-utils/storage"

	"gopkg.in/yaml.v2"
)

var logger = loggers.LogInstance()
var TimeoutSeconds int

func DepressModelPack(filePath, fileName string) (string, int, error) {
	var targetDir string

	// decompress file
	// check file format
	checkZipAndTarRe := `.*\.(zip|tar\.gz)$`
	r := regexp.MustCompile(checkZipAndTarRe)
	if !(r.MatchString(fileName)) {
		return targetDir, appErr.APP_ERROR_CODE, errors.New("can't support file format")
	}
	u1, err := uuid.NewV4()
	if err != nil {
		return targetDir, appErr.APP_ERROR_CODE, err
	}
	targetDir = common.MODEL_PACK_TEMP_DIR + "/" + u1.String() + "/"
	err = os.MkdirAll(targetDir, 0777)
	if err != nil {
		return targetDir, appErr.APP_ERROR_CODE, err
	}
	os.Chmod(targetDir, 0777)
	logger.Debugln("================== depress ======================")
	isZipRe := `.*\.zip$`
	r = regexp.MustCompile(isZipRe)
	if r.MatchString(fileName) {
		err = zipHandler(filePath, targetDir)
		if err != nil {
			return targetDir, appErr.APP_ERROR_CODE, err
		}
	} else {
		tarGzHandler(filePath, targetDir)
		if err != nil {
			return targetDir, appErr.APP_ERROR_CODE, err
		}
	}
	logger.Debugln("================== depress end ======================")

	return targetDir, appErr.SUCCESS_CODE, nil
}

func CreateModelsByPack(scope, modelName, modelDescription string, targetDir string) (int, error) {

	// === tidy directory first,because it's hard to operate remote dir in prepare and commit process
	// read manifest
	var manifest entity.ModelDbStruct
	errCode, err := readManifest(&manifest, targetDir+common.MANIFEST_SUB_DIR+"/manifest.yaml")
	if err != nil {
		logger.Debugln("read manifest yaml error")
		return errCode, err
	}
	// tidy model directory(move model to models directory)
	if manifest.Train.PretrainedModel != "" {
		pretrainedModelDir := "pretrained_model"
		pretrainedModelPath := manifest.Train.PretrainedModel
		tempStringArray := strings.Split(pretrainedModelPath, "/")
		pretrainedModelFileName := tempStringArray[len(tempStringArray)-1]
		err = os.MkdirAll(targetDir+"/"+pretrainedModelDir+"/", 0755)
		if err != nil {
			return appErr.APP_ERROR_CODE, err
		}
		err = os.Rename(targetDir+"/code/"+pretrainedModelPath, targetDir+"/"+pretrainedModelDir+"/"+pretrainedModelFileName)
		if err != nil {
			return appErr.APP_ERROR_CODE, err
		}
	}
	// ======

	// get remote directory
	if modelName == "" {
		modelName = manifest.Name
	}
	if modelDescription == "" {
		modelDescription = manifest.Description
	}
	// TODO: use file md5 as context
	u1, err := uuid.NewV4()
	if err != nil {
		return appErr.APP_ERROR_CODE, err
	}
	_, _, dstPath, locker, errCode, err := PrepareModel(u1.String(), scope, modelName, modelDescription, true)
	if err != nil {
		return errCode, err
	}
	// save to remote
	err = saveToLoader(targetDir, dstPath)
	if err != nil {
		return appErr.APP_ERROR_CODE, err
	}
	errCode, err = CommitModel(&proto.CommitModelReq{
		Lock: locker,
	})
	if err != nil {
		return errCode, err
	}

	return appErr.SUCCESS_CODE, nil
}

func zipHandler(src, dest string) error {
	var filenames []string

	r, err := zip.OpenReader(src)
	if err != nil {
		return err
	}
	defer r.Close()

	for _, f := range r.File {

		// Store filename/path for returning and using later on
		fpath := filepath.Join(dest, f.Name)

		// Check for ZipSlip. More Info: http://bit.ly/2MsjAWE
		if !strings.HasPrefix(fpath, filepath.Clean(dest)+string(os.PathSeparator)) {
			return fmt.Errorf("%s: illegal file path", fpath)
		}

		filenames = append(filenames, fpath)

		if f.FileInfo().IsDir() {
			// Make Folder
			os.MkdirAll(fpath, os.ModePerm)
			continue
		}

		// Make File
		if err = os.MkdirAll(filepath.Dir(fpath), os.ModePerm); err != nil {
			return err
		}

		outFile, err := os.OpenFile(fpath, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, f.Mode())
		if err != nil {
			return err
		}

		rc, err := f.Open()
		if err != nil {
			return err
		}

		_, err = io.Copy(outFile, rc)

		// Close the file without defer to close before next iteration of loop
		outFile.Close()
		rc.Close()

		if err != nil {
			return err
		}
	}
	return nil
}

func tarGzHandler(src, dst string) (err error) {
	// 打开准备解压的 tar 包
	fr, err := os.Open(src)
	if err != nil {
		return
	}
	defer fr.Close()

	// 将打开的文件先解压
	gr, err := gzip.NewReader(fr)
	if err != nil {
		return
	}
	defer gr.Close()

	// 通过 gr 创建 tar.Reader
	tr := tar.NewReader(gr)

	// 现在已经获得了 tar.Reader 结构了，只需要循环里面的数据写入文件就可以了
	for {
		hdr, err := tr.Next()

		switch {
		case err == io.EOF:
			return nil
		case err != nil:
			return err
		case hdr == nil:
			continue
		}

		// 处理下保存路径，将要保存的目录加上 header 中的 Name
		// 这个变量保存的有可能是目录，有可能是文件，所以就叫 FileDir 了……
		dstFileDir := filepath.Join(dst, hdr.Name)

		// 根据 header 的 Typeflag 字段，判断文件的类型
		switch hdr.Typeflag {
		case tar.TypeDir: // 如果是目录时候，创建目录
			// 判断下目录是否存在，不存在就创建
			if b := ExistDir(dstFileDir); !b {
				// 使用 MkdirAll 不使用 Mkdir ，就类似 Linux 终端下的 mkdir -p，
				// 可以递归创建每一级目录
				if err := os.MkdirAll(dstFileDir, 0775); err != nil {
					return err
				}
			}
		case tar.TypeReg: // 如果是文件就写入到磁盘
			// 创建一个可以读写的文件，权限就使用 header 中记录的权限
			// 因为操作系统的 FileMode 是 int32 类型的，hdr 中的是 int64，所以转换下
			file, err := os.OpenFile(dstFileDir, os.O_CREATE|os.O_RDWR, os.FileMode(hdr.Mode))
			if err != nil {
				return err
			}
			n, err := io.Copy(file, tr)
			if err != nil {
				return err
			}
			// 将解压结果输出显示
			fmt.Printf("成功解压： %s , 共处理了 %d 个字符\n", dstFileDir, n)

			// 不要忘记关闭打开的文件，因为它是在 for 循环中，不能使用 defer
			// 如果想使用 defer 就放在一个单独的函数中
			file.Close()
		}
	}
}

// 判断目录是否存在
func ExistDir(dirname string) bool {
	fi, err := os.Stat(dirname)
	return (err == nil || os.IsExist(err)) && fi.IsDir()
}

// func zipHandler(srcPath, destPath string) error {
// 	// Open a zip archive for reading.
// 	reader, err := zip.OpenReader(srcPath)
// 	if err != nil {
// 		return err
// 	}
// 	defer reader.Close()

// 	// Iterate through the files in the archive,
// 	// printing some of their contents.
// 	for _, file := range reader.File {
// 		path := filepath.Join(destPath, file.Name)

// 		// create directory
// 		if file.FileInfo().IsDir() {
// 			if err := os.MkdirAll(path, file.Mode()); err != nil {
// 				return err
// 			}
// 			continue
// 		}

// 		// create file
// 		fr, err := file.Open()
// 		if err != nil {
// 			return err
// 		}

// 		fw, err := os.OpenFile(path, os.O_CREATE|os.O_RDWR|os.O_TRUNC, file.Mode())
// 		if err != nil {
// 			return err
// 		}

// 		n, err := io.Copy(fw, fr)
// 		if err != nil {
// 			return err
// 		}

// 		// print output
// 		logger.Debugf("depress file: %s ，data size: %d bytes", path, n)

// 		fw.Close()
// 		fr.Close()
// 	}
// 	return nil
// }

// func tarHandler(srcPath, destPath string) error {
// 	fr, err := os.Open(srcPath)
// 	if err != nil {
// 		return err
// 	}
// 	defer fr.Close()

// 	gr, err := gzip.NewReader(fr)
// 	if err != nil {
// 		return err
// 	}
// 	defer gr.Close()

// 	tarReader := tar.NewReader(gr)

// 	for {
// 		header, err := tarReader.Next()

// 		switch {
// 		case err == io.EOF:
// 			return nil
// 		case err != nil:
// 			return err
// 		case header == nil:
// 			continue
// 		}

// 		dstFileDir := filepath.Join(destPath, header.Name)

// 		switch header.Typeflag {
// 		case tar.TypeDir:
// 			if err := os.MkdirAll(dstFileDir, 0775); err != nil {
// 				return err
// 			}
// 		case tar.TypeReg:
// 			file, err := os.OpenFile(dstFileDir, os.O_CREATE|os.O_RDWR, os.FileMode(header.Mode))
// 			defer file.Close()
// 			if err != nil {
// 				return err
// 			}
// 			_, err = io.Copy(file, tarReader)
// 			if err != nil {
// 				return err
// 			}
// 		}
// 	}
// }

func tarHandler(srcTar string, dstDir string) (err error) {
	// 清理路径字符串
	dstDir = path.Clean(dstDir) + string(os.PathSeparator)

	// 打开要解包的文件
	fr, er := os.Open(srcTar)
	if er != nil {
		return er
	}
	defer fr.Close()

	// 创建 tar.Reader，准备执行解包操作
	tr := tar.NewReader(fr)

	// 遍历包中的文件
	for hdr, er := tr.Next(); er != io.EOF; hdr, er = tr.Next() {
		if er != nil {
			return er
		}

		// 获取文件信息
		fi := hdr.FileInfo()

		// 获取绝对路径
		dstFullPath := dstDir + hdr.Name

		if hdr.Typeflag == tar.TypeDir {
			// 创建目录
			os.MkdirAll(dstFullPath, fi.Mode().Perm())
			// 设置目录权限
			os.Chmod(dstFullPath, fi.Mode().Perm())
		} else {
			// 创建文件所在的目录
			os.MkdirAll(path.Dir(dstFullPath), os.ModePerm)
			// 将 tr 中的数据写入文件中
			if er := unTarFile(dstFullPath, tr); er != nil {
				return er
			}
			// 设置文件权限
			os.Chmod(dstFullPath, fi.Mode().Perm())
		}
	}
	return nil
}

// 因为要在 defer 中关闭文件，所以要单独创建一个函数
func unTarFile(dstFile string, tr *tar.Reader) error {
	// 创建空文件，准备写入解包后的数据
	fw, er := os.Create(dstFile)
	if er != nil {
		return er
	}
	defer fw.Close()

	// 写入解包后的数据
	_, er = io.Copy(fw, tr)
	if er != nil {
		return er
	}

	return nil
}

func readManifest(manifest *entity.ModelDbStruct, manifestPath string) (int, error) {
	// check if manifest exist
	logger.Debugln(manifestPath)
	_, err := os.Stat(manifestPath) //os.Stat获取文件信息
	if err != nil {
		if os.IsExist(err) {
			return appErr.MANIFEST_READING_ERROR_CODE, errors.New("not found manifest config")
		}
		return appErr.MANIFEST_READING_ERROR_CODE, err
	}

	// read manifest
	yamlfile, err := ioutil.ReadFile(manifestPath)
	if err != nil {
		return appErr.MANIFEST_READING_ERROR_CODE, err
	}
	err = yaml.Unmarshal(yamlfile, manifest)
	if err != nil {
		return appErr.MANIFEST_READING_ERROR_CODE, err
	}
	logger.Debugln(manifest)
	errCode, err := validators.ValidateManifest(manifest)
	if err != nil {
		return errCode, err
	}

	if manifest.Train != nil {
		manifest.Ability = append(manifest.Ability, entity.MODEL_ABILITY_TRAIN)
	}

	return appErr.SUCCESS_CODE, nil
}

func readInferenceManifest(inference *entity.InferStruct, manifestPath string) (int, error) {
	// check if manifest exist
	_, err := os.Stat(manifestPath) //os.Stat获取文件信息
	if err != nil {
		if os.IsExist(err) {
			return appErr.INFERENCE_READING_ERROR_CODE, errors.New("not found inference config")
		}
		return appErr.INFERENCE_READING_ERROR_CODE, err
	}

	// read manifest
	yamlfile, err := ioutil.ReadFile(manifestPath)
	if err != nil {
		return appErr.INFERENCE_READING_ERROR_CODE, err
	}
	err = yaml.Unmarshal(yamlfile, inference)
	if err != nil {
		return appErr.INFERENCE_READING_ERROR_CODE, err
	}
	var inferDb entity.InferDbStruct
	err = yaml.Unmarshal(yamlfile, &inferDb)
	if err != nil {
		return appErr.INFERENCE_READING_ERROR_CODE, err
	}
	inference.InferDbStruct = inferDb
	logger.Debugln("=== found inference in :" + manifestPath + ", show below")
	logger.Debugln(inference)
	if inference.Center != nil {
		logger.Debugln(inference.Center)
	}
	if inference.Edge != nil {
		logger.Debugln(inference.Edge)
	}
	logger.Debugln("=== found inference manifest")
	errCode, err := validators.ValidateInferenceManifest(inference)
	if err != nil {
		return errCode, err
	}

	return appErr.SUCCESS_CODE, nil
}

func saveToLoader(src, dst string) error {
	var schemaType string
	var err error
	///////////// now storage sdk can not handle pvc
	srcMatch, err := regexp.MatchString("^pvc://.*", src)
	if err != nil {
		return err
	}
	dstMatch, err := regexp.MatchString("^pvc://.*", dst)
	if err != nil {
		return err
	}
	if err != nil {
		return err
	}
	if srcMatch || dstMatch {
		var srcRealPath string
		var dstRealPath string
		if srcMatch {
			srcRealPath, err = appStorage.Handler.GetTranslatePath(src)
			if err != nil {
				return err
			}
		} else {
			srcRealPath = src
		}
		if dstMatch {
			dstRealPath, err = appStorage.Handler.GetTranslatePath(dst)
			if err != nil {
				return err
			}
		} else {
			dstRealPath = dst
		}
		err = copyDirectory(srcRealPath, dstRealPath)
		if err != nil {
			return err
		}
		return nil
	}
	/////////////

	schemaType, err = storageComm.GetSchema(dst)
	if err != nil {
		return err
	}

	s, err := storage.New(schemaType)
	if err != nil {
		return err
	}

	err = s.Handler.Init(storageComm.HandlerCfg{
		Debug: false,
	})
	if err != nil {
		return err
	}

	logger.Debugln("copy local dir")
	err = s.Handler.CopyFromLocalDir(src, dst)
	if err != nil {
		return err
	}
	return nil
}

func copyDirectory(src, dst string) error {

	if src == dst {
		return nil
	}

	entries, err := ioutil.ReadDir(src)
	if err != nil {
		return err
	}
	for _, entry := range entries {
		sourcePath := filepath.Join(src, entry.Name())
		destPath := filepath.Join(dst, entry.Name())

		fileInfo, err := os.Stat(sourcePath)
		if err != nil {
			return err
		}

		switch fileInfo.Mode() & os.ModeType {
		case os.ModeDir:
			if err := os.MkdirAll(destPath, 0777); err != nil {
				return fmt.Errorf("failed to create directory: '%s', error: '%s'", destPath, err.Error())
			}
			if err := copyDirectory(sourcePath, destPath); err != nil {
				return err
			}
		default:
			source, err := os.Open(sourcePath)
			if err != nil {
				return err
			}
			defer source.Close()
			destination, err := os.Create(destPath)
			if err != nil {
				return err
			}
			defer destination.Close()
			_, err = io.Copy(destination, source)
			if err != nil {
				return err
			}
		}
	}
	return nil
}

// create new model and model version, it will detect request context, same context will do nothing
func PrepareModel(ctx, scope, modelName, modelDescription string, isPreload bool) (*entity.ModelDbStruct, *entity.ModelVersion, string, string, int, error) {
	var contextCount int64
	var modelVersion entity.ModelVersion
	var versionResult *entity.ModelVersion

	// check ctx not same
	err := databases.Db.
		Model(&entity.ModelVersion{}).
		Where("register_ctx = ?", ctx).
		Count(&contextCount).
		Error
	if err != nil {
		return nil, nil, "", "", appErr.QUERY_DB_ERROR_CODE, err
	}
	if contextCount != 0 {
		logger.Debugln("context found")
		err := databases.Db.
			Where("register_ctx = ?", ctx).
			First(&modelVersion).
			Error
		if err != nil {
			return nil, nil, "", "", appErr.QUERY_DB_ERROR_CODE, err
		}
		versionResult = &modelVersion
	}

	// generate a path for the version
	manifest := entity.ModelDbStruct{
		Scope:       scope,
		Name:        modelName,
		Description: modelDescription,
	}

	// check if model exist, if not, create a new model, else create a new version
	modelDb, err := entity.GetModelByNameAndScope(modelName, scope)
	if err != nil {
		return nil, nil, "", "", appErr.QUERY_DB_ERROR_CODE, err
	}
	if modelDb == nil {
		// save to db
		modelDb, err = entity.CreateModel(&manifest, isPreload)
		if err != nil {
			return nil, nil, "", "", appErr.CREATE_NEW_MODEL_ERROR_CODE, err
		}
	}

	if contextCount == 0 {
		versionResult, err = entity.CreateNewVersionForModel(ctx, modelDb.ID)
		if err != nil {
			return nil, nil, "", "", appErr.CREATE_NEW_MODEL_ERROR_CODE, err
		}
		fileDir := common.MODEL_STORAGE_DIR
		uploadPath, err := appStorage.Handler.GetUploadPath(int(modelDb.ID), int(versionResult.ID), fileDir)
		if err != nil {
			return nil, nil, "", "", appErr.STORAGE_COMPONENT_RELATE_ERROR_CODE, err
		}
		localPath, err := appStorage.Handler.GetTranslatePath(uploadPath)
		if err != nil {
			return nil, nil, "", "", appErr.STORAGE_COMPONENT_RELATE_ERROR_CODE, err
		}
		err = os.RemoveAll(localPath)
		if err != nil {
			return nil, nil, "", "", appErr.STORAGE_COMPONENT_RELATE_ERROR_CODE, err
		}
		oldMask := syscall.Umask(0)
		err = os.MkdirAll(localPath, 0777)
		syscall.Umask(oldMask)
		if err != nil {
			return nil, nil, "", "", appErr.STORAGE_COMPONENT_RELATE_ERROR_CODE, err
		}
		versionResult.StoragePath = uploadPath
		_, err = entity.UpdateModelVersion(versionResult)
		if err != nil {
			return nil, nil, "", "", appErr.CREATE_NEW_MODEL_ERROR_CODE, err
		}
	}

	return modelDb, versionResult, versionResult.StoragePath, versionResult.Locker, 0, nil
}

func CommitModel(req *proto.CommitModelReq) (int, error) {
	//check locker
	if req.Lock == "" {
		return appErr.PARAMETER_ERROR_CODE, errors.New("commit needs lock")
	}
	// get model Version
	// TODO: now can get by context
	version, err := entity.GetModelVersionByLocker(req.Lock)
	if err != nil {
		return appErr.QUERY_DB_ERROR_CODE, err
	}
	// check if timeout
	nowTimeStamp := time.Now().Unix()
	if int(nowTimeStamp-version.CreatedAt.Unix()) > TimeoutSeconds*1000 {
		return appErr.COMMIT_TIMEOUT_ERROR_CODE, errors.New("commit timeout")
	}
	// check if commit already
	if version.Status == common.MODEL_VERSION_STATUS_FREE {
		return appErr.SUCCESS_CODE, nil
	}
	// get path
	storagePath := version.StoragePath
	// download to local
	tempDir := "/tmp/apworkshop/commitcheck/" + strconv.Itoa(int(version.ModelID)) + "/" + strconv.Itoa(int(version.ID))
	err = os.MkdirAll(tempDir, 0755)
	if err != nil {
		return appErr.STORAGE_COMPONENT_RELATE_ERROR_CODE, err
	}
	err = saveToLoader(storagePath, tempDir)
	if err != nil {
		return appErr.STORAGE_COMPONENT_RELATE_ERROR_CODE, err
	}
	/////////
	// read manifest and inference yaml
	var model entity.ModelDbStruct
	var inferenceManifest entity.InferStruct

	// check if manifest sub directory is empty
	var errCode int
	if common.MANIFEST_SUB_DIR != "/" {
		errCode, err = readManifest(&model, tempDir+common.MANIFEST_SUB_DIR+"/manifest.yaml")
	}

	if err != nil {
		logger.Debugln("read manifest yaml error")
		return errCode, err
	}
	canInfer := false
	_, err = os.Stat(tempDir + common.MANIFEST_SUB_DIR + "/infer.yaml")
	if err != nil {
		if os.IsExist(err) {
			logger.Debugln("Inference yaml not found")
		} else {
			logger.Debugln("unknown inference yaml error:" + err.Error())
		}
	} else {
		canInfer = true
	}
	if canInfer {
		errCode, err = readInferenceManifest(&inferenceManifest, tempDir+common.MANIFEST_SUB_DIR+"/infer.yaml")
		if err != nil {
			logger.Debugln("read inference yaml error")
			return errCode, err
		}
	}

	// user can modify model name prior to manifest,so before update, check model name has not been set in prepare process.
	checkModel, err := entity.GetModelById(int(version.ModelID))
	if err != nil {
		return appErr.QUERY_DB_ERROR_CODE, err
	}
	if checkModel.Name != "" {
		model.Name = ""
	}
	if checkModel.Description != "" {
		model.Description = ""
	}
	// save manifest and inferenceManifest
	model.ID = version.ModelID
	_, err = entity.UpdateModel(&model)
	if err != nil {
		return appErr.QUERY_DB_ERROR_CODE, err
	}
	if canInfer {
		inferenceManifest.ModelVersionID = version.ID
		err = entity.CreateInference(int(model.ID), int(version.ID), &inferenceManifest)
		if err != nil {
			return appErr.QUERY_DB_ERROR_CODE, err
		}
	}

	//update model version info
	version.Extends = model.Extends
	version.Tags = inferenceManifest.Tags
	version.Train = model.Train
	version.Eval = model.Eval
	entity.UpdateModelVersion(version)

	// TODO: now can use context
	err = entity.CommitVersion(req.Lock)
	if err != nil {
		return appErr.QUERY_DB_ERROR_CODE, err
	}
	return appErr.SUCCESS_CODE, nil
}

func RollbackModelCommit(req *proto.CommitModelReq) (int, error) {
	//check locker
	if req.Lock == "" {
		return appErr.ROLLBACK_MODEL_ERROR_CODE, errors.New("rollback needs lock")
	}
	// get model Version
	version, err := entity.GetModelVersionByLocker(req.Lock)
	if err != nil {
		return appErr.APP_ERROR_CODE, err
	}
	err = entity.RollbackVersionById(int(version.ModelID))
	if err != nil {
		return appErr.APP_ERROR_CODE, err
	}

	return appErr.SUCCESS_CODE, nil
}

func GetModelList(req *proto.GetModelListReq) ([]entity.ModelDbStruct, int, int, error) {
	var total int
	list, total, err := entity.GetModelList(req.PageNum, req.PageSize, req.Scope, req.Name, req.Platform, req.Type, req.Sort, req.CanInfer, req.CanTrain, req.IsPreload)
	if err != nil {
		return nil, total, appErr.APP_ERROR_CODE, err
	}

	return list, total, appErr.SUCCESS_CODE, nil
}

func DescribeModel(reqContent proto.DescribeModelReq) (*entity.ModelDbStruct, int, error) {
	model, err := entity.GetModelById(reqContent.ModelId)
	if err != nil {
		return nil, appErr.APP_ERROR_CODE, err
	}
	return model, appErr.SUCCESS_CODE, err
}

func DeleteModel(reqContent proto.DeleteModelReq) (int, error) {
	var err error

	// delete model
	err = entity.DeleteModelById(reqContent.ModelId)
	if err != nil {
		return appErr.APP_ERROR_CODE, err
	}

	// delete model versions
	err = entity.DeleteVersionsByModelId(reqContent.ModelId)
	if err != nil {
		return appErr.APP_ERROR_CODE, err
	}

	return appErr.SUCCESS_CODE, err
}

func GetModelVersionCount(modelId int) (int, int, error) {
	var count int
	var errCode int
	var err error

	count, err = entity.GetVersionCommitCountByModelId(modelId)
	if err != nil {
		return count, appErr.APP_ERROR_CODE, err
	}

	return count, errCode, err
}

func GetModelTypes() ([]string, int, error) {
	var types []string

	types, err := entity.GetModelTypes()
	if err != nil {
		return nil, appErr.APP_ERROR_CODE, err
	}

	return types, appErr.SUCCESS_CODE, nil
}
