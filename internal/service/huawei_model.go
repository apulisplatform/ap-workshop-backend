/* ******************************************************************************
* 2019 - present Contributed by Apulis Technology (Shenzhen) Co. LTD
*
* This program and the accompanying materials are made available under the
* terms of the MIT License, which is available at
* https://www.opensource.org/licenses/MIT
*
* See the NOTICE file distributed with this work for additional
* information regarding copyright ownership.
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations
* under the License.
*
* SPDX-License-Identifier: MIT
******************************************************************************/
package service

import (
	"apworkshop/internal/appErr"
	"apworkshop/internal/entity"
	"errors"
	"io/ioutil"
	"os"

	"gopkg.in/yaml.v2"
)

func HuaweiModelPreHandle(targetDir string) (int, error) {
	configPath := targetDir + "/on_platform/plat_cfg.yaml"
	_, err := os.Stat(configPath)
	if err != nil {
		return appErr.APP_ERROR_CODE, errors.New("not found huawei config in pre process")
	}

	// read manifest
	var manifest entity.HuaweiManifest
	yamlfile, err := ioutil.ReadFile(configPath)
	if err != nil {
		return appErr.APP_ERROR_CODE, err
	}
	err = yaml.Unmarshal(yamlfile, &manifest)
	if err != nil {
		return appErr.APP_ERROR_CODE, err
	}
	logger.Debugln("============ huawei config below ========")
	logger.Debugln(manifest)
	logger.Debugln("============ huawei config above ========")

	// generate train params
	var trainUserParams []entity.ParamsStruct
	for _, userParam := range manifest.Train.CustomPara {
		pramaUnit := &entity.ParamsStruct{
			Name:     userParam.Name,
			Type:     userParam.Type,
			Default:  userParam.Default,
			Desc:     userParam.Desc.Zh,
			Editable: userParam.Visible,
		}
		trainUserParams = append(trainUserParams, *pramaUnit)
	}
	var trainSysParams []entity.SystemParamStruct
	for _, sysParam := range manifest.Train.CommPara {
		if sysParam.PlatName == "train_output" {
			sysParam.PlatName = "output_path"
		}
		pramaUnit := &entity.SystemParamStruct{
			Name: sysParam.PlatName,
			Arg:  sysParam.Name,
		}
		trainSysParams = append(trainSysParams, *pramaUnit)

	}

	// generate eval params
	var evalUserParams []entity.ParamsStruct
	for _, userParam := range manifest.Eval.CustomPara {
		pramaUnit := &entity.ParamsStruct{
			Name:     userParam.Name,
			Type:     userParam.Type,
			Default:  userParam.Default,
			Desc:     userParam.Desc.Zh,
			Editable: userParam.Visible,
		}
		evalUserParams = append(evalUserParams, *pramaUnit)
	}
	var evalSysParams []entity.SystemParamStruct
	for _, sysParam := range manifest.Eval.CommPara {
		if sysParam.PlatName == "train_output" {
			sysParam.PlatName = "checkpoint_path"
		}
		if sysParam.PlatName == "eval_output" {
			sysParam.PlatName = "output_path"
		}
		pramaUnit := &entity.SystemParamStruct{
			Name: sysParam.PlatName,
			Arg:  sysParam.Name,
		}
		evalSysParams = append(evalSysParams, *pramaUnit)

	}

	// create yaml
	model := &entity.ModelDbStruct{
		Platform:    []string{"iqi"},
		Label:       []string{"iqi-powered"},
		Name:        manifest.Algorithm.Name,
		Engine:      manifest.Train.Engine,
		Framework:   manifest.Algorithm.Framework,
		Description: manifest.Algorithm.Description.Zh,
		Type:        manifest.Algorithm.ModelType,
		Train: &entity.ModelDescStruct{
			Entry:      manifest.Train.Entry,
			UserParams: trainUserParams,
			SysParams:  trainSysParams,
		},
		Eval: &entity.ModelDescStruct{
			Entry:      manifest.Eval.Entry,
			UserParams: evalUserParams,
			SysParams:  evalSysParams,
		},
	}
	logger.Debugln("===== huawei config translate to below:")
	logger.Debugln(model)
	logger.Debugln("===== huawei config translate to above")
	output, err := yaml.Marshal(model)
	if err != nil {
		return appErr.APP_ERROR_CODE, err
	}
	err = ioutil.WriteFile(targetDir+"/manifest.yaml", output, 0777)
	if err != nil {
		return appErr.APP_ERROR_CODE, err
	}

	// move data to "code" dir
	err = os.MkdirAll(targetDir+"code", 0777)
	if err != nil {
		return appErr.APP_ERROR_CODE, err
	}
	files, err := ioutil.ReadDir(targetDir)
	if err != nil {
		return appErr.SUCCESS_CODE, err
	}
	for _, file := range files {
		if file.Name() != "code" && file.Name() != "manifest.yaml" {
			err = os.Rename(targetDir+file.Name(), targetDir+"/code/"+file.Name())
			if err != nil {
				return appErr.APP_ERROR_CODE, err
			}
		}
	}

	return appErr.SUCCESS_CODE, nil
}

func HuaweiPostHandler(targetDir string) (int, error) {
	configPath := targetDir + "/code/on_platform/plat_cfg.yaml"
	_, err := os.Stat(configPath)
	if err != nil {
		return appErr.APP_ERROR_CODE, errors.New("not found huawei config in post process")
	}

	// read resourceManifest
	var resourceManifest entity.ResourceFileStruct
	yamlfile, err := ioutil.ReadFile(configPath)
	if err != nil {
		return appErr.APP_ERROR_CODE, err
	}
	err = yaml.Unmarshal(yamlfile, &resourceManifest)
	if err != nil {
		return appErr.APP_ERROR_CODE, err
	}
	logger.Debugln(resourceManifest)

	for _, resourceDefine := range resourceManifest.ModelList {
		model, err := entity.GetModelByName(resourceDefine.Name)
		if err != nil {
			return appErr.APP_ERROR_CODE, err
		}
		if model == nil {
			continue
		}

		resource := &entity.ResourceDbStruct{
			Train: entity.ResourceRequirementDefineStruct{
				CPU:    resourceDefine.ResourceRequirement.Train.CPU,
				Memory: resourceDefine.ResourceRequirement.Train.Memory,
				NPU:    resourceDefine.ResourceRequirement.Train.NPU,
			},
			Infer: entity.ResourceRequirementDefineStruct{
				CPU:    resourceDefine.ResourceRequirement.Infer.CPU,
				Memory: resourceDefine.ResourceRequirement.Infer.Memory,
				NPU:    resourceDefine.ResourceRequirement.Infer.NPU,
			},
		}
		err = entity.CreateResourceDefine(resource)
		if err != nil {
			return appErr.APP_ERROR_CODE, err
		}
	}

	return appErr.SUCCESS_CODE, nil
}
