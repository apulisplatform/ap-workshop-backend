/* ******************************************************************************
* 2019 - present Contributed by Apulis Technology (Shenzhen) Co. LTD
*
* This program and the accompanying materials are made available under the
* terms of the MIT License, which is available at
* https://www.opensource.org/licenses/MIT
*
* See the NOTICE file distributed with this work for additional
* information regarding copyright ownership.
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations
* under the License.
*
* SPDX-License-Identifier: MIT
******************************************************************************/
package service

import (
	"apworkshop/internal/appErr"
	"apworkshop/internal/dao"
	"apworkshop/internal/entity"
)

func GetStudioInferenceByModelVersionId(modelVersionId int) (*entity.StudioInference, int, error) {
	infer, err := dao.GetStudioInferenceByVersionlId(modelVersionId)
	if err != nil {
		return nil, appErr.APP_ERROR_CODE, err
	}
	return infer, appErr.SUCCESS_CODE, nil
}
