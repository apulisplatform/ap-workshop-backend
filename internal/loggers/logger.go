/* ******************************************************************************
* 2019 - present Contributed by Apulis Technology (Shenzhen) Co. LTD
*
* This program and the accompanying materials are made available under the
* terms of the MIT License, which is available at
* https://www.opensource.org/licenses/MIT
*
* See the NOTICE file distributed with this work for additional
* information regarding copyright ownership.
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations
* under the License.
*
* SPDX-License-Identifier: MIT
******************************************************************************/
package loggers

import (
	"fmt"
	"io"
	"os"
	"path"
	"sync"

	"apworkshop/internal/configs"

	"github.com/sirupsen/logrus"
)

var once sync.Once
var instance *logrus.Logger

var logger = LogInstance()

func LogInstance() *logrus.Logger {
	once.Do(func() {
		instance = logrus.New()
	})
	return instance
}

func InitLogger(config *configs.LogConfig) {
	logConf := config
	logger.Formatter = new(Formatter)
	logger.SetLevel(logConf.Level)

	if logConf.WriteFile {
		if err := os.Mkdir(logConf.FileDir, 0777); err != nil {
			fmt.Println(err.Error())
		}
		fileName := path.Join(logConf.FileDir, logConf.FileName)
		if _, err := os.Stat(fileName); err != nil {
			if _, err := os.Create(fileName); err != nil {
				fmt.Println(err.Error())
			}
		}
		writeToFile, err := os.OpenFile(fileName, os.O_APPEND|os.O_WRONLY, os.ModeAppend)
		if err != nil {
			fmt.Println("err", err)
		}

		writers := io.MultiWriter(os.Stdout, writeToFile)
		logger.Out = writers
	}
}
