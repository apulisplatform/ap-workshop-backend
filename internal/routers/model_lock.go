/* ******************************************************************************
* 2019 - present Contributed by Apulis Technology (Shenzhen) Co. LTD
*
* This program and the accompanying materials are made available under the
* terms of the MIT License, which is available at
* https://www.opensource.org/licenses/MIT
*
* See the NOTICE file distributed with this work for additional
* information regarding copyright ownership.
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations
* under the License.
*
* SPDX-License-Identifier: MIT
******************************************************************************/
package api

import (
	"apworkshop/internal/appErr"
	"apworkshop/internal/proto"
	"apworkshop/internal/service"

	"github.com/gin-gonic/gin"
)

func AddModelLockRouter(group *gin.RouterGroup) {
	// routers for Huawei(松山湖)
	group.POST("ref", Wrapper(RefModel))
	group.POST("unref", Wrapper(UnrefModel))
}

func RefModel(c *gin.Context) error {
	var err error
	var reqContent proto.RefModelReq

	err = c.ShouldBindJSON(&reqContent)
	if err != nil {
		return ParameterError(c, err.Error())
	}
	errRsp := PreHandler(c, &reqContent)
	if errRsp != nil {
		return errRsp
	}

	version, errCode, err := service.DescribeModelVersion(proto.DescribeModelVersionReq{
		ModelVersionId: reqContent.ModelVersionId,
	})
	if err != nil {
		if errCode == appErr.RECORD_NOT_FOUND_ERROR_CODE {
			return AppError(c, appErr.REF_RESOURCE_UNFOUND, err.Error())
		}
		return AppError(c, errCode, err.Error())
	}

	data := proto.RefModelRsp{
		Path: version.StoragePath,
	}
	return SuccessResp(c, data)
}

func UnrefModel(c *gin.Context) error {
	var err error
	var reqContent proto.UnrefModel

	err = c.ShouldBindJSON(&reqContent)
	if err != nil {
		return ParameterError(c, err.Error())
	}
	errRsp := PreHandler(c, &reqContent)
	if errRsp != nil {
		return errRsp
	}

	data := "success"
	return SuccessResp(c, data)
}
