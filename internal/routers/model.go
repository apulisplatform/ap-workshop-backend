/* ******************************************************************************
* 2019 - present Contributed by Apulis Technology (Shenzhen) Co. LTD
*
* This program and the accompanying materials are made available under the
* terms of the MIT License, which is available at
* https://www.opensource.org/licenses/MIT
*
* See the NOTICE file distributed with this work for additional
* information regarding copyright ownership.
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations
* under the License.
*
* SPDX-License-Identifier: MIT
******************************************************************************/
package api

import (
	"apworkshop/internal/appErr"
	"apworkshop/internal/common"
	"apworkshop/internal/proto"
	"apworkshop/internal/service"
	appStorage "apworkshop/internal/storage"

	"github.com/gin-gonic/gin"
)

func AddModelRouter(group *gin.RouterGroup) {
	// routers for Huawei(松山湖)
	group.POST("modelPack", Wrapper(CreateModelsByPack))
	group.GET("models", Wrapper(GetModelsList))
	group.GET("model/:modelId", Wrapper(DescribeModel))
	group.POST("prepareModel", Wrapper(PrepareModel))
	group.PUT("commitModel", Wrapper(CommitModel))
	group.POST("cancelPrepare", Wrapper(CancelModel))
	group.DELETE("model/:modelId", Wrapper(DeleteModel))

	group.GET("modelTypes", Wrapper(GetModelTypes))
}

func CreateModelsByPack(c *gin.Context) error {
	var req *proto.CreateModelReq

	err := c.ShouldBindJSON(&req)
	if err != nil {
		return ParameterError(c, err.Error())
	}
	errRsp := PreHandler(c, req)
	if errRsp != nil {
		return errRsp
	}

	packLocalPath, err := appStorage.Handler.GetTranslatePath(req.FilePath)
	if err != nil {
		return AppError(c, appErr.APP_ERROR_CODE, err.Error())
	}
	depressDir, errCode, err := service.DepressModelPack(packLocalPath, req.FileName)
	if err != nil {
		return AppError(c, errCode, err.Error())
	}

	if req.Producer == common.PRODUCER_HUAWEI_NAME {
		errCode, err := service.HuaweiModelPreHandle(depressDir)
		if err != nil {
			return AppError(c, errCode, err.Error())
		}
	}
	errCode, err = service.CreateModelsByPack(req.Scope, req.ModelName, req.ModelDescription, depressDir)
	if err != nil {
		return AppError(c, errCode, err.Error())
	}
	if req.Producer == common.PRODUCER_HUAWEI_NAME {
		errCode, err := service.HuaweiPostHandler(depressDir)
		if err != nil {
			return AppError(c, errCode, err.Error())
		}
	}

	data := "success"
	return SuccessResp(c, data)
}

func PrepareModel(c *gin.Context) error {
	var req *proto.PrepareModelReq

	err := c.ShouldBindJSON(&req)
	if err != nil {
		return ParameterError(c, err.Error())
	}
	errRsp := PreHandler(c, req)
	if errRsp != nil {
		return errRsp
	}

	model, modelVersion, path, locker, errCode, err := service.PrepareModel(req.Context, req.Scope, req.ModelName, req.ModelDescription, false)
	if err != nil {
		return AppError(c, errCode, err.Error())
	}

	data := &proto.PrepareModelRsp{
		Lock:           locker,
		Path:           path,
		ModelId:        int(model.ID),
		ModelVersionId: int(modelVersion.ID),
	}
	return SuccessResp(c, data)
}

func CancelModel(c *gin.Context) error {
	var req *proto.CommitModelReq

	err := c.ShouldBindJSON(&req)
	if err != nil {
		return ParameterError(c, err.Error())
	}
	errRsp := PreHandler(c, req)
	if errRsp != nil {
		return errRsp
	}

	// prepare status will overdue, so don't need to cancel now

	data := "success"
	return SuccessResp(c, data)
}

func CommitModel(c *gin.Context) error {
	var req *proto.CommitModelReq

	err := c.ShouldBindJSON(&req)
	if err != nil {
		return ParameterError(c, err.Error())
	}
	errRsp := PreHandler(c, req)
	if errRsp != nil {
		return errRsp
	}

	if req.Status == common.MODEL_COMMIT_STATUS {
		_, err := service.CommitModel(req)
		if err != nil {
			return AppError(c, appErr.COMMIT_MODEL_ERROR_CODE, err.Error())
		}
	} else if req.Status == common.MODEL_ROLLBACK_STATUS {
		_, err := service.RollbackModelCommit(req)
		if err != nil {
			return AppError(c, appErr.ROLLBACK_MODEL_ERROR_CODE, err.Error())
		}
	} else {
		return AppError(c, appErr.COMMIT_MODEL_ERROR_CODE, "status unknown")
	}

	data := "success"
	return SuccessResp(c, data)
}

func DescribeModel(c *gin.Context) error {
	var err error
	var req *proto.DescribeModelReq

	err = c.ShouldBindUri(&req)
	if err != nil {
		return ParameterError(c, err.Error())
	}
	errRsp := PreHandler(c, req)
	if errRsp != nil {
		return errRsp
	}

	model, errCode, err := service.DescribeModel(*req)
	if err != nil {
		return AppError(c, errCode, err.Error())
	}

	latestVersion, errCode, err := service.GetModelLatestVersion(req.ModelId)
	if err != nil {
		return AppError(c, errCode, err.Error())
	}
	data := &proto.DescribeModelRsp{
		ModelDbStruct: *model,
	}
	if latestVersion == nil {
		data.LatestVersionId = 0
		data.LatestVersionName = ""
		data.InferenceInfo = nil
	} else {
		inference, errCode, err := service.GetInferenceByModelVersionId(int(latestVersion.ID))
		if err != nil {
			return AppError(c, errCode, err.Error())
		}
		data.LatestVersionId = int(latestVersion.ID)
		data.LatestVersionName = latestVersion.Name
		data.InferenceInfo = inference
	}

	return SuccessResp(c, data)
}

func GetModelsList(c *gin.Context) error {
	var req *proto.GetModelListReq

	err := c.ShouldBindQuery(&req)
	if err != nil {
		return ParameterError(c, err.Error())
	}
	errRsp := PreHandler(c, req)
	if errRsp != nil {
		return errRsp
	}

	var total int
	modelList, total, errCode, err := service.GetModelList(req)
	if err != nil {
		return AppError(c, errCode, err.Error())
	}

	var rspList []proto.GetModelListRspUnit
	for _, model := range modelList {
		var unit proto.GetModelListRspUnit
		versionCount, errCode, err := service.GetModelVersionCount(int(model.ID))
		if err != nil {
			return AppError(c, errCode, err.Error())
		}
		latestVersion, errCode, err := service.GetModelLatestVersion(int(model.ID))
		if err != nil {
			return AppError(c, errCode, err.Error())
		}
		unit = proto.GetModelListRspUnit{
			ModelDbStruct:     model,
			VersionCount:      versionCount,
			LatestVersionInfo: latestVersion,
		}

		if latestVersion == nil {
			unit.InferenceInfo = nil
			unit.LatestVersionId = 0
			unit.LatestVersionName = ""
		} else {
			inference, errCode, err := service.GetInferenceByModelVersionId(int(latestVersion.ID))
			if err != nil {
				return AppError(c, errCode, err.Error())
			}
			unit.InferenceInfo = inference
			unit.LatestVersionId = int(latestVersion.ID)
			unit.LatestVersionName = latestVersion.Name
		}
		rspList = append(rspList, unit)
	}

	data := &proto.GetModelListRsp{
		Items: rspList,
		Total: total,
	}

	return SuccessResp(c, data)
}

func DeleteModel(c *gin.Context) error {
	var req *proto.DeleteModelReq

	err := c.ShouldBindUri(&req)
	if err != nil {
		return ParameterError(c, err.Error())
	}
	errRsp := PreHandler(c, req)
	if errRsp != nil {
		return errRsp
	}

	errCode, err := service.DeleteModel(*req)
	if err != nil {
		return AppError(c, errCode, err.Error())
	}

	data := "success"
	return SuccessResp(c, data)
}

func GetModelTypes(c *gin.Context) error {
	types, errCode, err := service.GetModelTypes()
	if err != nil {
		return AppError(c, errCode, err.Error())
	}

	data := &proto.GetModelTypesRsp{
		Types: types,
	}
	return SuccessResp(c, data)
}
