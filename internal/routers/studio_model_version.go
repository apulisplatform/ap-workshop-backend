/* ******************************************************************************
* 2019 - present Contributed by Apulis Technology (Shenzhen) Co. LTD
*
* This program and the accompanying materials are made available under the
* terms of the MIT License, which is available at
* https://www.opensource.org/licenses/MIT
*
* See the NOTICE file distributed with this work for additional
* information regarding copyright ownership.
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations
* under the License.
*
* SPDX-License-Identifier: MIT
******************************************************************************/
package api

import (
	"apworkshop/internal/proto"
	"apworkshop/internal/service"

	"github.com/gin-gonic/gin"
)

func AddStudioModelVersionRouter(group *gin.RouterGroup) {
	// routers for AIStudio
	group.GET("studioModelVersionsByModel/:modelId", Wrapper(StudioGetModelVersionListByModel))
	group.GET("studioModelVersion/:modelId/:versionId", Wrapper(StudioDescribeModelVersion))
	// group.GET("studioModelVersionInference/:modelId/:versionId", Wrapper(studioGetModelVersionInference))
	group.DELETE("studioModelVersion/:versionId", Wrapper(StudioDeleteModelVersion))

	group.GET("studioGetYaml/:modelId/:versionId", Wrapper(StudioGetModelYaml))
}

func StudioGetModelVersionListByModel(c *gin.Context) error {
	var err error
	var reqContent proto.GetStudioModelVersionListByModelReq

	err = c.ShouldBindUri(&reqContent)
	if err != nil {
		return ParameterError(c, err.Error())
	}
	err = c.ShouldBindQuery(&reqContent)
	if err != nil {
		return ParameterError(c, err.Error())
	}
	errRsp := PreHandler(c, &reqContent)
	if errRsp != nil {
		return errRsp
	}

	versionList, errCode, err := service.GetStudioModelVersionListByModel(reqContent)
	if err != nil {
		return AppError(c, errCode, err.Error())
	}

	total, errCode, err := service.GetStudioModelVersionCount(reqContent.ModelId)
	if err != nil {
		return AppError(c, errCode, err.Error())
	}

	var versionCount int
	if total != 0 {
		versionCount, errCode, err = service.GetStudioModelVersionCount(versionList[0].ModelID)
		if err != nil {
			return AppError(c, errCode, err.Error())
		}
	}

	resultList := []proto.GetStudioModelVersionListByModelRspUnit{}
	for _, version := range versionList {
		infer, errCode, err := service.GetStudioInferenceByModelVersionId(version.ID)
		if err != nil {
			return AppError(c, errCode, err.Error())
		}
		unit := proto.GetStudioModelVersionListByModelRspUnit{
			VersionId:    version.ID,
			Name:         version.Name,
			ModelName:    version.ModelName,
			ModelId:      version.ModelID,
			Description:  version.Description,
			Engine:       version.Engine,
			Backbone:     version.Backbone,
			DatasetName:  version.Dataset,
			Parameters:   version.Parameters,
			Publisher:    version.Publisher,
			TrainTime:    version.TrainTime,
			TrainDevices: version.Devices,
			Evaluation:   version.Evaluation,
			VersionCount: versionCount,
			PublishTime:  version.PublishTime,
			CreatedAt:    version.CreatedAt,
			Tags:         version.Tags,
			Train:        version.Train,
			Eval:         version.Eval,
		}
		if infer != nil {
			if infer.Center != nil {
				unit.CenterInferPercision = infer.Center.Percision
				unit.CenterInferDevices = infer.Center.Devices
			}

			if infer.Edge != nil {
				unit.EdgeInferPercision = infer.Edge.Percision
				unit.EdgeInferDevices = infer.Edge.Devices
			}
		}
		resultList = append(resultList, unit)
	}

	data := &proto.GetStudioModelVersionListByModelRsp{
		Items: resultList,
		Total: total,
	}

	return SuccessResp(c, data)
}

func StudioDescribeModelVersion(c *gin.Context) error {
	var err error
	var reqContent proto.DescribeModelVersionReq

	err = c.ShouldBindUri(&reqContent)
	if err != nil {
		return ParameterError(c, err.Error())
	}
	errRsp := PreHandler(c, &reqContent)
	if errRsp != nil {
		return errRsp
	}

	version, errCode, err := service.DescribeStudioModelVersion(reqContent)
	if err != nil {
		return AppError(c, errCode, err.Error())
	}

	// model, errCode, err := service.DescribeStudioModel(proto.DescribeModelReq{
	// 	ModelId: reqContent.ModelId,
	// })
	if err != nil {
		return AppError(c, errCode, err.Error())
	}

	data := &proto.DescribeStudioModelVersionRsp{
		// ModelInfo:    model,
		ModelVersion: version,
	}

	return SuccessResp(c, data)
}

func StudioDeleteModelVersion(c *gin.Context) error {
	var err error
	var reqContent proto.DeleteModelVersionReq

	err = c.ShouldBindUri(&reqContent)
	if err != nil {
		return ParameterError(c, err.Error())
	}
	errRsp := PreHandler(c, &reqContent)
	if errRsp != nil {
		return errRsp
	}

	errCode, err := service.DeleteStudioModelVersion(reqContent)
	if err != nil {
		return AppError(c, errCode, err.Error())
	}

	data := "success"
	return SuccessResp(c, data)
}

func studioGetModelVersionInference(c *gin.Context) error {
	var err error
	var reqContent proto.DescribeModelVersionReq

	err = c.ShouldBindUri(&reqContent)
	if err != nil {
		return ParameterError(c, err.Error())
	}
	errRsp := PreHandler(c, &reqContent)
	if errRsp != nil {
		return errRsp
	}

	version, errCode, err := service.DescribeStudioModelVersion(reqContent)
	if err != nil {
		return AppError(c, errCode, err.Error())
	}

	data := &proto.DescribeStudioModelVersionRsp{
		// ModelInfo:    model,
		ModelVersion: version,
	}

	return SuccessResp(c, data)
}

func StudioGetModelYaml(c *gin.Context) error {
	var err error
	var reqContent proto.DescribeModelVersionReq

	err = c.ShouldBindUri(&reqContent)
	if err != nil {
		return ParameterError(c, err.Error())
	}
	errRsp := PreHandler(c, &reqContent)
	if errRsp != nil {
		return errRsp
	}

	output, errCode, err := service.GetModelVersionYaml(reqContent.ModelVersionId)
	if err != nil {
		return AppError(c, errCode, err.Error())
	}
	data := output
	return SuccessResp(c, data)
}
