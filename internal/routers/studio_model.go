/* ******************************************************************************
* 2019 - present Contributed by Apulis Technology (Shenzhen) Co. LTD
*
* This program and the accompanying materials are made available under the
* terms of the MIT License, which is available at
* https://www.opensource.org/licenses/MIT
*
* See the NOTICE file distributed with this work for additional
* information regarding copyright ownership.
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations
* under the License.
*
* SPDX-License-Identifier: MIT
******************************************************************************/
package api

import (
	"apworkshop/internal/appErr"
	"apworkshop/internal/common"
	"apworkshop/internal/dao"
	"apworkshop/internal/entity"
	"apworkshop/internal/middlewares"
	"apworkshop/internal/proto"
	"apworkshop/internal/service"
	appStorage "apworkshop/internal/storage"
	"apworkshop/internal/tasks"
	validators "apworkshop/internal/validator"
	"bufio"
	"encoding/base64"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"gopkg.in/yaml.v2"
)

func MyHookAuthMiddleware() gin.HandlerFunc {
	var original_middleware = middlewares.AutoAuthMiddleware()
	return func(c *gin.Context) {
		logger.Info("Request URI: " + c.Request.RequestURI)
		if strings.Contains(c.Request.RequestURI, "/api/v1/studioCommitModel?internal=1") {
			c.Next()
		} else {
			original_middleware(c)
		}
	}
}

func AddAuthRouter(group *gin.RouterGroup) {
	group.Use(MyHookAuthMiddleware())

	group.GET("studioModels", Wrapper(StudioGetModelsList))

	group.POST("studioModelPack", Wrapper(StudioCreateModelsByPack))

	group.PUT("studioCommitModel", Wrapper(StudioCommitModel))
}

func AddStudioModelRouter(group *gin.RouterGroup) {
	// routers for AIStudio
	// group.Use(middlewares.AutoAuthMiddleware())
	group.GET("studioOverview", Wrapper(StudioGetModelOverview))

	group.GET("studioModel/:modelId", Wrapper(StudioDescribeModel))

	group.POST("studioModelGetTmp", Wrapper(StudioModelGetTmp))
	group.POST("studioPrepareModel", Wrapper(StudioPrepareModel))

	group.DELETE("studioModel/:modelId", Wrapper(StudioDeleteModel))

	group.GET("studioGetTypes", Wrapper(StudioGetModelVarietyTypes))
	group.GET("studioGetFieldTypes", Wrapper(StudioGetModelFieldTypes))
	group.GET("studioGetOtherTypes", Wrapper(StudioGetModelOtherTypes))
	group.GET("studioGetRelatedTypes", Wrapper(StudioGetModelRelatedTypes))
	group.GET("StudioGetManifest/:modelId", Wrapper(StudioGetModelManifest))
	group.POST("studioManifestValidator", Wrapper(StudioValidateManifest))

	group.POST("studioCheckInferenceModel", Wrapper(studioCheckInferenceModel))
	group.POST("studioTransformInferenceModel", Wrapper(StudioTransformInferenceModel))

	group.POST("studioDownLoadModel", Wrapper(StudioDownLoadModel))
}

// routers for AIStudio starts here
func StudioGetModelOverview(c *gin.Context) error {
	preset, publish, userDefine, errCode, err := service.GetStudioOverview()
	if err != nil {
		return AppError(c, errCode, err.Error())
	}

	data := &proto.GetStudioModelOverview{
		PresetModel:    preset,
		PublishedModel: publish,
		UserDefine:     userDefine,
	}

	return SuccessResp(c, data)
}

func StudioCreateModelsByPack(c *gin.Context) error {
	var req *proto.CreateStudioModelReq

	err := c.ShouldBindJSON(&req)
	if err != nil {
		return ParameterError(c, err.Error())
	}
	errRsp := PreHandler(c, req)
	if errRsp != nil {
		return errRsp
	}

	packLocalPath, err := appStorage.Handler.GetTranslatePath(req.FilePath)
	if err != nil {
		return AppError(c, appErr.APP_ERROR_CODE, err.Error())
	}
	depressDir, errCode, err := service.DepressStudioModelPack(packLocalPath, req.FileName)
	if err != nil {
		return AppError(c, errCode, err.Error())
	}
	auth := c.Request.Header.Get("Authorization")
	if auth == "" {
		return AppError(c, errCode, errors.New("no bear token").Error())
	}
	SetHeadersWithContextKeys(c)
	userName, _ := c.Get("userName")
	userId, _ := c.Get("userId")
	userGroup := c.Request.Header.Get("groupAccount")
	logger.Info(fmt.Sprintf("UserName: %v    UserId: %v     UserGroup: %v       Auth: %s", userName, userId, userGroup, auth))
	errCode, err = service.CreateStudioModelsByPack(req.Scope, req.ModelName, req.ModelDescription, depressDir, auth, fmt.Sprintf("%v", userName), fmt.Sprintf("%v", userId), userGroup)
	if err != nil {
		return AppError(c, errCode, err.Error())
	}

	data := "success"
	return SuccessResp(c, data)
}

func StudioGetModelsList(c *gin.Context) error {
	var req proto.GetStudioModelListReq
	err := c.ShouldBindQuery(&req)
	if err != nil {
		return ParameterError(c, err.Error())
	}
	errRsp := PreHandler(c, &req)
	if errRsp != nil {
		return errRsp
	}

	auth := c.Request.Header.Get("Authorization")
	if auth == "" {
		return AppError(c, appErr.APP_ERROR_CODE, errors.New("no bear token").Error())
	}
	SetHeadersWithContextKeys(c)
	userId := c.Request.Header.Get("userId")
	userName := c.Request.Header.Get("userName")
	userGroup := c.Request.Header.Get("groupAccount")
	logger.Debug("auth: " + auth + "   userId: " + userId + "    userName: " + userName + "    userGroup: " + userGroup)

	var total int
	modelList, total, errCode, err := service.GetStudioModelList(&req, userGroup)
	if err != nil {
		return AppError(c, errCode, err.Error())
	}

	rspList := []proto.GetStudioModelListRspUnit{}
	for _, model := range modelList {
		var unit proto.GetStudioModelListRspUnit
		versionCount, errCode, err := service.GetStudioModelVersionCount(model.ID)
		if err != nil {
			return AppError(c, errCode, err.Error())
		}
		latestVersion, errCode, err := service.GetStudioModelLatestVersion(model.ID)
		if err != nil {
			return AppError(c, errCode, err.Error())
		}

		unit = proto.GetStudioModelListRspUnit{
			GormModel:            model.GormModel,
			Scope:                model.Scope,
			Source:               model.Source,
			AutoDL:               model.AutoDL,
			Logo:                 model.Logo,
			Name:                 model.Name,
			Framework:            model.Framework,
			DistributedFramework: model.DistributedFramework,
			Backbone:             model.Backbone,
			Description:          model.Description,
			Engine:               model.Engine,
			Devices:              model.Devices,
			Field:                model.Field,
			Task:                 model.Task,
			Ability:              model.Ability,
			CanTrain:             model.CanTrain,
			CanCenterInfer:       model.CanCenterInfer,
			CanEdgeInfer:         model.CanEdgeInfer,
			IsPreload:            model.IsPreload,
			Status:               model.Status,
			ReadMe:               model.ReadMe,
			VersionCount:         versionCount,
			// LatestVersionInfo: latestVersion,
		}

		if latestVersion == nil {
			// unit.InferenceInfo = nil
			unit.LatestVersionId = 0
			unit.LatestVersionName = ""
			unit.ShowList = []string{}
		} else {
			// inference, errCode, err := service.GetStudioInferenceByModelVersionId(latestVersion.ID)
			if err != nil {
				return AppError(c, errCode, err.Error())
			}
			// unit.InferenceInfo = inference
			unit.LatestVersionId = latestVersion.ID
			unit.LatestVersionName = latestVersion.Name
			unit.ShowList = []string{}
			unit.ShowList = append(unit.ShowList, unit.Ability...)
			unit.ShowList = append(unit.ShowList, unit.Task)
			unit.ShowList = append(unit.ShowList, unit.DeviceType)
			unit.ShowList = append(unit.ShowList, unit.Framework)

			// open and parse image into base64
			var encodedStr string
			localPath, err := appStorage.Handler.GetTranslatePath(latestVersion.StoragePath)
			if err != nil {
				return AppError(c, appErr.APP_ERROR_CODE, err.Error())
			}
			// file, err := os.Stat(localPath + common.MANIFEST_SUB_DIR + "/" + latestVersion.ImagePath)
			file, err := os.Stat(localPath + "/" + latestVersion.ImagePath)
			if os.IsExist(err) || file == nil {
				encodedStr = ""
			} else if file.IsDir() {
				encodedStr = ""
			} else {
				// f, err := os.Open(localPath + common.MANIFEST_SUB_DIR + "/" + latestVersion.ImagePath)
				f, err := os.Open(localPath + "/" + latestVersion.ImagePath)
				if err != nil {
					return AppError(c, appErr.APP_ERROR_CODE, err.Error())
				}
				reader := bufio.NewReader(f)
				content, err := ioutil.ReadAll(reader)
				if err != nil {
					return AppError(c, appErr.APP_ERROR_CODE, err.Error())
				}
				encodedStr = base64.StdEncoding.EncodeToString(content)
				if err != nil {
					return AppError(c, errCode, err.Error())
				}
			}
			unit.LogoPic = encodedStr
		}
		rspList = append(rspList, unit)
	}

	data := &proto.GetStudioModelListRsp{
		Items: rspList,
		Total: total,
	}

	return SuccessResp(c, data)
}

func StudioDescribeModel(c *gin.Context) error {
	var err error
	var req *proto.DescribeModelReq

	err = c.ShouldBindUri(&req)
	if err != nil {
		return ParameterError(c, err.Error())
	}
	errRsp := PreHandler(c, req)
	if errRsp != nil {
		return errRsp
	}

	model, errCode, err := service.DescribeStudioModel(*req)
	if err != nil {
		return AppError(c, errCode, err.Error())
	}

	latestVersion, errCode, err := service.GetStudioModelLatestVersion(req.ModelId)
	if err != nil {
		return AppError(c, errCode, err.Error())
	}
	data := &proto.DescribeStudioModelRsp{
		GormModel:            model.GormModel,
		Scope:                model.Scope,
		Source:               model.Source,
		AutoDL:               model.AutoDL,
		Logo:                 model.Logo,
		Name:                 model.Name,
		Framework:            model.Framework,
		DistributedFramework: model.DistributedFramework,
		Backbone:             model.Backbone,
		Description:          model.Description,
		Engine:               model.Engine,
		Devices:              model.Devices,
		Field:                model.Field,
		Task:                 model.Task,
		Ability:              model.Ability,
		CanTrain:             model.CanTrain,
		CanCenterInfer:       model.CanCenterInfer,
		CanEdgeInfer:         model.CanEdgeInfer,
		IsPreload:            model.IsPreload,
		Status:               model.Status,
		ReadMe:               model.ReadMe,
	}
	if latestVersion == nil {
		data.LatestVersionId = 0
		data.LatestVersionName = ""
		// data.InferenceInfo = nil
		data.VersionCount = 0
	} else {
		// inference, errCode, err := service.GetStudioInferenceByModelVersionId(latestVersion.ID)
		if err != nil {
			return AppError(c, errCode, err.Error())
		}
		versionCount, errCode, err := service.GetStudioModelVersionCount(model.ID)
		if err != nil {
			return AppError(c, errCode, err.Error())
		}
		data.LatestVersionId = latestVersion.ID
		data.LatestVersionName = latestVersion.Name
		data.DatasetName = latestVersion.Dataset
		// data.InferenceInfo = inference
		data.VersionCount = versionCount
	}

	return SuccessResp(c, data)
}

func StudioModelGetTmp(c *gin.Context) error {
	var req proto.StudioModelGetTmpReq

	err := c.ShouldBindJSON(&req)
	if err != nil {
		return ParameterError(c, err.Error())
	}

	version, errCode, err := service.StudioModelGetTmp(req.UserName, req.ModelId, req.ModelVersionId)
	if err != nil {
		return AppError(c, errCode, err.Error())
	}

	data := proto.StudioModelGetTmpRsp{
		TmpStoragePath: version.TmpStoragePath,
	}
	return SuccessResp(c, data)
}

func StudioPrepareModel(c *gin.Context) error {
	var req *proto.StudioPrepareModelReq

	err := c.ShouldBindJSON(&req)
	if err != nil {
		return ParameterError(c, err.Error())
	}
	errRsp := PreHandler(c, req)
	if errRsp != nil {
		return errRsp
	}

	model, modelVersion, path, locker, errCode, err := service.PrepareStudioModel(req.Context, req.Scope, req.ModelName, req.ModelDescription, req.UserName, req.IsTmp, false)
	if err != nil {
		return AppError(c, errCode, err.Error())
	}

	data := &proto.StudioPrepareModelRsp{
		Context:        locker,
		Path:           path,
		ModelId:        int(model.ID),
		ModelVersionId: int(modelVersion.ID),
	}
	return SuccessResp(c, data)
}

func StudioCommitModel(c *gin.Context) error {
	var req *proto.StudioCommitModelReq

	err := c.ShouldBindJSON(&req)
	if err != nil {
		return ParameterError(c, err.Error())
	}
	errRsp := PreHandler(c, req)
	if errRsp != nil {
		return errRsp
	}
	if req.Status == common.MODEL_COMMIT_STATUS {
		auth := c.Request.Header.Get("Authorization")
		if auth == "" {
			return AppError(c, appErr.APP_ERROR_CODE, errors.New("no bear token").Error())
		}
		SetHeadersWithContextKeys(c)
		userId := c.Request.Header.Get("userId")
		userName := c.Request.Header.Get("userName")
		userGroup := c.Request.Header.Get("groupAccount")
		logger.Debug("auth: " + auth + "   userId: " + userId + "    userName: " + userName + "    userGroup: " + userGroup)
		_, err := service.CommitStudioModel(req, auth, userName, userId, userGroup)
		if err != nil {
			return AppError(c, appErr.STUDIO_MODEL_COMMIT_FAIL, err.Error())
		}
	} else if req.Status == common.MODEL_ROLLBACK_STATUS {
		_, err := service.RollbackStudioModelCommit(req)
		if err != nil {
			return AppError(c, appErr.STUDIO_MODEL_ROLLBACK_NOT_EXISTS, err.Error())
		}
	} else {
		return AppError(c, appErr.STUDIO_MODEL_COMMIT_FAIL, "status unknown")
	}

	data := "success"
	return SuccessResp(c, data)
}

func StudioDeleteModel(c *gin.Context) error {
	var req *proto.DeleteModelReq

	err := c.ShouldBindUri(&req)
	if err != nil {
		return ParameterError(c, err.Error())
	}
	errRsp := PreHandler(c, req)
	if errRsp != nil {
		return errRsp
	}

	errCode, err := service.DeleteStudioModel(*req)
	if err != nil {
		return AppError(c, errCode, err.Error())
	}

	data := "success"
	return SuccessResp(c, data)
}

func StudioGetModelVarietyTypes(c *gin.Context) error {
	rsp, errCode, err := service.GetStudioModelVarietyTypes(common.STUDIO_MODEL_TYPES)
	if err != nil {
		return AppError(c, errCode, err.Error())
	}

	deviceTyps, err := dao.GetStudioModelDeviceType()
	if err != nil {
		return AppError(c, errCode, err.Error())
	}
	rsp["device_type"] = deviceTyps
	// typeMap["device_type"] = []string{"cpu", "huawei_npu", "nvidia_gpu"}

	data := rsp
	return SuccessResp(c, data)
}

func StudioGetModelFieldTypes(c *gin.Context) error {
	rsp, errCode, err := service.GetStudioModelFieldTypes()
	if err != nil {
		return AppError(c, errCode, err.Error())
	}
	data := rsp
	return SuccessResp(c, data)
}

func StudioGetModelOtherTypes(c *gin.Context) error {
	var err error
	var req proto.StudioModelTypesReq
	err = c.ShouldBindQuery(&req)
	if err != nil {
		return ParameterError(c, err.Error())
	}

	rsp, errCode, err := service.GetStudioModelOtherTypes(req.Field)
	if err != nil {
		return AppError(c, errCode, err.Error())
	}

	data := rsp
	return SuccessResp(c, data)
}

func StudioGetModelRelatedTypes(c *gin.Context) error {
	rsp, errCode, err := service.GetStudioModelFieldTypes()
	if err != nil {
		return AppError(c, errCode, err.Error())
	}

	outputStruct := map[string]interface{}{}
	for _, field := range rsp {
		otherType, errCode, err := service.GetStudioModelOtherTypes(field)
		if err != nil {
			return AppError(c, errCode, err.Error())
		}
		outputStruct[field] = otherType
	}

	data := outputStruct
	return SuccessResp(c, data)
}

func StudioGetModelManifest(c *gin.Context) error {
	var err error
	var req *proto.DescribeModelReq

	err = c.ShouldBindUri(&req)
	if err != nil {
		return ParameterError(c, err.Error())
	}
	errRsp := PreHandler(c, req)
	if errRsp != nil {
		return errRsp
	}

	model, errCode, err := service.DescribeStudioModel(*req)
	if err != nil {
		return AppError(c, errCode, err.Error())
	}

	data := model
	return SuccessResp(c, data)
}

func StudioValidateManifest(c *gin.Context) error {
	var err error
	inferFile, err := c.FormFile("inferFile")
	if err != nil {
		return ParameterError(c, err.Error())
	}
	manifestFile, err := c.FormFile("manifestFile")
	if err != nil {
		return ParameterError(c, err.Error())
	}
	// Retrieve file information
	inferExtension := filepath.Ext(inferFile.Filename)
	manifestExtension := filepath.Ext(manifestFile.Filename)
	// Generate random file name for the new uploaded file so it doesn't override the old file with same name
	inferFileName := uuid.New().String() + inferExtension
	manifestFileName := uuid.New().String() + manifestExtension

	_, err = os.Stat("/tmp/apworkshop/manifestvalidation")
	if !os.IsExist(err) {
		err = os.MkdirAll("/tmp/apworkshop/manifestvalidation", 0777)
		if err != nil {
			return AppError(c, appErr.APP_ERROR_CODE, err.Error())
		}
	}

	inferFilePath := "/tmp/apworkshop/manifestvalidation/" + inferFileName
	manifestFilePath := "/tmp/apworkshop/manifestvalidation/" + manifestFileName

	// The file is received, so let's save it
	err = c.SaveUploadedFile(inferFile, inferFilePath)
	if err != nil {
		return ParameterError(c, err.Error())
	}
	err = c.SaveUploadedFile(manifestFile, manifestFilePath)
	if err != nil {
		return ParameterError(c, err.Error())
	}

	var manifest entity.StudioModel
	var infer entity.StudioInference
	var inferDb entity.StudioInferenceModel

	inferByte, err := ioutil.ReadFile(inferFilePath)
	if err != nil {
		return AppError(c, appErr.APP_ERROR_CODE, err.Error())
	}
	manifestByte, err := ioutil.ReadFile(manifestFilePath)
	if err != nil {
		return AppError(c, appErr.APP_ERROR_CODE, err.Error())
	}
	err = yaml.Unmarshal(manifestByte, &manifest)
	if err != nil {
		return AppError(c, appErr.APP_ERROR_CODE, err.Error())
	}

	err = yaml.Unmarshal(inferByte, &inferDb)
	if err != nil {
		return AppError(c, appErr.APP_ERROR_CODE, err.Error())
	}
	err = yaml.Unmarshal(inferByte, &infer)
	if err != nil {
		return AppError(c, appErr.APP_ERROR_CODE, err.Error())
	}
	infer.StudioInferenceModel = inferDb
	errCode, err := validators.ValidateStudioManifest(&manifest)
	if err != nil {
		return AppError(c, errCode, err.Error())
	}
	errCode, err = validators.ValidateStudioInferenceManifest(&infer)
	if err != nil {
		return AppError(c, errCode, err.Error())
	}

	err = os.Remove(inferFilePath)
	if err != nil {
		return ParameterError(c, err.Error())
	}
	err = os.Remove(manifestFilePath)
	if err != nil {
		return ParameterError(c, err.Error())
	}

	data := "success"
	return SuccessResp(c, data)
}

func studioCheckInferenceModel(c *gin.Context) error {
	var err error
	var req *proto.StudioTransformModelReq
	err = c.ShouldBindJSON(&req)
	if err != nil {
		return ParameterError(c, err.Error())
	}
	errRsp := PreHandler(c, req)
	if errRsp != nil {
		return errRsp
	}
	needTransform, canInference, errCode, err := service.StudioCheckInferenceModel(req.ModelId, req.ModelVersionId, req.DeviceType)
	if err != nil {
		return AppError(c, errCode, err.Error())
	}
	data := proto.StudioCheckInferenceModelRsp{
		NeedTransform: needTransform,
		CanInference:  canInference,
	}
	return SuccessResp(c, data)
}

func StudioTransformInferenceModel(c *gin.Context) error {
	var err error
	var req *proto.StudioTransformModelReq

	err = c.ShouldBindJSON(&req)
	if err != nil {
		return ParameterError(c, err.Error())
	}
	errRsp := PreHandler(c, req)
	if errRsp != nil {
		return errRsp
	}

	jobId, errCode, err := tasks.CreateModelTransformJob(req.ModelId, req.ModelVersionId, req.DeviceType, req.ServiceId, req.QueueName)
	if err != nil {
		return AppError(c, errCode, err.Error())
	}

	logger.Info("Job transform JodId: " + jobId)
	data := entity.JsonB{"data": "success"}
	return SuccessResp(c, data)
}

func StudioDownLoadModel(c *gin.Context) error {
	var err error
	var req *proto.StudioDownLoadModelReq

	err = c.ShouldBindJSON(&req)
	if err != nil {
		return ParameterError(c, err.Error())
	}
	errRsp := PreHandler(c, req)
	if errRsp != nil {
		return errRsp
	}
	downloadRsp, errCode, err := service.StudioDownLoadModel(req.ModelId, req.ModelVersionId)
	if err != nil {
		return AppError(c, errCode, err.Error())
	}
	data := proto.StudioDownLoadModelRsp{
		Dir:          downloadRsp.Dir,
		DownloadLink: downloadRsp.DownloadLink,
		StoragePath:  downloadRsp.StoragePath,
	}
	return SuccessResp(c, data)
}
