/* ******************************************************************************
* 2019 - present Contributed by Apulis Technology (Shenzhen) Co. LTD
*
* This program and the accompanying materials are made available under the
* terms of the MIT License, which is available at
* https://www.opensource.org/licenses/MIT
*
* See the NOTICE file distributed with this work for additional
* information regarding copyright ownership.
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations
* under the License.
*
* SPDX-License-Identifier: MIT
******************************************************************************/
package api

import (
	"apworkshop/internal/appErr"
	validators "apworkshop/internal/validator"
	"fmt"
	"reflect"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
)

var validWorker = validators.ValidatorInstance()

// pre handler is used to handle the common logic of each comming request
func PreHandler(c *gin.Context, reqContent interface{}) *APIErrorResp {
	var err error

	// check reqContent type
	contentType := reflect.TypeOf(reqContent)
	switch contentType.Kind() {
	case reflect.Ptr:
		break
	default:
		logger.Errorf("PreHandler: reqContent is not a pointer")
		return ServerError(c)
	}

	// validate request content
	err = validWorker.Struct(reqContent)
	if err != nil {
		errs, _ := err.(validator.ValidationErrors)
		for _, validationError := range errs {
			if validationError.Tag() == "checkName" {
				return AppError(c, appErr.APP_ERROR_CODE, err.Error())
			}
		}
		return ParameterError(c, err.Error())
	}

	// get user info, user info comes from authentication
	if err != nil {
		return AppError(c, appErr.APP_ERROR_CODE, err.Error())
	}

	return nil
}

func SetHeadersWithContextKeys(c *gin.Context) {
	for key, value := range c.Keys {
		c.Request.Header.Set(key, fmt.Sprintf("%+v", value))
		fmt.Printf("%s:%v\n", key, value)
	}
}
