/* ******************************************************************************
* 2019 - present Contributed by Apulis Technology (Shenzhen) Co. LTD
*
* This program and the accompanying materials are made available under the
* terms of the MIT License, which is available at
* https://www.opensource.org/licenses/MIT
*
* See the NOTICE file distributed with this work for additional
* information regarding copyright ownership.
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations
* under the License.
*
* SPDX-License-Identifier: MIT
******************************************************************************/
package api

import (
	"apworkshop/internal/proto"
	"apworkshop/internal/service"

	"github.com/gin-gonic/gin"
)

//GetStudioInferenceByModelVersionId
func AddStudioModelInferenceRouter(group *gin.RouterGroup) {
	// routers for AIStudio
	group.GET("studioModelVersionInference/:modelId/:versionId", Wrapper(GetStudioModelVersionInference))
}

func GetStudioModelVersionInference(c *gin.Context) error {
	var err error
	var reqContent proto.DescribeModelVersionReq

	err = c.ShouldBindUri(&reqContent)
	if err != nil {
		return ParameterError(c, err.Error())
	}
	errRsp := PreHandler(c, &reqContent)
	if errRsp != nil {
		return errRsp
	}

	inference, errCode, err := service.GetStudioInferenceByModelVersionId(reqContent.ModelVersionId)

	if err != nil {
		return AppError(c, errCode, err.Error())
	}

	data := &proto.DescribeStudioModelVersionInferenceRsp{
		ModelVersionInference: inference,
	}

	return SuccessResp(c, data)
}
