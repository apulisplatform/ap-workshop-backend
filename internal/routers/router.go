/* ******************************************************************************
* 2019 - present Contributed by Apulis Technology (Shenzhen) Co. LTD
*
* This program and the accompanying materials are made available under the
* terms of the MIT License, which is available at
* https://www.opensource.org/licenses/MIT
*
* See the NOTICE file distributed with this work for additional
* information regarding copyright ownership.
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations
* under the License.
*
* SPDX-License-Identifier: MIT
******************************************************************************/
package api

import (
	"apworkshop/internal/configs"
	"apworkshop/internal/loggers"

	"github.com/gin-contrib/cors"
	"github.com/gin-contrib/sessions"
	"github.com/gin-contrib/sessions/cookie"
	"github.com/gin-gonic/gin"
	ginSwagger "github.com/swaggo/gin-swagger"
	"github.com/swaggo/gin-swagger/swaggerFiles"
)

var appconfig *configs.APWorkshopConfig

// @title ApulisEdge Cloud API
// @version alpha
// @description ApulisEdge cloud server.
func NewRouter(config *configs.APWorkshopConfig) *gin.Engine {
	appconfig = config

	r := gin.New()

	r.GET("/swagger/*any", ginSwagger.DisablingWrapHandler(swaggerFiles.Handler, "DISABLE_SWAGGER"))
	r.GET("/health", HandleProb)

	store := cookie.NewStore([]byte("secret"))
	r.Use(sessions.Sessions("mysession", store))

	r.Use(cors.Default())

	r.NoMethod(HandleNotFound)
	r.NoRoute(HandleNotFound)

	r.Use(loggers.GinLogger(logger))
	r.Use(gin.Recovery())

	v1Group := r.Group(config.Http.APIPrefix)
	authGroup := r.Group(config.Http.APIPrefix)

	// Huawei 松山湖 routers
	AddModelRouter(v1Group)
	AddModelVersionRouter(v1Group)
	AddModelLockRouter(v1Group)

	// AIStudio routers
	AddAuthRouter(authGroup)
	AddStudioModelRouter(v1Group)
	AddStudioModelVersionRouter(v1Group)
	AddStudioModelLockRouter(v1Group)
	AddStudioModelInferenceRouter(v1Group)

	return r
}
