/* ******************************************************************************
* 2019 - present Contributed by Apulis Technology (Shenzhen) Co. LTD
*
* This program and the accompanying materials are made available under the
* terms of the MIT License, which is available at
* https://www.opensource.org/licenses/MIT
*
* See the NOTICE file distributed with this work for additional
* information regarding copyright ownership.
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations
* under the License.
*
* SPDX-License-Identifier: MIT
******************************************************************************/
package api

import (
	"apworkshop/internal/proto"
	"apworkshop/internal/service"

	"github.com/gin-gonic/gin"
)

func AddModelVersionRouter(group *gin.RouterGroup) {
	// routers for Huawei(松山湖)
	group.GET("modelVersionsByModel/:modelId", Wrapper(GetModelVersionListByModel))
	group.GET("modelVersion/:modelId/:versionId", Wrapper(DescribeModelVersion))
	group.DELETE("modelVersion/:versionId", Wrapper(DeleteModelVersion))

}

func GetModelVersionListByModel(c *gin.Context) error {
	var err error
	var reqContent proto.GetModelVersionListByModelReq

	err = c.ShouldBindUri(&reqContent)
	if err != nil {
		return ParameterError(c, err.Error())
	}
	err = c.ShouldBindQuery(&reqContent)
	if err != nil {
		return ParameterError(c, err.Error())
	}
	errRsp := PreHandler(c, &reqContent)
	if errRsp != nil {
		return errRsp
	}

	versionList, errCode, err := service.GetModelVersionListByModel(reqContent)
	if err != nil {
		return AppError(c, errCode, err.Error())
	}

	total, errCode, err := service.GetModelVersionCount(reqContent.ModelId)
	if err != nil {
		return AppError(c, errCode, err.Error())
	}

	var resultList []proto.GetModelVersionListByModelRspUnit
	for _, version := range versionList {
		inference, errCode, err := service.GetInferenceByModelVersionId(int(version.ID))
		if err != nil {
			return AppError(c, errCode, err.Error())
		}
		unit := proto.GetModelVersionListByModelRspUnit{
			ModelVersion: version,
			InferInfo:    inference,
		}
		resultList = append(resultList, unit)
	}

	data := &proto.GetModelVersionListByModelRsp{
		Items: resultList,
		Total: total,
	}

	return SuccessResp(c, data)
}

func DescribeModelVersion(c *gin.Context) error {
	var err error
	var reqContent proto.DescribeModelVersionReq

	err = c.ShouldBindUri(&reqContent)
	if err != nil {
		return ParameterError(c, err.Error())
	}
	errRsp := PreHandler(c, &reqContent)
	if errRsp != nil {
		return errRsp
	}

	version, errCode, err := service.DescribeModelVersion(reqContent)
	if err != nil {
		return AppError(c, errCode, err.Error())
	}

	model, errCode, err := service.DescribeModel(proto.DescribeModelReq{
		ModelId: reqContent.ModelId,
	})
	if err != nil {
		return AppError(c, errCode, err.Error())
	}

	data := &proto.DescribeModelVersionRsp{
		ModelInfo:    model,
		ModelVersion: *version,
	}

	return SuccessResp(c, data)
}

func DeleteModelVersion(c *gin.Context) error {
	var err error
	var reqContent proto.DeleteModelVersionReq

	err = c.ShouldBindUri(&reqContent)
	if err != nil {
		return ParameterError(c, err.Error())
	}
	errRsp := PreHandler(c, &reqContent)
	if errRsp != nil {
		return errRsp
	}

	errCode, err := service.DeleteModelVersion(reqContent)
	if err != nil {
		return AppError(c, errCode, err.Error())
	}

	data := "success"
	return SuccessResp(c, data)
}
