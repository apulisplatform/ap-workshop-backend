/* ******************************************************************************
* 2019 - present Contributed by Apulis Technology (Shenzhen) Co. LTD
*
* This program and the accompanying materials are made available under the
* terms of the MIT License, which is available at
* https://www.opensource.org/licenses/MIT
*
* See the NOTICE file distributed with this work for additional
* information regarding copyright ownership.
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations
* under the License.
*
* SPDX-License-Identifier: MIT
******************************************************************************/
package api

import (
	"apworkshop/internal/appErr"
	"bytes"
	"io/ioutil"
	"net/http"

	"github.com/gin-gonic/gin"
)

type HandlerFunc func(c *gin.Context) error

type APIErrorResp struct {
	Code int    `json:"code"`
	Msg  string `json:"msg"`
}

type APISuccessResp struct {
	Code int         `json:"code"`
	Msg  string      `json:"msg"`
	Data interface{} `json:"data"`
}

type APISuccessNoContentResp struct {
	Code int    `json:"code"`
	Msg  string `json:"msg"`
}

func Wrapper(handler HandlerFunc) func(c *gin.Context) {
	return func(c *gin.Context) {
		if appconfig.DebugMode {
			buf := make([]byte, 1024000)
			n, _ := c.Request.Body.Read(buf)
			logger.Debugln("==================== request body below ==========")
			logger.Debugln(string(buf[0:n]))
			logger.Debugln("==================== request body above ==========")
			c.Request.Body = ioutil.NopCloser(bytes.NewBuffer(buf))
		}
		err := handler(c)
		if err != nil {
			logger.Error(err.Error())
		}
	}
}

//////////////// Success Message //////////////////
func SuccessResp(c *gin.Context, content interface{}) error {
	res := APISuccessResp{
		Code: appErr.SUCCESS_CODE,
		Msg:  appErr.SUCCESS_MSG,
		Data: content,
	}
	c.JSON(http.StatusOK, res)
	return nil
}

func SuccessRespNoContent(c *gin.Context) error {
	res := APISuccessNoContentResp{
		Code: appErr.SUCCESS_CODE,
		Msg:  appErr.SUCCESS_MSG,
	}
	c.JSON(http.StatusOK, res)
	return nil
}

//////////////// Error Message //////////////////
func (e *APIErrorResp) Error() string {
	return e.Msg
}

func ErrorResp(code int, msg string) *APIErrorResp {
	res := &APIErrorResp{
		Code: code,
		Msg:  msg,
	}
	return res
}

func HandleNotFound(c *gin.Context) {
	// no need to handle err here
	errRsp := ErrorResp(appErr.NOT_FOUND_ERROR_CODE, http.StatusText(http.StatusNotFound))
	c.JSON(http.StatusNotFound, errRsp)
}

func HandleProb(c *gin.Context) {
	logger.Infoln("health check")
	c.JSON(http.StatusOK, appErr.SUCCESS_MSG)
}

func ErrorNoBodyResp(code int, msg string) *APIErrorResp {
	errMsg := &APIErrorResp{
		Code: code,
		Msg:  msg,
	}
	return errMsg
}

func ServerError(c *gin.Context) *APIErrorResp {
	errRsp := ErrorResp(appErr.SERVER_ERROR_CODE, http.StatusText(http.StatusInternalServerError))
	c.JSON(http.StatusInternalServerError, errRsp)
	return errRsp
}

func NotFoundError(c *gin.Context) *APIErrorResp {
	errRsp := ErrorResp(appErr.NOT_FOUND_ERROR_CODE, http.StatusText(http.StatusNotFound))
	c.JSON(http.StatusNotFound, errRsp)
	return errRsp
}

func UnknownError(c *gin.Context, msg string) *APIErrorResp {
	errRsp := ErrorResp(appErr.UNKNOWN_ERROR_CODE, msg)
	c.JSON(http.StatusInternalServerError, errRsp)
	return errRsp
}

func ParameterError(c *gin.Context, msg string) *APIErrorResp {
	errRsp := ErrorResp(appErr.PARAMETER_ERROR_CODE, msg)
	c.JSON(http.StatusBadRequest, errRsp)
	return errRsp
}

func AppError(c *gin.Context, errCode int, msg string) *APIErrorResp {
	errRsp := ErrorResp(errCode, msg)
	c.JSON(http.StatusBadRequest, errRsp)
	return errRsp
}
