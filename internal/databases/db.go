/* ******************************************************************************
* 2019 - present Contributed by Apulis Technology (Shenzhen) Co. LTD
*
* This program and the accompanying materials are made available under the
* terms of the MIT License, which is available at
* https://www.opensource.org/licenses/MIT
*
* See the NOTICE file distributed with this work for additional
* information regarding copyright ownership.
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations
* under the License.
*
* SPDX-License-Identifier: MIT
******************************************************************************/
package databases

import (
	"apworkshop/internal/configs"
	"apworkshop/internal/loggers"
	"database/sql"
	"fmt"

	"gorm.io/driver/postgres"

	"log"

	"gorm.io/gorm"
	gormlogger "gorm.io/gorm/logger"

	_ "github.com/jinzhu/gorm/dialects/postgres"
	"gorm.io/driver/mysql"
)

var Db *gorm.DB
var DbType string
var logger = loggers.LogInstance()

const (
	DB_TYPE_MYSQL    = "mysql"
	DB_TYPE_POSTGRES = "postgres"
)

func checkPgCreateDatabase(db *sql.DB, dbName string) error {

	exist := 0
	err := db.QueryRow("SELECT count(1) FROM pg_database WHERE datname = $1", dbName).Scan(&exist)
	if err != nil {
		logger.Errorf("select count(1) err = %+v", err)
		return err
	}

	if exist == 0 {
		_, err = db.Exec("CREATE DATABASE " + dbName)
		if err != nil {
			logger.Errorf("create database err = %+v", err)
			return err
		}
	}

	return nil
}

func InitDatabase(dbConf *configs.DbConfig, openDebug bool) error {
	DbType = dbConf.Type

	if DbType == DB_TYPE_MYSQL {
		mysqlConf := dbConf
		sqlDb, err := sql.Open("mysql", fmt.Sprintf("%s:%s@tcp(%s:%d)/?charset=utf8&parseTime=True&loc=Local",
			mysqlConf.Username, mysqlConf.Password, mysqlConf.Host, mysqlConf.Port))
		if err != nil {
			return err
		}

		_, err = sqlDb.Exec("CREATE DATABASE IF NOT EXISTS " + mysqlConf.Database)
		if err != nil {
			return err
		}

		sqlDb, err = sql.Open("mysql", fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=utf8&parseTime=True&loc=Local",
			mysqlConf.Username, mysqlConf.Password, mysqlConf.Host, mysqlConf.Port, mysqlConf.Database))
		if err != nil {
			return err
		}

		var lvl gormlogger.LogLevel
		if openDebug {
			lvl = gormlogger.Info
		} else {
			lvl = gormlogger.Warn
		}

		newLogger := gormlogger.New(
			log.New(logger.Out, "\r\n", log.LstdFlags),
			gormlogger.Config{
				LogLevel: lvl,
			},
		)

		Db, err = gorm.Open(mysql.New(mysql.Config{
			Conn: sqlDb,
		}), &gorm.Config{Logger: newLogger})
		if err != nil {
			return err
		}

		logger.Info("DB connected success")
		sqlDb.SetMaxOpenConns(mysqlConf.MaxOpenConns)
		sqlDb.SetMaxIdleConns(mysqlConf.MaxIdleConns)
		return nil

	} else if DbType == DB_TYPE_POSTGRES {

		// create connection only
		postgresConf := dbConf

		sslmode := "require"
		if len(postgresConf.Sslmode) > 0 {
			sslmode = postgresConf.Sslmode
		}
		sqlDb, err := sql.Open("postgres", fmt.Sprintf("host=%s port=%d user=%s password=%s sslmode=%s",
			postgresConf.Host, postgresConf.Port, postgresConf.Username, postgresConf.Password, sslmode))

		if err != nil {
			logger.Errorf("open db error = %+v", err)
			return err
		}

		err = checkPgCreateDatabase(sqlDb, postgresConf.Database)
		if err != nil {
			return err
		}

		var lvl gormlogger.LogLevel
		if openDebug {
			lvl = gormlogger.Info
		} else {
			lvl = gormlogger.Warn
		}

		newLogger := gormlogger.New(
			log.New(logger.Out, "\r\n", log.LstdFlags),
			gormlogger.Config{
				LogLevel: lvl,
			},
		)

		sslmode = "require"
		if len(postgresConf.Sslmode) > 0 {
			sslmode = postgresConf.Sslmode
		}
		dsn := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=%s", postgresConf.Host, postgresConf.Port, postgresConf.Username, postgresConf.Password, postgresConf.Database, sslmode)
		Db, err = gorm.Open(postgres.Open(dsn), &gorm.Config{Logger: newLogger})

		if err != nil {
			logger.Errorf("open db error = %+v", err)
			return err
		}

		logger.Info("DB connected success")
		sqlDb.SetMaxOpenConns(postgresConf.MaxOpenConns)
		sqlDb.SetMaxIdleConns(postgresConf.MaxIdleConns)

		sqlDb.Close()

		return nil
	} else {
		return fmt.Errorf("Database type %s not supported\n", DbType)
	}
}

func CreateTableIfNotExists(modelType interface{}) error {
	var err error

	if DbType == DB_TYPE_MYSQL {

		if err = Db.
			Set("gorm:table_options", "ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8").
			AutoMigrate(modelType); err != nil {
			logger.Errorf("AutoMigrate failed! table = %s", modelType)
			return err
		}

		return nil

	} else if DbType == DB_TYPE_POSTGRES {

		if err = Db.AutoMigrate(modelType); err != nil {
			logger.Errorf("AutoMigrate failed! table = %s", modelType)
			return err
		}

		return nil
	} else {
		return fmt.Errorf("Database type %s not supported\n", DbType)
	}

}
