/* ******************************************************************************
* 2019 - present Contributed by Apulis Technology (Shenzhen) Co. LTD
*
* This program and the accompanying materials are made available under the
* terms of the MIT License, which is available at
* https://www.opensource.org/licenses/MIT
*
* See the NOTICE file distributed with this work for additional
* information regarding copyright ownership.
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations
* under the License.
*
* SPDX-License-Identifier: MIT
******************************************************************************/
package validators

import (
	"apworkshop/internal/appErr"
	"apworkshop/internal/common"
	"apworkshop/internal/entity"
)

var validWorker = ValidatorInstance()

func ValidateManifest(manifest *entity.ModelDbStruct) (int, error) {
	// some simple validate method was set on validate tag in define structure
	err := validWorker.Struct(manifest)
	if err != nil {
		return appErr.MANIFEST_VALIDATE_ERROR_CODE, err
	}

	if len(manifest.Dataset) != 0 {
		for _, dataset := range manifest.Dataset {
			if dataset.Usage == "" ||
				len(dataset.Type) == 0 ||
				dataset.Format == "" ||
				dataset.Name == "" ||
				dataset.Path == "" {
				return appErr.MANIFEST_DATASET_NOT_VALID_ERROR_CODE, appErr.MANIFEST_DATASET_NOT_VALID_ERROR
			}
		}
	}

	if manifest.Train != nil {
		if manifest.Train.Entry == "" ||
			len(manifest.Train.SysParams) == 0 {
			return appErr.MANIFEST_TRAIN_NOT_VALID_ERROR_CODE, appErr.MANIFEST_TRAIN_NOT_VALID_ERROR
		}
	}

	if manifest.Eval != nil {
		if manifest.Eval.Entry == "" ||
			len(manifest.Eval.SysParams) == 0 {
			return appErr.MANIFEST_EVAL_NOT_VALID_ERROR_CODE, appErr.MANIFEST_EVAL_NOT_VALID_ERROR
		}
	}
	return appErr.SUCCESS_CODE, nil
}

func ValidateInferenceManifest(inference *entity.InferStruct) (int, error) {
	// some simple validate method was set on validate tag in define structure
	err := validWorker.Struct(inference)
	if err != nil {
		return appErr.INFERENCE_VALIDATE_ERROR_CODE, err
	}

	// if inference.Center == nil && inference.Edge == nil {
	// 	return errors.New("no inference node define")
	// }

	if inference.Center != nil {
		if inference.Center.Framework == "" ||
			inference.Center.DeviceModel == "" ||
			inference.Center.DeviceType == "" {
			return appErr.INFERENCE_CENTER_NOT_VALID_ERROR_CODE, appErr.INFERENCE_CENTER_NOT_VALID_ERROR
		}
	}
	if inference.Edge != nil {
		if inference.Edge.Framework == "" ||
			inference.Edge.DeviceModel == "" ||
			inference.Edge.DeviceType == "" {
			return appErr.INFERENCE_EDGE_NOT_VALID_ERROR_CODE, appErr.INFERENCE_EDGE_NOT_VALID_ERROR
		}
	}
	return appErr.SUCCESS_CODE, nil
}

func ValidateStudioManifest(manifest *entity.StudioModel) (int, error) {
	// some simple validate method was set on validate tag in define structure
	err := validWorker.Struct(manifest)
	if err != nil {
		return appErr.MANIFEST_VALIDATE_ERROR_CODE, err
	}

	for _, device := range manifest.Devices {
		err := validWorker.Struct(device)
		if err != nil {
			return appErr.MANIFEST_VALIDATE_ERROR_CODE, err
		}
	}

	if len(manifest.Platform) != 0 {
		logger.Info("---------------------------------------1")
		for _, plt := range manifest.Platform {
			_, ok := common.STUDIO_MODEL_PALTFORM_TYPES[plt]
			if !ok {
				return appErr.MANIFEST_VALIDATE_ERROR_CODE, appErr.MANIFEST_NOT_VALID_ERROR
			}
		}
	}

	if manifest.Source != "" {
		logger.Info("---------------------------------------2")
		_, ok := common.STUDIO_MODEL_SOURCE_TYPES[manifest.Source]
		if !ok {
			return appErr.MANIFEST_VALIDATE_ERROR_CODE, appErr.MANIFEST_NOT_VALID_ERROR
		}
	}

	if manifest.Framework != "" {
		logger.Info("---------------------------------------3")
		_, ok := common.STUDIO_MODEL_FRAMEWORK_TYPES[manifest.Framework]
		if !ok {
			return appErr.MANIFEST_VALIDATE_ERROR_CODE, appErr.MANIFEST_NOT_VALID_ERROR
		}
	}

	if manifest.DistributedFramework != "" {
		logger.Info("---------------------------------------4")
		_, ok := common.STUDIO_MODEL_DISTRIBUTED_FRAMEWORK_TYPES[manifest.DistributedFramework]
		if !ok {
			return appErr.MANIFEST_VALIDATE_ERROR_CODE, appErr.MANIFEST_NOT_VALID_ERROR
		}
	}

	for _, device := range manifest.Devices {
		err := validWorker.Struct(device)
		if err != nil {
			return appErr.MANIFEST_VALIDATE_ERROR_CODE, err
		}
		_, ok := common.STUDIO_MODEL_INFER_CENTER_DEVICE_TYPES[device.Type]
		if !ok {
			return appErr.MANIFEST_VALIDATE_ERROR_CODE, appErr.MANIFEST_NOT_VALID_ERROR
		}
	}
	// if manifest.Device.Type != "" {
	// 	logger.Info("---------------------------------------5")
	// 	_, ok := common.STUDIO_MODEL_DEVICE_TYPES[manifest.Device.Type]
	// 	if !ok {
	// 		return appErr.MANIFEST_VALIDATE_ERROR_CODE, appErr.MANIFEST_NOT_VALID_ERROR
	// 	}
	// }

	if manifest.Datasets != nil {
		for _, dataset := range manifest.Datasets {
			logger.Info("---------------------------------------7")
			err := validWorker.Struct(dataset)
			if err != nil {
				return appErr.MANIFEST_VALIDATE_ERROR_CODE, err
			}
			if dataset.Usage != "" {
				_, ok := common.STUDIO_MODEL_DATASETS_USAGE_TYPES[dataset.Usage]
				if !ok {
					return appErr.MANIFEST_VALIDATE_ERROR_CODE, appErr.MANIFEST_NOT_VALID_ERROR
				}
			}
			if dataset.FileFormat != "" {
				_, ok := common.STUDIO_MODEL_DATASETS_FILE_FORMAT_TYPES[dataset.FileFormat]
				if !ok {
					return appErr.MANIFEST_VALIDATE_ERROR_CODE, appErr.MANIFEST_NOT_VALID_ERROR
				}
			}
		}
	}

	if manifest.Train.Visualization != "" {
		logger.Info("---------------------------------------8")
		_, ok := common.STUDIO_MODEL_TRAIN_VISUALIZATION_TYPES[manifest.Train.Visualization]
		if !ok {
			return appErr.MANIFEST_VALIDATE_ERROR_CODE, appErr.MANIFEST_NOT_VALID_ERROR
		}
	}

	if manifest.Train != nil {
		logger.Info("---------------------------------------10")
		if manifest.Train.Entry == "" ||
			len(manifest.Train.SysParams) == 0 {
			return appErr.MANIFEST_TRAIN_NOT_VALID_ERROR_CODE, appErr.MANIFEST_TRAIN_NOT_VALID_ERROR
		}
	}

	if manifest.Eval != nil {
		logger.Info("---------------------------------------11")
		if manifest.Eval.Entry == "" ||
			len(manifest.Eval.SysParams) == 0 {
			return appErr.MANIFEST_EVAL_NOT_VALID_ERROR_CODE, appErr.MANIFEST_EVAL_NOT_VALID_ERROR
		}
	}
	return appErr.SUCCESS_CODE, nil
}

func ValidateStudioInferenceManifest(inference *entity.StudioInference) (int, error) {
	// some simple validate method was set on validate tag in define structure
	err := validWorker.Struct(inference)
	if err != nil {
		return appErr.INFERENCE_VALIDATE_ERROR_CODE, err
	}

	if inference.Center == nil && inference.Edge == nil {
		return appErr.NO_INFERENCE_ERROR_CODE, appErr.NO_INFERENCE_ERROR
	}

	if len(inference.Platform) != 0 {
		for _, plt := range inference.Platform {
			_, ok := common.STUDIO_MODEL_PALTFORM_TYPES[plt]
			if !ok {
				return appErr.INFERENCE_VALIDATE_ERROR_CODE, appErr.INFERENCE_NOT_VALID_ERROR
			}
		}
	}

	// inference center validation
	if inference.Center != nil {
		if inference.Center.Framework == "" {
			return appErr.INFERENCE_CENTER_NOT_VALID_ERROR_CODE, appErr.INFERENCE_CENTER_NOT_VALID_ERROR
		}

		if inference.Center.Framework != "" {
			_, ok := common.STUDIO_MODEL_INFER_CENTER_FRAMEWORK_TYPES[inference.Center.Framework]
			if !ok {
				return appErr.INFERENCE_VALIDATE_ERROR_CODE, appErr.INFERENCE_NOT_VALID_ERROR
			}
		}

		if inference.Center.Format != "" {
			_, ok := common.STUDIO_MODEL_INFER_CENTER_FORMAT_TYPES[inference.Center.Format]
			if !ok {
				return appErr.INFERENCE_VALIDATE_ERROR_CODE, appErr.INFERENCE_NOT_VALID_ERROR
			}
		}

		if inference.Center.Percision != "" {
			_, ok := common.STUDIO_MODEL_INFER_CENTER_PERCISION_TYPES[inference.Center.Percision]
			if !ok {
				return appErr.INFERENCE_VALIDATE_ERROR_CODE, appErr.INFERENCE_NOT_VALID_ERROR
			}
		}

		for _, device := range inference.Center.Devices {
			err := validWorker.Struct(device)
			if err != nil {
				return appErr.INFERENCE_VALIDATE_ERROR_CODE, err
			}
			_, ok := common.STUDIO_MODEL_INFER_CENTER_DEVICE_TYPES[device.Type]
			if !ok {
				return appErr.INFERENCE_VALIDATE_ERROR_CODE, appErr.INFERENCE_NOT_VALID_ERROR
			}
		}
		// if inference.Center.Device.Type != "" {
		// 	_, ok := common.STUDIO_MODEL_INFER_CENTER_DEVICE_TYPES[inference.Center.Device.Type]
		// 	if !ok {
		// 		return appErr.INFERENCE_VALIDATE_ERROR_CODE, appErr.INFERENCE_NOT_VALID_ERROR
		// 	}
		// }

	}

	// inference edge validation
	if inference.Edge != nil {
		if inference.Edge.Framework == "" {
			return appErr.INFERENCE_EDGE_NOT_VALID_ERROR_CODE, appErr.INFERENCE_EDGE_NOT_VALID_ERROR
		}

		if inference.Edge.Framework != "" {
			_, ok := common.STUDIO_MODEL_INFER_EDGE_FRAMEWORK_TYPES[inference.Edge.Framework]
			if !ok {
				return appErr.INFERENCE_VALIDATE_ERROR_CODE, appErr.INFERENCE_NOT_VALID_ERROR
			}
		}

		if inference.Edge.Format != "" {
			_, ok := common.STUDIO_MODEL_INFER_EDGE_FORMAT_TYPES[inference.Edge.Format]
			if !ok {
				return appErr.INFERENCE_VALIDATE_ERROR_CODE, appErr.INFERENCE_NOT_VALID_ERROR
			}
		}

		if inference.Edge.Percision != "" {
			_, ok := common.STUDIO_MODEL_INFER_EDGE_PERCISION_TYPES[inference.Edge.Percision]
			if !ok {
				return appErr.INFERENCE_VALIDATE_ERROR_CODE, appErr.INFERENCE_NOT_VALID_ERROR
			}
		}

		for _, device := range inference.Edge.Devices {
			err := validWorker.Struct(device)
			if err != nil {
				return appErr.INFERENCE_VALIDATE_ERROR_CODE, err
			}
			_, ok := common.STUDIO_MODEL_INFER_EDGE_DEVICE_TYPES[device.Type]
			if !ok {
				return appErr.INFERENCE_VALIDATE_ERROR_CODE, appErr.INFERENCE_NOT_VALID_ERROR
			}
		}
		// if inference.Edge.Device.Type != "" {
		// _, ok := common.STUDIO_MODEL_INFER_EDGE_DEVICE_TYPES[inference.Edge.Device.Type]
		// if !ok {
		// 	return appErr.INFERENCE_VALIDATE_ERROR_CODE, appErr.INFERENCE_NOT_VALID_ERROR
		// }
		// }

	}
	return appErr.SUCCESS_CODE, nil
}
