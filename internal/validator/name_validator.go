/* ******************************************************************************
* 2019 - present Contributed by Apulis Technology (Shenzhen) Co. LTD
*
* This program and the accompanying materials are made available under the
* terms of the MIT License, which is available at
* https://www.opensource.org/licenses/MIT
*
* See the NOTICE file distributed with this work for additional
* information regarding copyright ownership.
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations
* under the License.
*
* SPDX-License-Identifier: MIT
******************************************************************************/
package validators

import (
	"regexp"
	"unicode"

	"github.com/go-playground/validator/v10"
)

func checkDescription(fl validator.FieldLevel) bool {
	description := fl.Field().String()
	if description == "" {
		return true
	}
	if len(description) > 140 {
		return false
	}
	return true
}

func checkName(fl validator.FieldLevel) bool {
	name := fl.Field().String()
	// don't handle empty name
	if name == "" {
		return true
	}

	// if HasChineseChar(name) {
	// 	return false
	// }

	if HasSpace(name) {
		return false
	}

	// if !IsStartWithEnglish(name) {
	// 	return false
	// }

	if len(name) > 100 {
		return false
	}

	if HasIllegalCharacter(name) {
		return false
	}

	return true
}

func HasChineseChar(str string) bool {
	for _, r := range str {
		if unicode.Is(unicode.Scripts["Han"], r) || (regexp.MustCompile("[\u3002\uff1b\uff0c\uff1a\u201c\u201d\uff08\uff09\u3001\uff1f\u300a\u300b]").MatchString(string(r))) {
			return true
		}
	}
	return false
}

func HasSpace(str string) bool {
	re := `[\ ]`
	r := regexp.MustCompile(re)
	return r.MatchString(str)
}

func IsStartWithEnglish(str string) bool {
	re := `^[a-z].*`
	r := regexp.MustCompile(re)
	return r.MatchString(str)
}

func HasIllegalCharacter(str string) bool {
	re := `[^a-zA-Z0-9-._\p{Han}]`
	r := regexp.MustCompile(re)
	return r.MatchString(str)
}
