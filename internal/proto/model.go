/* ******************************************************************************
* 2019 - present Contributed by Apulis Technology (Shenzhen) Co. LTD
*
* This program and the accompanying materials are made available under the
* terms of the MIT License, which is available at
* https://www.opensource.org/licenses/MIT
*
* See the NOTICE file distributed with this work for additional
* information regarding copyright ownership.
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations
* under the License.
*
* SPDX-License-Identifier: MIT
******************************************************************************/
package proto

import "apworkshop/internal/entity"

type TestReq struct {
}

type CreateModelReq struct {
	FilePath         string `json:"filePath" validate:"required"`
	FileName         string `json:"fileName" validate:"required"`
	Scope            string `json:"scope"`
	Producer         string `json:"producer"`
	ModelName        string `json:"modelName" validate:"required,checkName"`
	ModelDescription string `json:"modelDescription"`
}

type PrepareModelReq struct {
	Scope            string `json:"scope" validate:"required"`
	Context          string `json:"context" validate:"required"`
	ModelName        string `json:"modelName" validate:"required"`
	ModelDescription string `json:"modelDescription"`
}

type PrepareModelRsp struct {
	Lock           string `json:"lock"`
	Path           string `json:"path"`
	ModelId        int    `json:"modelId"`
	ModelVersionId int    `json:"modelVersionId"`
}

type CommitModelReq struct {
	Lock   string `json:"lock"`
	Status string `json:"status" validate:"required"`
}

type DescribeModelReq struct {
	ModelId int `uri:"modelId" validate:"required"`
}

type DescribeModelRsp struct {
	entity.ModelDbStruct `mapstructure:"squash,"`
	LatestVersionId      int                 `json:"latestVersionId"`
	LatestVersionName    string              `json:"latestVersionName"`
	InferenceInfo        *entity.InferStruct `json:"inferenceInfo"`
}

type GetModelListReq struct {
	Scope     string `form:"scope"`
	PageNum   int    `form:"pageNum"`
	PageSize  int    `form:"pageSize"`
	Platform  string `form:"platform"`
	Name      string `form:"name"`
	Type      string `form:"type"`
	CanInfer  bool   `form:"canInfer"`
	CanTrain  bool   `form:"canTrain"`
	IsPreload bool   `form:"isPreload"`
	Sort      string `form:"sort"`
}

type GetModelListRsp struct {
	Items []GetModelListRspUnit `json:"items"`
	Total int                   `json:"total"`
}

type GetModelListRspUnit struct {
	entity.ModelDbStruct `mapstructure:",squash"`
	InferenceInfo        *entity.InferStruct  `json:"inferenceInfo"`
	VersionCount         int                  `json:"versionCount"`
	LatestVersionInfo    *entity.ModelVersion `json:"latestVersionInfo"`
	LatestVersionId      int                  `json:"latestVersionId"`
	LatestVersionName    string               `json:"latestVersionName"`
}

type DeleteModelReq struct {
	ModelId int `uri:"modelId" validate:"required"`
}

type GetModelTypesRsp struct {
	Types []string `json:"types"`
}
