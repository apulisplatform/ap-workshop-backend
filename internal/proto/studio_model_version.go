/* ******************************************************************************
* 2019 - present Contributed by Apulis Technology (Shenzhen) Co. LTD
*
* This program and the accompanying materials are made available under the
* terms of the MIT License, which is available at
* https://www.opensource.org/licenses/MIT
*
* See the NOTICE file distributed with this work for additional
* information regarding copyright ownership.
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations
* under the License.
*
* SPDX-License-Identifier: MIT
******************************************************************************/
package proto

import "apworkshop/internal/entity"

type DescribeStudioModelVersionRsp struct {
	// ModelInfo    *entity.StudioModel        `json:"modelInfo"`
	ModelVersion *entity.StudioModelVersion `json:"modelVersion"`
}

type DescribeStudioModelVersionInferenceRsp struct {
	ModelVersionInference *entity.StudioInference `json:"modelVersionInference"`
}

// type GetStudioModelVersionListByModelRspUnit struct {
// 	ModelVersion entity.StudioModelVersion `json:"modelVersion"`
// 	InferInfo    *entity.StudioInference   `json:"inferInfo"`
// }

type GetStudioModelVersionListByModelRspUnit struct {
	Name                 string                         `json:"name"`
	CreatedAt            entity.UnixTime                `json:"createdAt"`
	ModelName            string                         `json:"modelName"`
	ModelId              int                            `json:"modelId"`
	VersionId            int                            `json:"versionId"`
	Description          string                         `json:"description"`
	Backbone             string                         `json:"backbone"`
	Engine               string                         `json:"engine"`
	StoragePath          string                         `json:"storagePath"`
	TrainDevices         entity.StudioModelDevice       `json:"trainDevices"`
	CenterInferDevices   entity.StudioModelDevice       `json:"centerInferDevices"`
	EdgeInferDevices     entity.StudioModelDevice       `json:"edgeInferDevices"`
	DatasetName          string                         `json:"datasetName"`
	CenterInferPercision string                         `json:"centerInferPercision"`
	EdgeInferPercision   string                         `json:"edgeInferPercision"`
	Parameters           string                         `json:"parameters"`
	Publisher            string                         `json:"publisher"`
	TrainTime            int                            `json:"trainTime"`
	Evaluation           entity.StudioModelEvaluation   `json:"evaluation"`
	PublishTime          entity.UnixTime                `json:"publishTime"`
	VersionCount         int                            `json:"versionCount"`
	Tags                 entity.TagsStruct              `json:"tags"`
	Train                *entity.StudioModelTrainStruct `json:"train"`
	Eval                 *entity.StudioModelEvalStruct  `json:"eval"`
}

type GetStudioModelVersionListByModelRsp struct {
	Items []GetStudioModelVersionListByModelRspUnit `json:"items"`
	Total int                                       `json:"total"`
}

type GetStudioModelVersionListByModelReq struct {
	ModelId  int    `uri:"modelId" validate:"required"`
	PageSize int    `form:"pageSize" validate:"required"`
	PageNum  int    `form:"pageNum" validate:"required"`
	Sort     string `form:"sort"`
}

type GetStudioModelVersionYamlRsp struct {
	InferYaml    string `json:"infer.yaml"`
	ManifestYaml string `json:"manifest.yaml"`
}
