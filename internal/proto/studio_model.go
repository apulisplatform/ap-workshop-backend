/* ******************************************************************************
* 2019 - present Contributed by Apulis Technology (Shenzhen) Co. LTD
*
* This program and the accompanying materials are made available under the
* terms of the MIT License, which is available at
* https://www.opensource.org/licenses/MIT
*
* See the NOTICE file distributed with this work for additional
* information regarding copyright ownership.
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations
* under the License.
*
* SPDX-License-Identifier: MIT
******************************************************************************/
package proto

import "apworkshop/internal/entity"

type GetStudioModelListReq struct {
	Scope          string `form:"scope"`
	PageNum        int    `form:"pageNum"`
	PageSize       int    `form:"pageSize"`
	DeviceType     string `form:"deviceType"`
	Field          string `form:"field"`
	Framework      string `form:"framework"`
	Source         string `form:"source"`
	Task           string `form:"task"`
	Name           string `form:"name"`
	CanEdgeInfer   bool   `form:"canEdgeInfer"`
	CanCenterInfer bool   `form:"canCenterInfer"`
	CanTrain       bool   `form:"canTrain"`
	IsPreload      bool   `form:"isPreload"`
	Sort           string `form:"sort"`
	StartTime      int    `form:"startTime"`
	EndTime        int    `form:"endTime"`
}

type DescribeStudioModelRsp struct {
	// entity.StudioModel `mapstructure:"squash,"`
	entity.GormModel
	Scope                string                   `json:"scope"`
	Source               string                   `json:"source"`
	AutoDL               bool                     `json:"autoDL"`
	Logo                 string                   `json:"logo"`
	Name                 string                   `json:"name"`
	Framework            string                   `json:"framework"`
	DistributedFramework string                   `json:"distributedFramework"`
	Backbone             string                   `json:"backbone"`
	Description          string                   `json:"description"`
	Engine               string                   `json:"engine"`
	DatasetName          string                   `json:"datasetName"`
	Devices              entity.StudioModelDevice `json:"devices"`
	Field                string                   `json:"field"`
	Task                 string                   `json:"task" yaml:"task"`
	DeviceType           string                   `json:"deviceType"`
	Ability              []string                 `json:"ability"`
	CanTrain             bool                     `json:"canTrain"`
	CanCenterInfer       bool                     `json:"canCenterInfer"`
	CanEdgeInfer         bool                     `json:"canEdgeInfer"`
	IsPreload            bool                     `json:"isPreload"`
	Status               string                   `json:"status"`
	ReadMe               string                   `json:"readMe"`

	LatestVersionId   int                     `json:"latestVersionId"`
	LatestVersionName string                  `json:"latestVersionName"`
	VersionCount      int                     `json:"versionCount"`
	InferenceInfo     *entity.StudioInference `json:"inferenceInfo"`
}

type GetStudioModelListRspUnit struct {
	// entity.StudioModel `mapstructure:",squash"`
	// InferenceInfo      *entity.StudioInference    `json:"inferenceInfo"`
	// LatestVersionInfo *entity.StudioModelVersion `json:"latestVersionInfo"`
	entity.GormModel
	Scope                string                   `json:"scope"`
	Source               string                   `json:"source"`
	AutoDL               bool                     `json:"autoDL"`
	Logo                 string                   `json:"logo"`
	LogoPic              string                   `json:"logoPic"`
	Name                 string                   `json:"name"`
	Framework            string                   `json:"framework"`
	DistributedFramework string                   `json:"distributedFramework"`
	Backbone             string                   `json:"backbone"`
	Description          string                   `json:"description"`
	ReadMe               string                   `json:"readMe"`
	Engine               string                   `json:"engine"`
	Devices              entity.StudioModelDevice `json:"devices"`
	Field                string                   `json:"field"`
	Task                 string                   `json:"task" yaml:"task"`
	DeviceType           string                   `json:"deviceType"`
	Ability              []string                 `json:"ability"`
	CanTrain             bool                     `json:"canTrain"`
	CanCenterInfer       bool                     `json:"canCenterInfer"`
	CanEdgeInfer         bool                     `json:"canEdgeInfer"`
	IsPreload            bool                     `json:"isPreload"`
	Status               string                   `json:"status"`
	VersionCount         int                      `json:"versionCount"`
	LatestVersionId      int                      `json:"latestVersionId"`
	LatestVersionName    string                   `json:"latestVersionName"`
	ShowList             []string                 `json:"showList"`
}

type GetStudioModelListRsp struct {
	Items []GetStudioModelListRspUnit `json:"items"`
	Total int                         `json:"total"`
}

type GetStudioModelOverview struct {
	PresetModel    int `json:"preset"`
	PublishedModel int `json:"published"`
	UserDefine     int `json:"userDefine"`
}

type GetStudioModelVarietyTypes struct {
	Types []string `json:"types"`
}

type CreateStudioModelReq struct {
	FilePath string `json:"filePath" validate:"required"`
	FileName string `json:"fileName" validate:"required"`
	Scope    string `json:"scope"`
	// Producer         string `json:"producer"`
	ModelName string `json:"modelName" validate:"required,checkName"`
	// ModelName        string `json:"modelName" validate:"required"`
	ModelDescription string `json:"modelDescription" validate:"checkDescription"`
}

type StudioManifestInferReq struct {
	StudioModelInference string `json:"studioInference"`
	StudioModel          string `json:"studioModel"`
}

type StudioModelGetTmpReq struct {
	UserName       string `json:"userName" validate:"required"`
	ModelId        int    `json:"modelId"`
	ModelVersionId int    `json:"modelVersionId"`
}

type StudioModelGetTmpRsp struct {
	TmpStoragePath string `json:"TmpStoragePath"`
}

type StudioPrepareModelReq struct {
	IsTmp            int    `json:"isTmp"`
	Scope            string `json:"scope" validate:"required"`
	Context          string `json:"context" validate:"required"`
	UserName         string `json:"userName"`
	ModelName        string `json:"modelName" validate:"required"`
	ModelDescription string `json:"modelDescription"`
}

type StudioPrepareModelRsp struct {
	Context        string `json:"context"`
	Path           string `json:"path"`
	ModelId        int    `json:"modelId"`
	ModelVersionId int    `json:"modelVersionId"`
}

type StudioCheckInferenceModelRsp struct {
	CanInference  bool `json:"canInference"`
	NeedTransform bool `json:"needTransform"`
}

type StudioCommitModelReq struct {
	Context string `json:"context"`
	Status  string `json:"status" validate:"required"`
}

type StudioTransformModelReq struct {
	ModelId        int    `json:"modelId" validate:"required"`
	ModelVersionId int    `json:"modelVersionId" validate:"required"`
	OriginalFormat string `json:"originalFormat"`
	TargetFormat   string `json:"targetFormat"`
	DeviceType     string `json:"deviceType" validate:"required"`
	ServiceId      string `json:"serviceId"`
	QueueName      string `json:"queueName"`
}

type StudioModelTransformRsp struct {
	ModelId        int    `json:"modelId"`
	ModelVersionId int    `json:"modelVersionId"`
	Success        bool   `json:"success"`
	ServiceId      string `json:"serviceId"`
}

type StudioModelTypesReq struct {
	Field string `form:"field"`
}

type StudioDownLoadModelReq struct {
	ModelId        int `json:"modelId"`
	ModelVersionId int `json:"modelVersionId"`
}

type StudioDownLoadModelRsp struct {
	Dir          string `json:"dir"`
	DownloadLink string `json:"downloadLink"`
	StoragePath  string `json:"storagePath"`
}

type HarborSuccessResp struct {
	Code int               `json:"code"`
	Msg  string            `json:"msg"`
	Data HarborSuccessData `json:"data"`
}

type HarborSuccessData struct {
	IfExist bool `json:"ifExist"`
}

type FileServerSuccessResp struct {
	Code int                   `json:"code"`
	Msg  string                `json:"msg"`
	Data FileServerSuccessData `json:"data"`
}

type FileServerSuccessData struct {
	Dir          string `json:"dir"`
	DownloadLink string `json:"downloadLink"`
	StoragePath  string `json:"storagePath"`
}
