/* ******************************************************************************
* 2019 - present Contributed by Apulis Technology (Shenzhen) Co. LTD
*
* This program and the accompanying materials are made available under the
* terms of the MIT License, which is available at
* https://www.opensource.org/licenses/MIT
*
* See the NOTICE file distributed with this work for additional
* information regarding copyright ownership.
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations
* under the License.
*
* SPDX-License-Identifier: MIT
******************************************************************************/
package proto

import "apworkshop/internal/entity"

type GetModelVersionListByModelReq struct {
	ModelId  int `uri:"modelId" validate:"required"`
	PageSize int `form:"pageSize" validate:"required"`
	PageNum  int `form:"pageNum" validate:"required"`
}

type GetModelVersionListByModelRsp struct {
	Items []GetModelVersionListByModelRspUnit `json:"items"`
	Total int                                 `json:"total"`
}

type GetModelVersionListByModelRspUnit struct {
	entity.ModelVersion
	InferInfo *entity.InferStruct `json:"inferInfo"`
}
type DescribeModelVersionReq struct {
	ModelId        int `uri:"modelId" validate:"required"`
	ModelVersionId int `uri:"versionId" validate:"required"`
}

type DescribeModelVersionRsp struct {
	ModelInfo           *entity.ModelDbStruct `json:"modelInfo"`
	entity.ModelVersion `mapstructure:"squash,"`
}

type DeleteModelVersionReq struct {
	ModelVersionId int `uri:"versionId" validate:"required"`
}
