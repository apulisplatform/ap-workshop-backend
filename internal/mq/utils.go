/* ******************************************************************************
* 2019 - present Contributed by Apulis Technology (Shenzhen) Co. LTD
*
* This program and the accompanying materials are made available under the
* terms of the MIT License, which is available at
* https://www.opensource.org/licenses/MIT
*
* See the NOTICE file distributed with this work for additional
* information regarding copyright ownership.
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations
* under the License.
*
* SPDX-License-Identifier: MIT
******************************************************************************/
package mq

import (
	"errors"
	"fmt"
	"sync"
	"time"

	"apworkshop/internal/configs"
	"apworkshop/internal/loggers"

	"github.com/streadway/amqp"
)

var RabbitConn *amqp.Connection
var RabbitChannel *amqp.Channel
var RabbitInferenceNotifyQueue amqp.Queue
var isReconnecting = false
var initLock = &sync.Mutex{}
var logger = loggers.LogInstance()

func InitMq(config *configs.APWorkshopConfig) error {
	err := initConn(config)
	if err != nil {
		return err
	}

	RabbitInferenceNotifyQueue, err = RabbitChannel.QueueDeclare(
		config.RabbitMq.InferenceNotifyQueue,
		true,  // durable
		false, // delete when unused
		false, // exclusive
		false, // no-wait
		nil,   // arguments
	)
	if err != nil {
		return err
	}

	return nil
}

func initConn(config *configs.APWorkshopConfig) error {
	var err error
	url := fmt.Sprintf("amqp://%s:%s@%s:%d/", config.RabbitMq.Username, config.RabbitMq.Password, config.RabbitMq.Host, config.RabbitMq.Port)
	RabbitConn, err = amqp.Dial(url)
	if err != nil {
		return err
	}

	RabbitChannel, err = RabbitConn.Channel()
	return err
}

func TryReConn(tryTimes int) error {
	var err error
	if isReconnecting {
		time.Sleep(time.Second * 1)
		return errors.New("Reconnecting")
	}
	initLock.Lock()
	isReconnecting = true
	for i := 0; i < tryTimes; i++ {
		err = initConn(configs.Config)
		if err != nil {
			logger.Warn("Trying to connect %d times", i)
			time.Sleep(time.Second * 3)
		} else {
			logger.Info("Reconnect success")
			break
		}
	}
	isReconnecting = false
	initLock.Unlock()
	return err
}
