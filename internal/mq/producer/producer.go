/* ******************************************************************************
* 2019 - present Contributed by Apulis Technology (Shenzhen) Co. LTD
*
* This program and the accompanying materials are made available under the
* terms of the MIT License, which is available at
* https://www.opensource.org/licenses/MIT
*
* See the NOTICE file distributed with this work for additional
* information regarding copyright ownership.
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations
* under the License.
*
* SPDX-License-Identifier: MIT
******************************************************************************/
package producer

import (
	"apworkshop/internal/loggers"
	"apworkshop/internal/mq"

	"github.com/streadway/amqp"
)

// var rabbitInferenceNotifyQueue = mq.RabbitInferenceNotifyQueue
var logger = loggers.LogInstance()

func PublishInference(msg []byte, queueName string) error {
	return publishMsg(msg, queueName)
}

func publishMsg(msg []byte, queue string) error {
	err := mq.RabbitChannel.Publish(
		"",
		queue,
		false, // mandatory
		false,
		amqp.Publishing{
			DeliveryMode: amqp.Persistent,
			ContentType:  "text/plain",
			Body:         msg,
		},
	)
	if err != nil {
		logger.Errorln(err)
		err = mq.TryReConn(5)
		if err == nil {
			return publishMsg(msg, queue)
		}
		logger.Errorf("Reconnect error: %s", err.Error())
	}
	return err
}
