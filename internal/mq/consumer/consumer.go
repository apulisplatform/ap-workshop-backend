/* ******************************************************************************
* 2019 - present Contributed by Apulis Technology (Shenzhen) Co. LTD
*
* This program and the accompanying materials are made available under the
* terms of the MIT License, which is available at
* https://www.opensource.org/licenses/MIT
*
* See the NOTICE file distributed with this work for additional
* information regarding copyright ownership.
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations
* under the License.
*
* SPDX-License-Identifier: MIT
******************************************************************************/
package consumer

// import (
// 	"apworkshop/internal/loggers"
// 	"apworkshop/internal/mq"
// 	"apworkshop/internal/proto"
// 	"context"
// 	"encoding/json"
// 	"fmt"

// 	"github.com/streadway/amqp"
// )

// var logger = loggers.LogInstance()

// func Consume() {
// 	go listenConn()
// 	select {}
// }

// func listenConn() {
// 	for {
// 		_, cancel := startConsume()
// 		<-mq.RabbitConn.NotifyClose(make(chan *amqp.Error))
// 		cancel()
// 		err := mq.TryReConn(5)
// 		if err != nil {
// 			logger.Errorf("Reconnect failed: %s", err.Error())
// 		}
// 		startConsume()
// 	}
// }

// func startConsume() (context.Context, context.CancelFunc) {
// 	ctx, cancel := context.WithCancel(context.Background())
// 	go consume(mq.RabbitInferenceNotifyQueue.Name, mq.RabbitChannel, ctx)
// 	return ctx, cancel
// }

// func consume(queueName string, ch *amqp.Channel, ctx context.Context) {
// 	var fn func([]byte) (string, error)
// 	if queueName == mq.RabbitInferenceNotifyQueue.Name {
// 		fn = handleMsgBodyForInferenceNotification
// 	}
// 	for {
// 		select {
// 		case <-ctx.Done():
// 			logger.Warn("Stop current consumer ...")
// 			return
// 		default:
// 			msgs, err := ch.Consume(
// 				queueName,
// 				"",
// 				false,
// 				false,
// 				false,
// 				false,
// 				nil,
// 			)
// 			if err != nil {
// 				logger.Error(err, "Failed to register consumer")
// 			}
// 			for d := range msgs {
// 				imageName, err := fn(d.Body)
// 				if err != nil {
// 					logger.Error(fmt.Sprintf("Consume image<%s> error: %s", imageName, err.Error()))
// 				} else {
// 					logger.Info(fmt.Sprintf("Consume image<%s> success.", imageName))
// 				}
// 				d.Ack(false)
// 			}
// 		}
// 	}
// }

// func handleMsgBodyForInferenceNotification(body []byte) (string, error) {
// 	logger.Info("consuming uninference data")
// 	TransformRsp := proto.StudioModelTransformRsp{}
// 	err := json.Unmarshal(body, &TransformRsp)
// 	fmt.Println(TransformRsp)
// 	if err != nil {
// 		return "", err
// 	}
// 	// return uninferenceRawData.ImageName, buildUninferenceRawImage(uninferenceRawData)
// 	return "", nil
// }
