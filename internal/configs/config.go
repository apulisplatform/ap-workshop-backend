/* ******************************************************************************
* 2019 - present Contributed by Apulis Technology (Shenzhen) Co. LTD
*
* This program and the accompanying materials are made available under the
* terms of the MIT License, which is available at
* https://www.opensource.org/licenses/MIT
*
* See the NOTICE file distributed with this work for additional
* information regarding copyright ownership.
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations
* under the License.
*
* SPDX-License-Identifier: MIT
******************************************************************************/
package configs

import (
	"fmt"

	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

type APWorkshopConfig struct {
	DebugMode    bool
	Http         HttpConfig
	Log          LogConfig
	Database     DbConfig
	Storage      StorageConfig
	Time         TimeConfig
	Endpoint     EndpointConfig
	Harbor       HarborConfig
	AiLab        AiLabConfig
	FileServer   FileServerConfig
	JobScheduler JobSchedulerConfig
	Job          JobConfig
	JobImages    JobImageConfig
	Paths        PathConfig
	RabbitMq     RabbitMqConfig
	Jwt          JwtConfig
}

type HttpConfig struct {
	APIPrefix string
	Address   string
	Port      int
}

type LogConfig struct {
	Level     logrus.Level
	WriteFile bool
	FileDir   string
	FileName  string
}

type DbConfig struct {
	Type         string
	Username     string
	Password     string
	Host         string
	Port         int
	Database     string
	MaxOpenConns int
	MaxIdleConns int
	Sslmode      string
}

type TimeConfig struct {
	TimeZoneStr string
}

type EndpointConfig struct {
	Scheme    string
	Host      string
	Port      string
	UrlPrefix string
}

type HarborConfig struct {
	Scheme    string
	Host      string
	Port      string
	UrlPrefix string
}

type AiLabConfig struct {
	Scheme         string
	Host           string
	Port           string
	RefUrlPrefix   string
	UnrefUrlPrefix string
}

type FileServerConfig struct {
	Scheme          string
	Host            string
	Port            string
	InfoUrlPrefix   string
	UploadUrlPrefix string
}

type JobSchedulerConfig struct {
	Scheme    string
	Host      string
	Port      int
	UrlPrefix string
	RabbitMQ  RabbitMQ
}

type RabbitMQ struct {
	User         string
	Password     string
	Host         string
	Port         string
	ExchangeName string
	Topic        string
}

type JobConfig struct {
	IsDev                  bool
	IsHostpath             bool
	ModId                  int
	JobNamespace           string
	ArchType               string
	ConfigMapName          string
	ConfigMapSubpath       string
	ConfigMapContainerPath string
	ModelDataPvcName       string
	AppDataPvcName         string
	// ConfigMountPath        HostPathConfig
	// JobMountPoints         JobMountConfig
}

// type JobMountConfig struct {
// 	HostPaths []HostPathConfig
// }

// // TODO Name may not be needed anymore
// type HostPathConfig struct {
// 	Name      string
// 	Path      string
// 	MountPath string
// }

type JobImageConfig struct {
	ModelTransformer string
}

type PathConfig struct {
	ModelPvcPath string
	AppPvcPath   string
}

type RabbitMqConfig struct {
	Host                 string
	Port                 int
	Username             string
	Password             string
	Vhost                string
	Queue                string
	InferenceNotifyQueue string
}

type JwtConfig struct {
	Debug         bool
	SignAlgorithm string
	SecretKey     string
	PublicKey     string
}

var Config *APWorkshopConfig

func InitConfig(configPath string, config *APWorkshopConfig) error {
	viper.SetConfigFile(configPath)
	err := viper.ReadInConfig()
	if err != nil {
		panic(fmt.Errorf("Fatal error read config file: %s \n", err))
	}

	if err := viper.Unmarshal(&config); err != nil {
		panic(fmt.Errorf("Fatal error unmarshal config file: %s \n", err))
	}
	Config = config
	return nil
}
