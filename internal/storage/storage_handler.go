/* ******************************************************************************
* 2019 - present Contributed by Apulis Technology (Shenzhen) Co. LTD
*
* This program and the accompanying materials are made available under the
* terms of the MIT License, which is available at
* https://www.opensource.org/licenses/MIT
*
* See the NOTICE file distributed with this work for additional
* information regarding copyright ownership.
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations
* under the License.
*
* SPDX-License-Identifier: MIT
******************************************************************************/
package storage

import (
	"apworkshop/internal/configs"
	"apworkshop/internal/loggers"
)

var logger = loggers.LogInstance()
var Handler StorageHandler

const (
	STORAGE_FILE_TYPE = "file"
	STORAGE_PVC_TYPE  = "pvc"
)

func InitStorage(config *configs.StorageConfig) {
	storageType := config.Type

	logger.Infoln("Storage type  is set as: " + storageType)

	if storageType == STORAGE_FILE_TYPE {
		Handler = &FileHandler{}
	} else if storageType == STORAGE_PVC_TYPE {
		storageConfig := config.PVC
		var mappings []MappingsStruct
		for _, mapping := range storageConfig.Mappings {
			mappings = append(mappings, MappingsStruct(mapping))
		}
		Handler = &PVCHandler{
			UploadPVCName: config.PVC.UploadPVCName,
			Mappings:      mappings,
		}
	} else {
		logger.Panicln("Storage type " + config.Type + " not support")
	}
}

type StorageHandler interface {
	GetUploadPath(modelId, modelVersionId int, dir string) (string, error)
	/*
	 * @Desc  get model upload path;
	 * @Param modelId           model's id
	 * @Param modelVersionId          model version's id
	 */
	GetUploadTmpPath(modelId, modelVersionId int, userName string) (string, error)
	GetTranslatePath(originPath string) (string, error)
}
