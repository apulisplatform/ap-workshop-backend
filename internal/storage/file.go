/* ******************************************************************************
* 2019 - present Contributed by Apulis Technology (Shenzhen) Co. LTD
*
* This program and the accompanying materials are made available under the
* terms of the MIT License, which is available at
* https://www.opensource.org/licenses/MIT
*
* See the NOTICE file distributed with this work for additional
* information regarding copyright ownership.
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations
* under the License.
*
* SPDX-License-Identifier: MIT
******************************************************************************/
package storage

import "fmt"

const (
	FILE_REMOTE_PREFIX = "FILE://"
)

type FileHandler struct{}

func (f *FileHandler) GetUploadPath(modelId, modelVersionId int, dir string) (string, error) {
	uploadPath := fmt.Sprintf("%s%s/models/%d/%d/", FILE_REMOTE_PREFIX, dir, modelId, modelVersionId)

	return uploadPath, nil
}

func (f *FileHandler) GetTranslatePath(dir string) (string, error) {
	return dir, nil
}

func (f *FileHandler) GetUploadTmpPath(modelId, modelVersionId int, userName string) (string, error) {
	return "", nil
}
