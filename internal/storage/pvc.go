/* ******************************************************************************
* 2019 - present Contributed by Apulis Technology (Shenzhen) Co. LTD
*
* This program and the accompanying materials are made available under the
* terms of the MIT License, which is available at
* https://www.opensource.org/licenses/MIT
*
* See the NOTICE file distributed with this work for additional
* information regarding copyright ownership.
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations
* under the License.
*
* SPDX-License-Identifier: MIT
******************************************************************************/
package storage

import (
	"errors"
	"fmt"
	"regexp"
	"strings"
)

const (
	PVC_REMOTE_PREFIX = "pvc://"
	TMP_DIRECTORY     = "tmp"
)

type PVCHandler struct {
	UploadPVCName string
	Mappings      []MappingsStruct
}

type MappingsStruct struct {
	PVCName     string
	MappingPath string
}

func (p *PVCHandler) GetUploadPath(modelId, modelVersionId int, dir string) (string, error) {
	uploadPath := fmt.Sprintf("%s%s/models/%d/%d", PVC_REMOTE_PREFIX, p.UploadPVCName, modelId, modelVersionId)
	return uploadPath, nil
}

func (p *PVCHandler) GetUploadTmpPath(modelId, modelVersionId int, userName string) (string, error) {
	uploadPath := fmt.Sprintf("%s%s/%s/%s", PVC_REMOTE_PREFIX, p.UploadPVCName, TMP_DIRECTORY, userName)
	return uploadPath, nil
}

func (p *PVCHandler) GetTranslatePath(target string) (string, error) {
	target = strings.Replace(target, "pvc://", "", -1)
	logger.Debugln(target)
	target = strings.Replace(target, "PVC://", "", -1)
	logger.Debugln(target)

	match := false
	var err error
	for _, mapping := range p.Mappings {
		match, err = regexp.MatchString("^"+mapping.PVCName+".*", target)
		if err != nil {
			return "", err
		}
		if match {
			target = strings.Replace(target, mapping.PVCName, mapping.MappingPath, 1)
			logger.Debugln(target)
			break
		}

	}
	if !match {
		return "", errors.New("no pvc match")
	}
	return target, nil
}
