/* ******************************************************************************
* 2019 - present Contributed by Apulis Technology (Shenzhen) Co. LTD
*
* This program and the accompanying materials are made available under the
* terms of the MIT License, which is available at
* https://www.opensource.org/licenses/MIT
*
* See the NOTICE file distributed with this work for additional
* information regarding copyright ownership.
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations
* under the License.
*
* SPDX-License-Identifier: MIT
******************************************************************************/
package middlewares

import (
	"apworkshop/internal/configs"
	"fmt"

	"github.com/apulis/go-business/pkg/jwt"
	"github.com/gin-gonic/gin"
)

func AuthMiddleware() gin.HandlerFunc {
	return jwt.NewJwtAuthN(
		jwt.SigningAlgorithm(configs.Config.Jwt.SignAlgorithm),
		jwt.SecretKey([]byte(configs.Config.Jwt.SecretKey)),
		jwt.PublicKey(configs.Config.Jwt.PublicKey),
	).Middleware()
}

func FakeAuthMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Set("userId", 666)
		c.Set("userName", "fakeUser")
		c.Set("userGroup", 66)
		c.Set("groupAccount", "fakeGroup")
		c.Set("orgId", 6)
		c.Set("orgName", "fakeOrg")
		c.Next()
	}
}

func Mock() gin.HandlerFunc {
	return func(c *gin.Context) {
		for _, h := range c.Request.Header {
			fmt.Println(h)
		}

	}
}

func AutoAuthMiddleware() gin.HandlerFunc {
	if configs.Config.Jwt.Debug {
		return FakeAuthMiddleware()
	}
	return AuthMiddleware()
}
