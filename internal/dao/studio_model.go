/* ******************************************************************************
* 2019 - present Contributed by Apulis Technology (Shenzhen) Co. LTD
*
* This program and the accompanying materials are made available under the
* terms of the MIT License, which is available at
* https://www.opensource.org/licenses/MIT
*
* See the NOTICE file distributed with this work for additional
* information regarding copyright ownership.
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations
* under the License.
*
* SPDX-License-Identifier: MIT
******************************************************************************/
package dao

import (
	"apworkshop/internal/databases"
	"apworkshop/internal/entity"
	"apworkshop/internal/loggers"
	"apworkshop/internal/utils"
	"strings"
)

const (
	STUDIO_MODEL_FIELD       = "field"
	STUDIO_MODEL_TASK        = "task"
	STUDIO_MODEL_DEVICE_TYPE = "deviceType"
	STUDIO_MODEL_SOURCE      = "source"
	STUDIO_MODEL_FRAMEWORK   = "framework"
	// model capability is related to ability? to be verify
	STUDIO_MODEL_CAPABILITY = "capability"
)

var logger = loggers.LogInstance()

func CreateStudioModel(model *entity.StudioModel, isPrelod bool) (*entity.StudioModel, error) {
	model.IsPreload = isPrelod
	model.Status = entity.MODEL_PREPARE_STATUS // when model is first created, we think no version is commited, so it is not ready
	return model, databases.Db.Create(model).Error
}

func GetStudioModelByNameAndScope(name, scope string) (*entity.StudioModel, error) {
	var count int64
	err := databases.Db.
		Model(&entity.StudioModel{}).
		Where("name = ?", name).
		Where("scope = ?", scope).
		Count(&count).
		Error
	if err != nil {
		return nil, err
	}
	if count == 0 {
		return nil, nil
	}
	var model entity.StudioModel
	err = databases.Db.
		Where("name = ?", name).
		Where("scope = ?", scope).
		First(&model).
		Error
	if err != nil {
		return nil, err
	}

	return &model, nil
}

func GetStudioModelByName(name string) (*entity.StudioModel, error) {
	var count int64
	err := databases.Db.
		Model(&entity.StudioModel{}).
		Where("name = ?", name).
		Count(&count).
		Error
	if err != nil {
		return nil, err
	}
	if count == 0 {
		return nil, nil
	}
	var model entity.StudioModel
	err = databases.Db.
		Where("name = ?", name).
		First(&model).
		Error
	if err != nil {
		return nil, err
	}

	return &model, nil
}

func GetStudioModelById(modelId int) (*entity.StudioModel, error) {
	var model entity.StudioModel
	err := databases.Db.
		Where("id = ?", modelId).
		First(&model).
		Error
	if err != nil {
		return nil, err
	}

	return &model, nil
}

func GetStudioModelList(pageNum, pageSize int,
	scope, name, sortArgs, source, framework, deviceType, task, field string,
	canEdgeInfer, canCenterInfer, canTrain, isPrelod bool,
	startTime, endTime int, userGroup string) ([]entity.StudioModel, int, error) {

	var result []entity.StudioModel
	var total int64

	tempRes := databases.Db
	tempRes1 := databases.Db

	if scope != "" {
		tempRes = tempRes.Where("scope = ?", scope)
		tempRes1 = tempRes1.Where("scope = ?", scope)
	}

	// only show ready
	tempRes = tempRes.Where("status = ?", entity.MODEL_READY_STATUS)
	tempRes1 = tempRes1.Where("status = ?", entity.MODEL_READY_STATUS)

	// check name
	if name != "" {
		tempRes = tempRes.Where("name like ?", "%"+name+"%")
		tempRes1 = tempRes1.Where("name like ?", "%"+name+"%")
	}

	if source != "" {
		tempRes = tempRes.Where("source = ?", source)
		tempRes1 = tempRes1.Where("source = ?", source)
	}

	if framework != "" {
		tempRes = tempRes.Where("framework = ?", framework)
		tempRes1 = tempRes1.Where("framework = ?", framework)
	}

	if deviceType != "" {
		tempRes = tempRes.Where("device_type = ?", deviceType)
		tempRes1 = tempRes1.Where("device_type = ?", deviceType)
	}

	if task != "" {
		tempRes = tempRes.Where("task = ?", task)
		tempRes1 = tempRes1.Where("task = ?", task)
	}

	if field != "" {
		tempRes = tempRes.Where("field = ?", field)
		tempRes1 = tempRes1.Where("field = ?", field)
	}

	if canCenterInfer || canEdgeInfer || canTrain {
		if canCenterInfer {
			tempRes = tempRes.Where("can_center_infer = ?", canCenterInfer)
			tempRes1 = tempRes1.Where("can_center_infer = ?", canCenterInfer)
		} else if canEdgeInfer {
			tempRes = tempRes.Where("can_edge_infer = ?", canEdgeInfer)
			tempRes1 = tempRes1.Where("can_edge_infer = ?", canEdgeInfer)
		} else if canTrain {
			tempRes = tempRes.Where("can_train = ?", canTrain)
			tempRes1 = tempRes1.Where("can_train = ?", canTrain)
		}
	}

	// if isPrelod {
	// 	tempRes = tempRes.Where("is_preload = ?", isPrelod)
	// 	tempRes1 = tempRes1.Where("is_preload = ?", isPrelod)
	// }

	if startTime != 0 && endTime != 0 {
		tempRes = tempRes.Where("created_at BETWEEN ? AND ?", utils.IntTimeToUnixTime(startTime), utils.IntTimeToUnixTime(endTime))
		tempRes1 = tempRes1.Where("created_at BETWEEN ? AND ?", utils.IntTimeToUnixTime(startTime), utils.IntTimeToUnixTime(endTime))
	}

	tempRes = tempRes.Where(databases.Db.Where("source = ?", "preset").Or("user_group = ?", userGroup))
	tempRes1 = tempRes1.Where(databases.Db.Where("source = ?", "preset").Or("user_group = ?", userGroup))

	err := tempRes1.Model(&entity.StudioModel{}).Count(&total).Error
	if err != nil {
		return nil, int(total), err
	}

	// check order
	if sortArgs != "" {
		argsArray := strings.Split(sortArgs, ",")
		for _, args := range argsArray {
			sortNameAndOrder := strings.Split(args, "|")
			sortName := sortNameAndOrder[0]
			sortOrder := sortNameAndOrder[1]

			if sortName == "createdAt" {
				sortName = "created_at"
			}

			if sortName == "updatedAt" {
				sortName = "updated_at"
			}

			if sortOrder != "desc" && sortOrder != "asc" {
				sortOrder = "desc"
			}

			tempRes = tempRes.
				Order(sortName + " " + sortOrder)
		}
	} else {
		tempRes = tempRes.
			Order("created_at" + " " + "desc")
	}

	if pageSize != 0 {
		offset := pageSize * (pageNum - 1)
		limit := pageSize
		tempRes = tempRes.Offset(offset).Limit(limit)
	}

	err = tempRes.Find(&result).Error

	if err != nil {
		return nil, int(total), err
	}

	return result, int(total), nil
}

func UpdateStudioModel(model *entity.StudioModel) (*entity.StudioModel, error) {
	err := databases.Db.
		Updates(model).
		Error
	if err != nil {
		return nil, err
	}
	return model, nil
}

func DeleteStudioModelById(id int) error {
	err := databases.Db.
		Where("id = ?", id).
		Delete(&entity.StudioModel{}).
		Error
	if err != nil {
		return err
	}

	return nil
}

func GetStudioModelOverview() (int, int, int, error) {
	var presetCount int64
	var publishCount int64
	var userDefineCount int64

	err := databases.Db.
		Model(&entity.StudioModel{}).
		Where("source = ?", "preset").
		Count(&presetCount).
		Error
	if err != nil {
		return 0, 0, 0, err
	}

	err = databases.Db.
		Model(&entity.StudioModel{}).
		Where("source = ?", "publish").
		Count(&publishCount).
		Error
	if err != nil {
		return 0, 0, 0, err
	}

	err = databases.Db.
		Model(&entity.StudioModel{}).
		Where("source = ?", "userDefine").
		Count(&userDefineCount).
		Error
	if err != nil {
		return 0, 0, 0, err
	}

	return int(presetCount), int(publishCount), int(userDefineCount), nil
}

func GetStudioModelDeviceType() ([]string, error) {
	var cpu int64
	var huawei_npu int64
	var nvidia_gpu int64
	var output = []string{}

	err := databases.Db.
		Model(&entity.StudioModel{}).
		Where("device_type_cpu = ?", true).
		Count(&cpu).
		Error
	if err != nil {
		return output, err
	}

	err = databases.Db.
		Model(&entity.StudioModel{}).
		Where("device_type_nvidia_gpu = ?", true).
		Count(&nvidia_gpu).
		Error
	if err != nil {
		return output, err
	}

	err = databases.Db.
		Model(&entity.StudioModel{}).
		Where("device_type_huawei_npu = ?", true).
		Count(&huawei_npu).
		Error
	if err != nil {
		return output, err
	}

	if cpu != 0 {
		output = append(output, "cpu")
	}
	if huawei_npu != 0 {
		output = append(output, "huawei_npu")
	}
	if nvidia_gpu != 0 {
		output = append(output, "nvidia_gpu")
	}
	return output, nil
}

func GetStudioModelRelatedDeviceType(field string) ([]string, error) {
	var cpu int64
	var huawei_npu int64
	var nvidia_gpu int64
	var output = []string{}

	err := databases.Db.
		Model(&entity.StudioModel{}).
		Where("device_type_cpu = ?", true).
		Where("field = ?", field).
		Count(&cpu).
		Error
	if err != nil {
		return output, err
	}

	err = databases.Db.
		Model(&entity.StudioModel{}).
		Where("device_type_nvidia_gpu = ?", true).
		Where("field = ?", field).
		Count(&nvidia_gpu).
		Error
	if err != nil {
		return output, err
	}

	err = databases.Db.
		Model(&entity.StudioModel{}).
		Where("device_type_huawei_npu = ?", true).
		Where("field = ?", field).
		Count(&huawei_npu).
		Error
	if err != nil {
		return output, err
	}

	if cpu != 0 {
		output = append(output, "cpu")
	}
	if huawei_npu != 0 {
		output = append(output, "huawei_npu")
	}
	if nvidia_gpu != 0 {
		output = append(output, "nvidia_gpu")
	}
	return output, nil
}

func GetStudioModelVarietyTypes(currType string) ([]string, error) {
	var typeList []string
	var modelList []entity.StudioModel
	var err error
	set := map[string]bool{}

	err = databases.Db.
		Model(&entity.StudioModel{}).
		Where(currType+" != ?", "").
		Select(currType).
		Distinct(currType).
		Find(&modelList).
		Error

	if err != nil {
		return []string{}, err
	}

	// TODO get device types from device chart
	for _, model := range modelList {
		var modelType string
		if currType == "source" {
			modelType = model.Source
		} else if currType == "framework" {
			modelType = model.Framework
		} else if currType == "engine" {
			modelType = model.Engine
		} else if currType == "task" {
			modelType = model.Task
			// } else if currType == "device_type" {
			// 	modelType = model.DeviceType
		} else if currType == "ability" {
			for _, types := range model.Ability {
				_, ok := set[types]
				if !ok {
					set[types] = true
				}
			}
		} else if currType == "field" {
			modelType = model.Field
		}
		typeList = append(typeList, modelType)
	}

	if currType == "ability" {
		typeList = []string{}
		for key := range set {
			typeList = append(typeList, key)
		}
	}

	return typeList, nil
}

func GetStudioModelFieldTypes() ([]string, error) {
	var err error
	var typeList []string
	var modelList []entity.StudioModel

	err = databases.Db.
		Model(&entity.StudioModel{}).
		Where("field != ?", "").
		Select("field").
		Distinct("field").
		Find(&modelList).
		Error

	if err != nil {
		return []string{}, err
	}

	for _, model := range modelList {
		typeList = append(typeList, model.Field)
	}

	return typeList, nil
}

func GetStudioModelOtherTypes(field string) (map[string][]string, error) {
	var err error
	var modelList []entity.StudioModel
	typeMap := map[string][]string{}
	types := []string{"source", "framework", "ability", "task", "engine"}
	for _, currType := range types {
		if field != "" {
			err = databases.Db.
				Model(&entity.StudioModel{}).
				Where(currType+" != ?", "").
				Where("field = ?", field).
				Select(currType).
				Distinct(currType).
				Find(&modelList).
				Error
		} else {
			err = databases.Db.
				Model(&entity.StudioModel{}).
				Where(currType+" != ?", "").
				Select(currType).
				Distinct(currType).
				Find(&modelList).
				Error
		}

		if err != nil {
			return typeMap, err
		}

		set := map[string]bool{}
		for _, model := range modelList {

			if currType == "source" {
				typeMap[currType] = append(typeMap[currType], model.Source)
			} else if currType == "framework" {
				typeMap[currType] = append(typeMap[currType], model.Framework)
			} else if currType == "ability" {
				for _, types := range model.Ability {
					_, ok := set[types]
					if !ok {
						set[types] = true
					}
				}
			} else if currType == "engine" {
				typeMap[currType] = append(typeMap[currType], model.Engine)
			} else if currType == "task" {
				typeMap[currType] = append(typeMap[currType], model.Task)
			}
		}

		if currType == "ability" {
			for key := range set {
				typeMap[currType] = append(typeMap[currType], key)
			}
		}
	}

	return typeMap, nil
}
