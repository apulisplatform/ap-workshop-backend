/* ******************************************************************************
* 2019 - present Contributed by Apulis Technology (Shenzhen) Co. LTD
*
* This program and the accompanying materials are made available under the
* terms of the MIT License, which is available at
* https://www.opensource.org/licenses/MIT
*
* See the NOTICE file distributed with this work for additional
* information regarding copyright ownership.
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations
* under the License.
*
* SPDX-License-Identifier: MIT
******************************************************************************/
package dao

import (
	"apworkshop/internal/databases"
	"apworkshop/internal/entity"

	"gorm.io/gorm/clause"
)

func CreateJob(modelTransformJob entity.Job) (entity.Job, error) {
	res := databases.Db.Create(&modelTransformJob)
	return modelTransformJob, res.Error
}

func GetJobById(id string) (entity.Job, error) {
	job := entity.Job{}
	res := databases.Db.Preload(clause.Associations).Where("job_id = ?", id).First(&job)
	return job, res.Error
}

func GetJobList() ([]entity.Job, error) {
	jobList := []entity.Job{}
	res := databases.Db.Preload(clause.Associations).Find(&jobList)
	return jobList, res.Error
}

func UpdateJob(job entity.Job) (entity.Job, error) {
	res := databases.Db.Save(&job)
	return job, res.Error
}
