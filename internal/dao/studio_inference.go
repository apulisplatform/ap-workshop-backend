/* ******************************************************************************
* 2019 - present Contributed by Apulis Technology (Shenzhen) Co. LTD
*
* This program and the accompanying materials are made available under the
* terms of the MIT License, which is available at
* https://www.opensource.org/licenses/MIT
*
* See the NOTICE file distributed with this work for additional
* information regarding copyright ownership.
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations
* under the License.
*
* SPDX-License-Identifier: MIT
******************************************************************************/
package dao

import (
	"apworkshop/internal/common"
	"apworkshop/internal/databases"
	"apworkshop/internal/entity"
	"encoding/json"
	"strings"
)

func CreateStudioInference(modelId, modelVersionId int, inference *entity.StudioInference) error {
	// create inference
	inferDb := inference.StudioInferenceModel
	err := databases.Db.Create(&inferDb).Error
	if err != nil {
		return err
	}
	// create inference node
	if inference.Center != nil {
		centerNode := &entity.StudioInferenceNode{
			InferID:      inferDb.ID,
			NodeType:     common.INFER_CENTER_NODE_TYPE,
			Framework:    inference.Center.Framework,
			ModelPath:    inference.Center.ModelPath,
			InputParams:  inference.Center.InputParams,
			OutputParams: inference.Center.OutputParams,
			Format:       inference.Center.Format,
			VerifyScript: inference.Center.VerifyScript,
			Engine:       inference.Center.Engine,
			Devices:      inference.Center.Devices,
		}
		err = CreateStudioInferNode(centerNode)
		if err != nil {
			return err
		}
	}
	if inference.Edge != nil {
		edgeNode := &entity.StudioInferenceNode{
			InferID:      inferDb.ID,
			NodeType:     common.INFER_EDGE_NODE_TYPE,
			Framework:    inference.Edge.Framework,
			ModelPath:    inference.Edge.ModelPath,
			InputParams:  inference.Edge.InputParams,
			OutputParams: inference.Edge.OutputParams,
			Format:       inference.Edge.Format,
			Engine:       inference.Edge.Engine,
			Devices:      inference.Edge.Devices,
		}
		err = CreateStudioInferNode(edgeNode)
		if err != nil {
			return err
		}
	}

	// update model infer info
	model, err := GetStudioModelById(modelId)
	if err != nil {
		return err
	}
	// create model ability info
	arrayString, err := json.Marshal(model.Ability)
	if err != nil {
		return err
	}
	if !strings.Contains(string(arrayString), entity.STUDIO_MODEL_ABILITY_CENTER_INFER) {
		model.Ability = append(model.Ability, entity.STUDIO_MODEL_ABILITY_CENTER_INFER)
		model.CanCenterInfer = true
	}
	if inference.Edge != nil {
		if !strings.Contains(string(arrayString), entity.STUDIO_MODEL_ABILITY_EDGE_INFER) {
			model.Ability = append(model.Ability, entity.STUDIO_MODEL_ABILITY_EDGE_INFER)
			model.CanEdgeInfer = true
		}
	}
	UpdateStudioModel(model)

	return nil
}

func GetStudioInferenceByVersionlId(modelId int) (*entity.StudioInference, error) {
	var count int64
	var infer entity.StudioInference
	var inferDb entity.StudioInferenceModel
	var inferEdgeNode entity.StudioInferenceNode
	var inferCenterNode entity.StudioInferenceNode

	// make sure model has inference info
	err := databases.Db.
		Model(&entity.StudioInferenceModel{}).
		Where("model_version_id = ?", modelId).
		Count(&count).
		Error
	if err != nil {
		return nil, err
	}
	if count == 0 {
		return nil, nil
	}
	err = databases.Db.
		Where("model_version_id = ?", modelId).
		First(&inferDb).
		Error
	if err != nil {
		return nil, err
	}
	infer.StudioInferenceModel = inferDb

	// check if has center inference
	err = databases.Db.
		Model(&entity.StudioInferenceNode{}).
		Where("infer_id = ?", infer.ID).
		Where("node_type = ?", common.INFER_CENTER_NODE_TYPE).
		Count(&count).
		Error
	if err != nil {
		return nil, err
	}
	if count != 0 {
		err = databases.Db.
			Where("infer_id = ?", infer.ID).
			Where("node_type = ?", common.INFER_CENTER_NODE_TYPE).
			First(&inferCenterNode).
			Error
		if err != nil {
			return nil, err
		}
		infer.Center = &inferCenterNode
	}

	// check if has edge inference
	err = databases.Db.
		Model(&entity.StudioInferenceNode{}).
		Where("infer_id = ?", infer.ID).
		Where("node_type = ?", common.INFER_EDGE_NODE_TYPE).
		Count(&count).
		Error
	if err != nil {
		return nil, err
	}
	if count != 0 {
		err = databases.Db.
			Where("infer_id = ?", infer.ID).
			Where("node_type = ?", common.INFER_EDGE_NODE_TYPE).
			First(&inferEdgeNode).
			Error
		if err != nil {
			return nil, err
		}
		infer.Edge = &inferEdgeNode
	}

	return &infer, nil
}
