/* ******************************************************************************
* 2019 - present Contributed by Apulis Technology (Shenzhen) Co. LTD
*
* This program and the accompanying materials are made available under the
* terms of the MIT License, which is available at
* https://www.opensource.org/licenses/MIT
*
* See the NOTICE file distributed with this work for additional
* information regarding copyright ownership.
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations
* under the License.
*
* SPDX-License-Identifier: MIT
******************************************************************************/
package dao

import (
	"apworkshop/internal/appErr"
	"apworkshop/internal/common"
	"apworkshop/internal/databases"
	"apworkshop/internal/entity"
	"errors"
	"fmt"
	"strconv"
	"strings"

	"github.com/gofrs/uuid"
	"gorm.io/gorm"
)

func CreateNewVersionForStudioModel(ctx string, modelId int) (*entity.StudioModelVersion, error) {
	var count int64
	err := databases.Db.
		Model(&entity.StudioModelVersion{}).
		Where("register_ctx = ?", ctx).
		Count(&count).
		Error
	if err != nil {
		return nil, err
	}
	if count != 0 {
		var oldVersion entity.StudioModelVersion
		err := databases.Db.
			Where("register_ctx = ?", ctx).
			First(&oldVersion).
			Error
		if err != nil {
			return nil, err
		}
		return &oldVersion, nil
	}
	err = databases.Db.
		Unscoped().
		Model(&entity.StudioModelVersion{}).
		Where("model_id = ?", modelId).
		Count(&count).
		Error
	if err != nil {
		return nil, err
	}

	u1, err := uuid.NewV4()
	if err != nil {
		return nil, err
	}
	// check if model has one version
	var modelVersion *entity.StudioModelVersion
	if count == 0 {
		modelVersion = &entity.StudioModelVersion{
			ModelID:     modelId,
			Name:        common.FIRST_MODEL_VERSION,
			Locker:      u1.String(),
			RegisterCtx: ctx,
			Status:      common.MODEL_VERSION_STATUS_PREPARE,
		}
		result, err := CreateStudioModelVersion(modelVersion)
		if err != nil {
			return nil, err
		}
		return result, nil
	}
	// get new version name
	newVersionName, err := GetStudioModelLatestVersionName(modelId)
	if err != nil {
		return nil, err
	}
	modelVersion = &entity.StudioModelVersion{
		ModelID:     modelId,
		Name:        newVersionName,
		Locker:      u1.String(),
		RegisterCtx: ctx,
		Status:      common.MODEL_VERSION_STATUS_PREPARE,
	}
	result, err := CreateStudioModelVersion(modelVersion)
	if err != nil {
		return nil, err
	}
	return result, nil

}

func CreateStudioModelVersion(version *entity.StudioModelVersion) (*entity.StudioModelVersion, error) {
	err := databases.Db.Create(version).Error
	if err != nil {
		return nil, err
	}

	return version, nil
}

func CommitStudioVersion(locker string) error {
	targetVersion, err := GetStudioModelVersionByLockerStatusPrepare(locker)
	if err != nil {
		return err
	}
	err = databases.Db.
		Model(&entity.StudioModelVersion{}).
		Where("locker = ?", locker).
		Update("status", common.MODEL_VERSION_STATUS_FREE).
		Error
	if err != nil {
		return err
	}

	err = databases.Db.
		Model(&entity.StudioModel{}).
		Where("id = ?", targetVersion.ModelID).
		Update("status", entity.MODEL_READY_STATUS).
		Error
	if err != nil {
		return err
	}

	return nil
}

func GetStudioModelVersion(modelVersionId int) (*entity.StudioModelVersion, error) {
	fmt.Println(modelVersionId)
	var modelVersion entity.StudioModelVersion
	err := databases.Db.
		Where("id = ?", modelVersionId).
		First(&modelVersion).
		Error
	if err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return nil, appErr.RECORD_NOT_FOUND_ERROR
		}
		return nil, err
	}

	return &modelVersion, nil
}

func GetStudioModelVersionListByModel(modelId, pageNum, pageSize int, sortArgs string) ([]entity.StudioModelVersion, error) {
	var list []entity.StudioModelVersion
	tempRes := databases.Db
	if pageSize != 0 {
		offset := pageSize * (pageNum - 1)
		limit := pageSize
		tempRes = tempRes.Offset(offset).Limit(limit)
	}

	// check order
	if sortArgs != "" {
		argsArray := strings.Split(sortArgs, ",")
		for _, args := range argsArray {
			sortNameAndOrder := strings.Split(args, "|")
			sortName := sortNameAndOrder[0]
			sortOrder := sortNameAndOrder[1]

			if sortName == "createdAt" {
				sortName = "created_at"
			}

			if sortName == "updatedAt" {
				sortName = "updated_at"
			}

			if sortName == "publishTime" {
				sortName = "publish_time"
			}

			if sortOrder != "desc" && sortOrder != "asc" {
				sortOrder = "desc"
			}

			tempRes = tempRes.
				Order(sortName + " " + sortOrder)
		}
	} else {
		tempRes = tempRes.
			Order("created_at" + " " + "desc")
	}

	err := tempRes.
		Where(databases.Db.Where("model_id = ?", modelId).Where("status = ?", common.MODEL_VERSION_STATUS_FREE)).
		Or(databases.Db.Where("model_id = ?", modelId).Where("status = ?", common.MODEL_VERSION_STATUS_LOCK)).
		Find(&list).
		Error
	if err != nil {
		return nil, err
	}

	return list, nil
}

func GetStudioModelVersionByLocker(locker string) (*entity.StudioModelVersion, error) {
	var version entity.StudioModelVersion

	err := databases.Db.
		Where("locker = ?", locker).
		First(&version).
		Error
	if err != nil {
		return nil, err
	}
	return &version, nil
}

func GetStudioModelVersionByLockerStatusPrepare(locker string) (*entity.StudioModelVersion, error) {
	var version entity.StudioModelVersion

	err := databases.Db.
		Where("locker = ?", locker).
		Where("status = ?", common.MODEL_VERSION_STATUS_PREPARE).
		First(&version).
		Error
	if err != nil {
		return nil, err
	}
	return &version, nil
}

func GetStudioModelVersionByLockerStatusLock(locker string) (*entity.StudioModelVersion, error) {
	var version entity.StudioModelVersion

	err := databases.Db.
		Where("locker = ?", locker).
		Where("status = ?", common.MODEL_VERSION_STATUS_LOCK).
		First(&version).
		Error
	if err != nil {
		return nil, err
	}
	return &version, nil
}

func UpdateStudioModelVersion(modelversion *entity.StudioModelVersion) (*entity.StudioModelVersion, error) {

	err := databases.Db.
		Where("id = ?", modelversion.ID).
		Updates(modelversion).
		Error
	if err != nil {
		return nil, err
	}

	return modelversion, nil
}

func GetStudioModelVersionCount(modelId int) (int, error) {
	var count int64
	err := databases.Db.
		Model(&entity.StudioModelVersion{}).
		Where("model_id = ?", modelId).
		Where(databases.Db.Where("status = ?", common.MODEL_VERSION_STATUS_FREE).Or("status = ?", common.MODEL_VERSION_STATUS_LOCK)).
		Count(&count).
		Error
	if err != nil {
		return 0, err
	}

	return int(count), nil
}

func DeleteStudioModelVersion(modelVersionId int) error {
	err := databases.Db.
		Where("id = ?", modelVersionId).
		Delete(&entity.StudioModelVersion{}).
		Error
	if err != nil {
		return err
	}

	return nil
}

func DeleteStudioVersionsByModelId(modelId int) error {
	err := databases.Db.
		Where("model_id = ?", modelId).
		Delete(&entity.StudioModelVersion{}).
		Error
	if err != nil {
		return err
	}

	return nil
}

// carefully delete one version because it might need to delete model when no version of i
// this action might cause some lock problems
// func DeleteVersionById(modelVersionId int) error {
// 	err := databases.Db.
// 		Where("id = ?", modelVersionId).
// 		Update("status", common.MODEL_VERSION_STATUS_DELETING).
// 		Error
// 	if err != nil {
// 		return err
// 	}

// 	return nil
// }

func RollbackStudioVersionById(modelVersionId int) error {
	err := databases.Db.
		Model(&entity.StudioModelVersion{}).
		Where("id = ?", modelVersionId).
		Where("status", common.MODEL_VERSION_STATUS_PREPARE).
		Update("status", common.MODEL_VERSION_STATUS_DELETING).
		Error
	if err != nil {
		return err
	}

	return nil
}

func GetStudioModelLatestVersionName(modelId int) (string, error) {
	var latestVersion entity.StudioModelVersion
	err := databases.Db.
		Unscoped().
		Where("model_id = ?", modelId).
		Order("created_at desc").
		First(&latestVersion).
		Error
	if err != nil {
		return "", err
	}

	latestName, err := GetStudioNextVersion(latestVersion.Name)
	if err != nil {
		return "", err
	}

	return latestName, nil
}

func GetStudioNextVersion(formerVersion string) (string, error) {
	var newVersionName string

	// dump "1.0" to "10"
	tempString := strings.Replace(formerVersion, ".", "", -1)
	// dump "10" to 10
	formerVersionInt, err := strconv.Atoi(tempString)
	if err != nil {
		return newVersionName, err
	}
	// update 10 to 11
	tempInt := formerVersionInt + 1
	// dump 11 to "11"
	tempString = strconv.Itoa(tempInt)
	// dump "11" to "1.1"
	length := len(tempString)
	newVersionName = tempString[:length-1] + "." + tempString[length-1:]

	return newVersionName, nil
}

func GetLatestStudioVersionByModelId(modelId int) (*entity.StudioModelVersion, error) {
	var versionCount int64
	err := databases.Db.
		Model(&entity.StudioModelVersion{}).
		Where("model_id = ?", modelId).
		Where("status = ?", common.MODEL_VERSION_STATUS_FREE).
		Count(&versionCount).
		Error
	if err != nil {
		return nil, err
	}
	if versionCount == 0 {
		return nil, nil
	}
	var latestVersion entity.StudioModelVersion
	err = databases.Db.
		Where("model_id = ?", modelId).
		Where("status = ?", common.MODEL_VERSION_STATUS_FREE).
		Order("created_at desc").
		First(&latestVersion).
		Error
	if err != nil {
		return nil, err
	}

	return &latestVersion, nil
}

func GetStudioVersionCommitCountByModelId(modelId int) (int, error) {
	var count int64

	err := databases.Db.
		Model(&entity.StudioModelVersion{}).
		Where("model_id = ?", modelId).
		Where("status = ?", common.MODEL_VERSION_STATUS_FREE).
		Count(&count).
		Error
	if err != nil {
		return int(count), err
	}

	return int(count), err
}

func GetStudioModelVersionById(modelVersionId int) (*entity.StudioModelVersion, error) {
	var version entity.StudioModelVersion

	err := databases.Db.
		Where("id = ?", modelVersionId).
		First(&version).
		Error
	if err != nil {
		return nil, err
	}
	return &version, nil
}
