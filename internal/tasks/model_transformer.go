/* ******************************************************************************
* 2019 - present Contributed by Apulis Technology (Shenzhen) Co. LTD
*
* This program and the accompanying materials are made available under the
* terms of the MIT License, which is available at
* https://www.opensource.org/licenses/MIT
*
* See the NOTICE file distributed with this work for additional
* information regarding copyright ownership.
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations
* under the License.
*
* SPDX-License-Identifier: MIT
******************************************************************************/
package tasks

import (
	"apworkshop/internal/appErr"
	"apworkshop/internal/common"
	"apworkshop/internal/configs"
	"apworkshop/internal/dao"
	"apworkshop/internal/entity"
	"apworkshop/internal/mq/producer"
	"apworkshop/internal/proto"
	appStorage "apworkshop/internal/storage"
	"encoding/json"
	"fmt"
	"strings"

	"github.com/apulis/go-business/pkg/jobscheduler"
	"github.com/google/uuid"
	"github.com/mitchellh/mapstructure"
)

type Model_Transformer_Handler struct{}

type Model_transformer_Meta struct {
	ModelId        int    `json:"modelId"`
	ModelVersionId int    `json:"modelVersionId"`
	Format         string `json:"format"`
	DeviceType     string `json:"deviceType"`
	ServiceId      string `json:"serviceId"`
	QueueName      string `json:"queueName"`
}

func (*Model_Transformer_Handler) HandleOnSuccess(job entity.Job) error {
	logger.Info("Job is a success")
	metaData := Model_transformer_Meta{}

	err := mapstructure.Decode(job.JobMeta, &metaData)
	if err != nil {
		return err
	}
	rsp := proto.StudioModelTransformRsp{
		ModelId:        metaData.ModelId,
		ModelVersionId: metaData.ModelVersionId,
		Success:        true,
		ServiceId:      metaData.ServiceId,
	}
	msg, err := json.Marshal(rsp)
	if err != nil {
		return err
	}
	err = producer.PublishInference(msg, metaData.QueueName)
	if err != nil {
		return err
	}
	logger.Info("Queue Name: ", metaData.QueueName)
	logger.Info("Producer send message: ", string(msg))
	return nil
}

// TODO why change cv dataset status?
func (*Model_Transformer_Handler) HandleOnFailure(job entity.Job) error {
	logger.Info("Job is failed")
	metaData := Model_transformer_Meta{}
	err := mapstructure.Decode(job.JobMeta, &metaData)
	if err != nil {
		return err
	}
	rsp := proto.StudioModelTransformRsp{
		ModelId:        metaData.ModelId,
		ModelVersionId: metaData.ModelVersionId,
		Success:        false,
		ServiceId:      metaData.ServiceId,
	}
	msg, err := json.Marshal(rsp)
	if err != nil {
		return err
	}
	err = producer.PublishInference(msg, metaData.QueueName)
	if err != nil {
		return err
	}
	logger.Info("Queue Name: ", metaData.QueueName)
	logger.Info("Producer send message: ", string(msg))
	return nil
}

func CreateModelTransformJob(modelId, modelVersionId int, deviceType, serviceId, queueName string) (string, int, error) {
	var req jobscheduler.CreateJobReq
	req.ImageName = configs.Config.JobImages.ModelTransformer
	inference, err := dao.GetStudioInferenceByVersionlId(modelVersionId)
	if err != nil {
		return "", appErr.NOT_FOUND_ERROR_CODE, err
	}
	version, err := dao.GetStudioModelVersionById(modelVersionId)
	if err != nil {
		return "", appErr.NOT_FOUND_ERROR_CODE, err
	}
	localPath, err := appStorage.Handler.GetTranslatePath(version.StoragePath)
	if err != nil {
		return "", appErr.APP_ERROR_CODE, err
	}

	var cmd string
	if deviceType == "huawei_npu" {
		if inference.Center.Format == "air" {
			cmd = fmt.Sprintf("python /work/main.py --source_format=air --model_path=%s --output_path=%s", localPath+"/infer/"+inference.Center.ModelPath, localPath+"/infer")
		} else {
			return "", appErr.NOT_FOUND_ERROR_CODE, err
		}
	}

	if deviceType == "nvidia_gpu" {
		if inference.Center.Format == "saved_model" {
			cmd = fmt.Sprintf("python /work/main.py  --source_format=saved_model --model_path=%s  --output_path=%s", localPath+"/infer/"+inference.Center.ModelPath, localPath+"/infer")
		} else if inference.Center.Format == "onnx" {
			cmd = fmt.Sprintf("python /work/main.py  --source_format=onnx --model_path=%s  --output_path=%s", localPath+"/infer/"+inference.Center.ModelPath, localPath+"/infer")
		} else {
			return "", appErr.NOT_FOUND_ERROR_CODE, err
		}
	}
	req.Cmd = strings.Split(cmd, " ")
	req.Labels = map[string]string{"jobType": common.JOB_TYPE_MODEL_TRANSFORMER}
	var mountPoints []jobscheduler.MountPoint
	// if configs.Config.Job.IsHostpath {
	// 	for _, hostPath := range configs.Config.Job.JobMountPoints.HostPaths {
	// 		var mountPoint jobscheduler.MountPoint
	// 		mountPoint.ReadOnly = false
	// 		mountPoint.Path = hostPath.Path
	// 		mountPoint.ContainerPath = hostPath.MountPath
	// 		mountPoints = append(mountPoints, mountPoint)
	// 	}
	// } else {
	// 	var mountPointDataset jobscheduler.MountPoint
	// 	mountPointDataset.ReadOnly = false
	// 	mountPointDataset.Path = fmt.Sprintf("pvc://%s", configs.Config.Job.ModelDataPvcName)
	// 	mountPointDataset.ContainerPath = fmt.Sprintf("%s", configs.Config.Paths.ModelPvcPath)
	// 	mountPoints = append(mountPoints, mountPointDataset)

	// 	var mountPointApp jobscheduler.MountPoint
	// 	mountPointApp.ReadOnly = false
	// 	mountPointApp.Path = fmt.Sprintf("pvc://%s", configs.Config.Job.AppDataPvcName)
	// 	mountPointApp.ContainerPath = fmt.Sprintf("%s", configs.Config.Paths.AppPvcPath)
	// 	mountPoints = append(mountPoints, mountPointApp)
	// }
	var mountPointDataset jobscheduler.MountPoint
	mountPointDataset.ReadOnly = false
	mountPointDataset.Path = fmt.Sprintf("pvc://%s", configs.Config.Job.ModelDataPvcName)
	mountPointDataset.ContainerPath = fmt.Sprintf("%s", configs.Config.Paths.ModelPvcPath)
	mountPoints = append(mountPoints, mountPointDataset)

	var mountPointApp jobscheduler.MountPoint
	mountPointApp.ReadOnly = false
	mountPointApp.Path = fmt.Sprintf("pvc://%s", configs.Config.Job.AppDataPvcName)
	mountPointApp.ContainerPath = fmt.Sprintf("%s", configs.Config.Paths.AppPvcPath)
	mountPoints = append(mountPoints, mountPointApp)

	req.MountPoints = mountPoints
	req.JobId = "mt-" + uuid.NewString()
	rsp, err := createJob(req)
	if err != nil {
		return "", appErr.STUDIO_MODEL_JOB_ERROR_CODE, err
	}
	var job entity.Job
	job.JobID = rsp.JobId
	job.JobType = common.JOB_TYPE_MODEL_TRANSFORMER
	job.JobImage = req.ImageName
	job.JobCmd = strings.Join(req.Cmd, " ")
	job.JobStatus = rsp.JobState.Name
	job.JobMeta = entity.JsonB{"modelId": modelId, "modelVersionId": modelVersionId, "deviceType": deviceType, "serviceId": serviceId, "queueName": queueName}
	job.JobSchedulerMsg = rsp.JobState.Msg
	job, err = dao.CreateJob(job)
	if err != nil {
		return "", appErr.STUDIO_MODEL_JOB_ERROR_CODE, err
	}

	return rsp.JobId, appErr.SUCCESS_CODE, nil
}
