/* ******************************************************************************
* 2019 - present Contributed by Apulis Technology (Shenzhen) Co. LTD
*
* This program and the accompanying materials are made available under the
* terms of the MIT License, which is available at
* https://www.opensource.org/licenses/MIT
*
* See the NOTICE file distributed with this work for additional
* information regarding copyright ownership.
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations
* under the License.
*
* SPDX-License-Identifier: MIT
******************************************************************************/
package tasks

import (
	"apworkshop/internal/common"
	"apworkshop/internal/configs"
	"apworkshop/internal/dao"
	"encoding/json"
	"fmt"

	"github.com/apulis/go-business/pkg/jobscheduler"
	"github.com/apulis/sdk/go-utils/broker"
)

func RabbitMQMsgHandler(event broker.Event) error {
	var jobMsg jobscheduler.JobMsg
	err := json.Unmarshal(event.Message().Body, &jobMsg)
	if err != nil {
		return err
	}
	logger.Info(fmt.Sprintf("RabbitMQ Message JobID %s Status: %v", jobMsg.JobId, jobMsg.JobState))

	err = HandleJobMsg(jobMsg)
	if err != nil {
		logger.Errorf("HandleJobMsg error %v", err)
		return err
	}
	return nil
}

func HandleJobMsg(jobMsg jobscheduler.JobMsg) error {
	var jobHandler JobHandler
	job, err := dao.GetJobById(jobMsg.JobId)
	if err != nil {
		return err
	}
	jobType := job.JobType
	switch jobType {
	case common.JOB_TYPE_MODEL_TRANSFORMER:
		jobHandler = &Model_Transformer_Handler{}
	default:
		logger.Error(fmt.Errorf("job type %s not supported", jobType))
		return fmt.Errorf("job type %s not supported", jobType)
	}

	jobStatus := job.JobStatus
	jobStatusName := jobMsg.JobState.Name
	switch jobStatus {
	case common.JOB_STATUS_QUEUEING, common.JOB_STATUS_SCHEDULING, common.JOB_STATUS_RUNNING, common.JOB_STATUS_UNAPPROVE:
		switch jobStatusName {
		case jobscheduler.JOB_STATUS_NAME_QUEUEING, jobscheduler.JOB_STATUS_NAME_SCHEDULING, jobscheduler.JOB_STATUS_NAME_RUNNING, jobscheduler.JOB_STATUS_NAME_UNAPPROVE:
			job.JobStatus = jobStatusName
			job.JobSchedulerMsg = jobMsg.JobState.Msg
			_, err := dao.UpdateJob(job)
			if err != nil {
				return err
			}
		case jobscheduler.JOB_STATUS_NAME_FINISHED:
			job.JobStatus = jobStatusName
			job.JobSchedulerMsg = jobMsg.JobState.Msg
			job, err := dao.UpdateJob(job)
			if err != nil {
				return err
			}
			err = jobHandler.HandleOnSuccess(job)
			if err != nil {
				return err
			}
			if !configs.Config.Job.IsDev {
				defer func(jobId string) {
					_, err := deleteJob(jobId)
					if err != nil {
						logger.Error(err)
						job.JobStatus = common.JOB_STATUS_DELETE_JOB_ERROR
						_, _ = dao.UpdateJob(job)
					}
				}(job.JobID)
			}

		case jobscheduler.JOB_STATUS_NAME_ERROR:
			job.JobStatus = jobStatusName
			job.JobSchedulerMsg = jobMsg.JobState.Msg
			job, err := dao.UpdateJob(job)
			if err != nil {
				return err
			}
			err = jobHandler.HandleOnFailure(job)
			if err != nil {
				return err
			}
			if !configs.Config.Job.IsDev {
				defer func(jobId string) {
					_, err := deleteJob(jobId)
					if err != nil {
						logger.Error(err)
						job.JobStatus = common.JOB_STATUS_DELETE_JOB_ERROR
						_, _ = dao.UpdateJob(job)
					}
				}(job.JobID)
			}
		}
	case common.JOB_STATUS_FINISHED:

	case common.JOB_STATUS_ERROR:

	case common.JOB_STATUS_DELETE_JOB_ERROR:
		job.JobStatus = jobStatusName
		job.JobSchedulerMsg = jobMsg.JobState.Msg
		job, err = dao.UpdateJob(job)
		if err != nil {
			return err
		}
		if !configs.Config.Job.IsDev {
			defer func(jobId string) {
				_, err := deleteJob(jobId)
				if err != nil {
					logger.Error(err)
					job.JobStatus = common.JOB_STATUS_DELETE_JOB_ERROR
					_, _ = dao.UpdateJob(job)
				}
			}(job.JobID)
		}
	}

	return nil
}
