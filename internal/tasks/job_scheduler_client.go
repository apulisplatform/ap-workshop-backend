/* ******************************************************************************
* 2019 - present Contributed by Apulis Technology (Shenzhen) Co. LTD
*
* This program and the accompanying materials are made available under the
* terms of the MIT License, which is available at
* https://www.opensource.org/licenses/MIT
*
* See the NOTICE file distributed with this work for additional
* information regarding copyright ownership.
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations
* under the License.
*
* SPDX-License-Identifier: MIT
******************************************************************************/
package tasks

import (
	"apworkshop/internal/configs"
	"apworkshop/internal/loggers"
	"apworkshop/internal/proto"
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"time"

	"github.com/apulis/go-business/pkg/jobscheduler"

	"github.com/mitchellh/mapstructure"
)

var logger = loggers.LogInstance()

//TODO config this client
var client = &http.Client{
	Timeout: time.Second * 10,
}

// TODO url should be const to avoid being changed
// var url = fmt.Sprintf("%s://%s:%d/%s/%s",
// 	configs.Config.JobScheduler.Scheme,
// 	configs.Config.JobScheduler.Host,
// 	configs.Config.JobScheduler.Port,
// 	configs.Config.JobScheduler.UrlPrefix,
// 	"jobs")

func createJob(create jobscheduler.CreateJobReq) (jobscheduler.CreateJobRsp, error) {
	url := fmt.Sprintf("%s://%s:%d/%s/%s",
		configs.Config.JobScheduler.Scheme,
		configs.Config.JobScheduler.Host,
		configs.Config.JobScheduler.Port,
		configs.Config.JobScheduler.UrlPrefix,
		"jobs")
	create.ModId = configs.Config.Job.ModId
	create.ResType = jobscheduler.RESOURCE_TYPE_JOB
	create.Namespace = configs.Config.Job.JobNamespace
	create.ArchType = configs.Config.Job.ArchType
	// if configs.Config.Job.IsHostpath {
	// 	var mountPoint = jobscheduler.MountPoint{
	// 		Path:          configs.Config.Job.ConfigMountPath.Path,
	// 		ContainerPath: configs.Config.Job.ConfigMountPath.MountPath,
	// 		ReadOnly:      true,
	// 	}
	// 	create.MountPoints = append(create.MountPoints, mountPoint)
	// } else {
	// 	var mountPoint = jobscheduler.MountPoint{
	// 		Path:          fmt.Sprintf("cm://%s/%s", configs.Config.Job.ConfigMapName, configs.Config.Job.ConfigMapSubpath),
	// 		ContainerPath: configs.Config.Job.ConfigMapContainerPath,
	// 		ReadOnly:      true,
	// 	}
	// 	create.MountPoints = append(create.MountPoints, mountPoint)
	// }
	var createJobRsp jobscheduler.CreateJobRsp
	data, err := json.Marshal(create)
	fmt.Println(data)
	if err != nil {
		return createJobRsp, err
	}
	logger.Info("CreateJobScheduler JSON: ", string(data))
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(data))
	if err != nil {
		return createJobRsp, err
	}
	req.Header.Set("Content-Type", "application/json")
	resp, err := client.Do(req)
	if err != nil {
		return createJobRsp, err
	}
	defer resp.Body.Close()
	var respBody proto.APISuccessResp

	// logger.Info("-------------------------------------")
	// bodys, _ := ioutil.ReadAll(resp.Body)
	// logger.Info(string(bodys))
	err = json.NewDecoder(resp.Body).Decode(&respBody)
	if err != nil {
		return createJobRsp, err
	}
	// logger.Info("-------------------------------------")

	err = mapstructure.Decode(respBody.Data, &createJobRsp)
	if err != nil {
		return createJobRsp, err
	}
	logger.Info("CreateJobScheduler Response:", createJobRsp, "HTTP StatusCode:", resp.StatusCode)
	switch resp.StatusCode {
	case 200:
		return createJobRsp, nil
	default:
		return createJobRsp, errors.New(fmt.Sprintf("job-scheduler create job error: %v", respBody.Msg))
	}
}

func deleteJob(jobId string) (proto.APISuccessResp, error) {
	var deleteJobRsp proto.APISuccessResp
	url := fmt.Sprintf("%s://%s:%d/%s/%s",
		configs.Config.JobScheduler.Scheme,
		configs.Config.JobScheduler.Host,
		configs.Config.JobScheduler.Port,
		configs.Config.JobScheduler.UrlPrefix,
		"jobs")
	deleteUrl := url + fmt.Sprintf("/%s", jobId)
	req, err := http.NewRequest("DELETE", deleteUrl, nil)
	if err != nil {
		return deleteJobRsp, err
	}
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	resp, err := client.Do(req)
	if err != nil {
		return deleteJobRsp, err
	}
	err = json.NewDecoder(resp.Body).Decode(&deleteJobRsp)
	if err != nil {
		return deleteJobRsp, err
	}
	logger.Info(fmt.Sprintf("job scheduler delete job %s status code:%d response:%v", jobId, resp.StatusCode, deleteJobRsp))
	switch resp.StatusCode {
	case 200:
		return deleteJobRsp, nil
	default:
		return deleteJobRsp, errors.New(fmt.Sprintf("job-scheduler error %d %s", resp.StatusCode, deleteJobRsp.Msg))
	}
}
