/* ******************************************************************************
* 2019 - present Contributed by Apulis Technology (Shenzhen) Co. LTD
*
* This program and the accompanying materials are made available under the
* terms of the MIT License, which is available at
* https://www.opensource.org/licenses/MIT
*
* See the NOTICE file distributed with this work for additional
* information regarding copyright ownership.
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations
* under the License.
*
* SPDX-License-Identifier: MIT
******************************************************************************/
package entity

type HuaweiManifest struct {
	Algorithm AlgorithmStruct   `yaml:"algorithm"`
	Train     HuaweiTrainStruct `yaml:"train"`
	Eval      HuaweiEvalStruct  `yaml:"eval"`
}

type AlgorithmStruct struct {
	Name        string           `yaml:"name"`
	Framework   string           `yaml:"framework"`
	ModelType   string           `yaml:"model_type"`
	Description HuaweiDescStruct `yaml:"description"`
}

type HuaweiDescStruct struct {
	Zh string `yaml:"zh"`
	En string `yaml:"en"`
}

type HuaweiTrainStruct struct {
	Engine     string              `yaml:"engine"`
	Entry      string              `yaml:"entry"`
	CommPara   []HuaweiParamStruct `yaml:"comm_para"`
	CustomPara []HuaweiParamStruct `yaml:"custom_para"`
}

type HuaweiParamStruct struct {
	Name     string           `yaml:"name"`
	PlatName string           `yaml:"plat_name"`
	Type     string           `yaml:"type"`
	Visible  bool             `yaml:"visible"`
	Default  string           `yaml:"default"`
	Desc     HuaweiDescStruct `yaml:"desc"`
}

type HuaweiEvalStruct struct {
	Entry      string              `yaml:"entry"`
	Desc       string              `yaml:"desc"`
	CommPara   []HuaweiParamStruct `yaml:"comm_para"`
	CustomPara []HuaweiParamStruct `yaml:"custom_para"`
}
