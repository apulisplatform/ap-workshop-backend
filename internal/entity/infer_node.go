/* ******************************************************************************
* 2019 - present Contributed by Apulis Technology (Shenzhen) Co. LTD
*
* This program and the accompanying materials are made available under the
* terms of the MIT License, which is available at
* https://www.opensource.org/licenses/MIT
*
* See the NOTICE file distributed with this work for additional
* information regarding copyright ownership.
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations
* under the License.
*
* SPDX-License-Identifier: MIT
******************************************************************************/
package entity

import (
	"apworkshop/internal/common"
	"apworkshop/internal/databases"
	"database/sql/driver"
	"encoding/json"
)

type InferNodeDbStruct struct {
	ID             int64           `gorm:"primary_key" json:"id"`
	InferID        int64           `json:"infer_id"`
	Framework      string          `json:"framework" yaml:"framework"`
	InferenceModel string          `json:"inference_model" yaml:"inference_model"`
	Engine         string          `json:"engine" yaml:"engine"`
	DeviceType     string          `json:"deviceType" yaml:"device_type"`
	DeviceModel    string          `json:"deviceModel" yaml:"device_model"`
	NodeType       string          `json:"nodeType" yaml:"node_type"`
	CreatedAt      common.UnixTime `json:"created_at"`
	UpdatedAt      common.UnixTime `json:"updated_at"`
}

func (array InferDbStruct) Value() (driver.Value, error) {
	result, err := json.Marshal(array)
	if err != nil {
		return nil, err
	}
	return result, nil
}

func (array *InferDbStruct) Scan(data interface{}) error {
	return json.Unmarshal(data.([]byte), &array)
}

func CreateInferNode(inferNode *InferNodeDbStruct) error {
	return databases.Db.Create(inferNode).Error
}
