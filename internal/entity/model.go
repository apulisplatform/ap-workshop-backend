/* ******************************************************************************
* 2019 - present Contributed by Apulis Technology (Shenzhen) Co. LTD
*
* This program and the accompanying materials are made available under the
* terms of the MIT License, which is available at
* https://www.opensource.org/licenses/MIT
*
* See the NOTICE file distributed with this work for additional
* information regarding copyright ownership.
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations
* under the License.
*
* SPDX-License-Identifier: MIT
******************************************************************************/
package entity

import (
	"apworkshop/internal/common"
	"apworkshop/internal/databases"
	"database/sql/driver"
	"encoding/json"
	"strings"

	"gorm.io/gorm"
)

const (
	MODEL_PREPARE_STATUS = "prepare" // when this model has no version committed, it is "prepare"
	MODEL_READY_STATUS   = "ready"

	MODEL_ABILITY_INFER      = "infer"
	MODEL_ABILITY_TRAIN      = "train"
	MODEL_ABILITY_EDGE_INFER = "edgeInfer"
)

type ModelDbStruct struct {
	ID                   int64                   `gorm:"primary_key" json:"id"`
	Scope                string                  `json:"scope" gorm:"uniqueIndex:scope"`
	Platform             stringArray             `json:"platform" yaml:"platform" validate:"required"`
	Label                stringArray             `json:"label" yaml:"label"`
	Props                ParamsArrayStruct       `json:"props" yaml:"props"`
	JobId                string                  `json:"jobId" yaml:"job_id"`
	Name                 string                  `json:"name" yaml:"name" validate:"required" gorm:"uniqueIndex:scope"`
	DeviceType           string                  `json:"device_type" yaml:"device_type"`
	Engine               string                  `json:"engine" yaml:"engine"`
	Framework            string                  `json:"framework" yaml:"framework" validate:"required"`
	DistributedFramework string                  `json:"distributedFramework" yaml:"distributed_framework"`
	Backbone             string                  `json:"backbone" yaml:"backbone"`
	Description          string                  `json:"description" yaml:"description"`
	Dataset              ModelDatasetArrayStruct `json:"dataset" yaml:"dataset" `
	Type                 string                  `json:"type" yaml:"type" validate:"required"`
	Ability              stringArray             `json:"ability"`
	IsPreload            bool                    `json:"isPreload"`
	Status               string                  `json:"status"`
	/////// these attributes are only used for reading yaml, they will save to version when model commit
	Train   *ModelDescStruct `json:"train" yaml:"train"`
	Eval    *ModelDescStruct `json:"eval" yaml:"eval"`
	Extends string           `json:"extends" yaml:"extends"`
	//////
	CreatedAt common.UnixTime `json:"createdAt"`
	UpdatedAt common.UnixTime `json:"updatedAt"`
	DeletedAt gorm.DeletedAt  `json:"deletedAt" gorm:"uniqueIndex:scope"`
}

type stringArray []string

func (array stringArray) Value() (driver.Value, error) {
	result, err := json.Marshal(array)
	if err != nil {
		return nil, err
	}
	return result, nil
}

func (array *stringArray) Scan(data interface{}) error {
	return json.Unmarshal(data.([]byte), &array)
}

type ModelDatasetStruct struct {
	Usage  string   `json:"usage" yaml:"usage"`
	Type   []string `json:"type" yaml:"type"`
	Format string   `json:"format" yaml:"format"`
	Name   string   `json:"name" yaml:"name"`
	Path   string   `json:"path" yaml:"path"`
}

type ModelDatasetArrayStruct []ModelDatasetStruct

func (target ModelDatasetArrayStruct) Value() (driver.Value, error) {
	result, err := json.Marshal(target)
	if err != nil {
		return nil, err
	}
	return result, nil
}

func (target *ModelDatasetArrayStruct) Scan(data interface{}) error {
	return json.Unmarshal(data.([]byte), &target)
}

type ModelDescStruct struct {
	PretrainedModel string              `json:"pretrainedModel" yaml:"pretrained_model"`
	Entry           string              `json:"entry" yaml:"entry"`
	UserParams      []ParamsStruct      `json:"userParams" yaml:"user_params"`
	SysParams       []SystemParamStruct `json:"sysParams" yaml:"sys_params"`
}

func (target ModelDescStruct) Value() (driver.Value, error) {
	result, err := json.Marshal(target)
	if err != nil {
		return nil, err
	}
	return result, nil
}

func (target *ModelDescStruct) Scan(data interface{}) error {
	return json.Unmarshal(data.([]byte), &target)
}

type ParamsStruct struct {
	Name     string `json:"name" yaml:"name"`
	Type     string `json:"type" yaml:"type"`
	Default  string `json:"default" yaml:"default"`
	Required bool   `json:"required" yaml:"required"`
	Desc     string `json:"desc" yaml:"desc"`
	Editable bool   `json:"editable" yaml:"editable"`
}
type SystemParamStruct struct {
	Name string `json:"name" yaml:"name"`
	Arg  string `json:"arg" yaml:"arg"`
}

type ParamsArrayStruct []ParamsStruct

func (target ParamsArrayStruct) Value() (driver.Value, error) {
	result, err := json.Marshal(target)
	if err != nil {
		return nil, err
	}
	return result, nil
}

func (target *ParamsArrayStruct) Scan(data interface{}) error {
	return json.Unmarshal(data.([]byte), &target)
}

func CreateModel(model *ModelDbStruct, isPrelod bool) (*ModelDbStruct, error) {
	model.IsPreload = isPrelod
	model.Status = MODEL_PREPARE_STATUS // when model is first created, we think no version is commited, so it is not ready
	return model, databases.Db.Create(model).Error
}

func GetModelByNameAndScope(name, scope string) (*ModelDbStruct, error) {
	var count int64
	err := databases.Db.
		Model(&ModelDbStruct{}).
		Where("name = ?", name).
		Where("scope = ?", scope).
		Count(&count).
		Error
	if err != nil {
		return nil, err
	}
	if count == 0 {
		return nil, nil
	}
	var model ModelDbStruct
	err = databases.Db.
		Where("name = ?", name).
		Where("scope = ?", scope).
		First(&model).
		Error
	if err != nil {
		return nil, err
	}

	return &model, nil
}

func GetModelByName(name string) (*ModelDbStruct, error) {
	var count int64
	err := databases.Db.
		Model(&ModelDbStruct{}).
		Where("name = ?", name).
		Count(&count).
		Error
	if err != nil {
		return nil, err
	}
	if count == 0 {
		return nil, nil
	}
	var model ModelDbStruct
	err = databases.Db.
		Where("name = ?", name).
		First(&model).
		Error
	if err != nil {
		return nil, err
	}

	return &model, nil
}

func GetModelById(modelId int) (*ModelDbStruct, error) {
	var model ModelDbStruct
	err := databases.Db.
		Where("id = ?", modelId).
		First(&model).
		Error
	if err != nil {
		return nil, err
	}

	return &model, nil
}

func GetModelList(pageNum, pageSize int, scope, name, platform, modelType, sortArgs string, canInfer, canTrain, isPrelod bool) ([]ModelDbStruct, int, error) {
	var result []ModelDbStruct
	var total int64

	tempRes := databases.Db
	if scope != "" {
		tempRes = tempRes.
			Where("scope = ?", scope)
	}

	// only show ready
	tempRes = tempRes.
		Where("status = ?", MODEL_READY_STATUS)

	// check name
	if name != "" {
		tempRes = tempRes.
			Where("name like ?", "%"+name+"%")
	}

	if platform != "" {
		tempRes = tempRes.
			Where("platform like ?", "%"+platform+"%")
	}

	// check type
	if modelType != "" {
		tempRes = tempRes.
			Where("type = ?", modelType)
	}

	if canInfer {
		tempRes = tempRes.
			Where("can_infer = ?", canInfer)
	}
	if canTrain {
		tempRes = tempRes.
			Where("train != ?", "null")
	}

	tempRes = tempRes.
		Where("is_preload = ?", isPrelod)

	err := tempRes.Model(&ModelDbStruct{}).Count(&total).Error
	if err != nil {
		return nil, int(total), err
	}

	// check order
	if sortArgs != "" {
		argsArray := strings.Split(sortArgs, ",")
		for _, args := range argsArray {
			sortNameAndOrder := strings.Split(args, "|")
			sortName := sortNameAndOrder[0]
			sortOrder := sortNameAndOrder[1]

			if sortName == "createdAt" {
				sortName = "created_at"
			}

			if sortName == "updatedAt" {
				sortName = "updated_at"
			}

			if sortOrder != "desc" && sortOrder != "asc" {
				sortOrder = "desc"
			}

			tempRes = tempRes.
				Order(sortName + " " + sortOrder)
		}
	} else {
		tempRes = tempRes.
			Order("created_at" + " " + "desc")
	}

	if pageSize != 0 {
		offset := pageSize * (pageNum - 1)
		limit := pageSize
		tempRes = tempRes.Offset(offset).Limit(limit)
	}

	err = tempRes.Find(&result).Error

	if err != nil {
		return nil, int(total), err
	}

	return result, int(total), nil
}

func UpdateModel(model *ModelDbStruct) (*ModelDbStruct, error) {
	err := databases.Db.
		Updates(model).
		Error
	if err != nil {
		return nil, err
	}
	return model, nil
}

func DeleteModelById(id int) error {
	err := databases.Db.
		Where("id = ?", id).
		Delete(&ModelDbStruct{}).
		Error
	if err != nil {
		return err
	}

	return nil
}

func GetModelTypes() ([]string, error) {
	var types []string
	var modelList []ModelDbStruct
	err := databases.Db.
		Model(&ModelDbStruct{}).
		Where("type != ?", "").
		Select("type").
		Distinct("type").
		Find(&modelList).
		Error
	if err != nil {
		return nil, err
	}

	for _, model := range modelList {
		modelType := model.Type
		types = append(types, modelType)
	}

	return types, nil
}
