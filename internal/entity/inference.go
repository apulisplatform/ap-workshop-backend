/* ******************************************************************************
* 2019 - present Contributed by Apulis Technology (Shenzhen) Co. LTD
*
* This program and the accompanying materials are made available under the
* terms of the MIT License, which is available at
* https://www.opensource.org/licenses/MIT
*
* See the NOTICE file distributed with this work for additional
* information regarding copyright ownership.
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations
* under the License.
*
* SPDX-License-Identifier: MIT
******************************************************************************/
package entity

import (
	"apworkshop/internal/common"
	"apworkshop/internal/databases"
	"database/sql/driver"
	"encoding/json"
	"strings"
)

type InferDbStruct struct {
	ID             int64             `gorm:"primary_key" json:"id"`
	ModelVersionID int64             `json:"modelVersionId"`
	Name           string            `json:"name" yaml:"name"`
	Platform       stringArray       `json:"platform" yaml:"platform"`
	Backbone       string            `json:"backbone" yaml:"backbone"`
	Label          stringArray       `json:"label" yaml:"label"`
	Description    string            `json:"description" yaml:"description"`
	Type           string            `json:"type" yaml:"type"`
	Format         string            `json:"format" yaml:"format"`
	Props          ParamsArrayStruct `json:"props" yaml:"props"`
	Tags           mapStructure      `json:"tags" yaml:"tags"`
	CreatedAt      common.UnixTime   `json:"created_at"`
	UpdatedAt      common.UnixTime   `json:"updated_at"`
}

type mapStructure map[string]interface{}

func (array mapStructure) Value() (driver.Value, error) {
	result, err := json.Marshal(array)
	if err != nil {
		return nil, err
	}
	return result, nil
}

func (array *mapStructure) Scan(data interface{}) error {
	return json.Unmarshal(data.([]byte), &array)
}

type InferStruct struct {
	InferDbStruct `mapstructure:",squash,remain"`
	Center        *InferNodeDbStruct `json:"center" yaml:"center"`
	Edge          *InferNodeDbStruct `json:"edge" yaml:"edge"`
}

func CreateInference(modelId, modelVersionId int, inference *InferStruct) error {

	// create inference
	inferDb := inference.InferDbStruct
	err := databases.Db.Create(&inferDb).Error
	if err != nil {
		return err
	}
	// create inference node
	if inference.Center != nil {
		centerNode := &InferNodeDbStruct{
			InferID:        inferDb.ID,
			Framework:      inference.Center.Framework,
			InferenceModel: inference.Center.InferenceModel,
			Engine:         inference.Center.Engine,
			DeviceType:     inference.Center.DeviceType,
			DeviceModel:    inference.Center.DeviceModel,
			NodeType:       common.INFER_CENTER_NODE_TYPE,
		}
		err = CreateInferNode(centerNode)
		if err != nil {
			return err
		}
	}
	if inference.Edge != nil {
		edgeNode := &InferNodeDbStruct{
			InferID:        inferDb.ID,
			Framework:      inference.Edge.Framework,
			InferenceModel: inference.Edge.InferenceModel,
			Engine:         inference.Edge.Engine,
			DeviceType:     inference.Edge.DeviceType,
			DeviceModel:    inference.Edge.DeviceModel,
			NodeType:       common.INFER_EDGE_NODE_TYPE,
		}
		err = CreateInferNode(edgeNode)
		if err != nil {
			return err
		}
	}

	// update model infer info
	model, err := GetModelById(modelId)
	if err != nil {
		return err
	}
	// create model ability info
	arrayString, err := json.Marshal(model.Ability)
	if err != nil {
		return err
	}
	if !strings.Contains(string(arrayString), MODEL_ABILITY_INFER) {
		model.Ability = append(model.Ability, MODEL_ABILITY_INFER)
	}
	if inference.Edge != nil {
		if !strings.Contains(string(arrayString), MODEL_ABILITY_EDGE_INFER) {
			model.Ability = append(model.Ability, MODEL_ABILITY_EDGE_INFER)
		}
	}
	UpdateModel(model)

	return nil
}

func GetInferenceByVersionlId(modelId int) (*InferStruct, error) {
	var count int64
	var infer InferStruct
	var inferDb InferDbStruct
	var inferEdgeNode InferNodeDbStruct
	var inferCenterNode InferNodeDbStruct

	// make sure model has inference info
	err := databases.Db.
		Model(&InferDbStruct{}).
		Where("model_version_id = ?", modelId).
		Count(&count).
		Error
	if err != nil {
		return nil, err
	}
	if count == 0 {
		return nil, nil
	}
	err = databases.Db.
		Where("model_version_id = ?", modelId).
		First(&inferDb).
		Error
	if err != nil {
		return nil, err
	}
	infer.InferDbStruct = inferDb

	// check if has center inference
	err = databases.Db.
		Model(&InferNodeDbStruct{}).
		Where("infer_id = ?", infer.ID).
		Where("node_type = ?", common.INFER_CENTER_NODE_TYPE).
		Count(&count).
		Error
	if err != nil {
		return nil, err
	}
	if count != 0 {
		err = databases.Db.
			Where("infer_id = ?", infer.ID).
			Where("node_type = ?", common.INFER_CENTER_NODE_TYPE).
			First(&inferCenterNode).
			Error
		if err != nil {
			return nil, err
		}
		infer.Center = &inferCenterNode
	}

	// check if has edge inference
	err = databases.Db.
		Model(&InferNodeDbStruct{}).
		Where("infer_id = ?", infer.ID).
		Where("node_type = ?", common.INFER_EDGE_NODE_TYPE).
		Count(&count).
		Error
	if err != nil {
		return nil, err
	}
	if count != 0 {
		err = databases.Db.
			Where("infer_id = ?", infer.ID).
			Where("node_type = ?", common.INFER_CENTER_NODE_TYPE).
			First(&inferEdgeNode).
			Error
		if err != nil {
			return nil, err
		}
		infer.Edge = &inferEdgeNode
	}

	return &infer, nil
}
