/* ******************************************************************************
* 2019 - present Contributed by Apulis Technology (Shenzhen) Co. LTD
*
* This program and the accompanying materials are made available under the
* terms of the MIT License, which is available at
* https://www.opensource.org/licenses/MIT
*
* See the NOTICE file distributed with this work for additional
* information regarding copyright ownership.
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations
* under the License.
*
* SPDX-License-Identifier: MIT
******************************************************************************/
package entity

type StudioInferenceModel struct {
	GormModel

	ModelVersionID int                    `json:"modelVersionId"`
	Name           string                 `json:"name" yaml:"name" validate:"required"`
	Backbone       string                 `json:"backbone" yaml:"backbone"`
	Description    string                 `json:"description" yaml:"description"`
	Field          string                 `json:"field" yaml:"field" validate:"required"`
	Task           string                 `json:"task" yaml:"task" validate:"required"`
	Platform       stringArray            `json:"platform" yaml:"platform"`
	Props          StudioPropsArrayStruct `json:"props" yaml:"props"`
	// Tags           stringArray            `json:"tag" yaml:"tag"`
}

type StudioInference struct {
	StudioInferenceModel `mapstructure:",squash,remain"`
	// StudioInferenceModel
	Center *StudioInferenceNode `json:"center" yaml:"center"`
	Edge   *StudioInferenceNode `json:"edge" yaml:"edge"`
}
