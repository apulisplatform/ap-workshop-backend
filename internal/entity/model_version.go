/* ******************************************************************************
* 2019 - present Contributed by Apulis Technology (Shenzhen) Co. LTD
*
* This program and the accompanying materials are made available under the
* terms of the MIT License, which is available at
* https://www.opensource.org/licenses/MIT
*
* See the NOTICE file distributed with this work for additional
* information regarding copyright ownership.
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations
* under the License.
*
* SPDX-License-Identifier: MIT
******************************************************************************/
package entity

import (
	"apworkshop/internal/appErr"
	"apworkshop/internal/common"
	"apworkshop/internal/databases"
	"errors"
	"fmt"
	"strconv"
	"strings"

	"github.com/gofrs/uuid"
	"gorm.io/gorm"
)

type ModelVersion struct {
	ID          int64            `gorm:"primary_key" json:"id"`
	RegisterCtx string           `json:"registerCtx" gorm:"uniqueIndex:ctx"`
	ModelID     int64            `json:"modelId" gorm:"uniqueIndex:version"`
	Name        string           `json:"name" gorm:"uniqueIndex:version"`
	Description string           `json:"description"`
	Locker      string           `json:"locker" gorm:"uniqueIndex:locker"`
	Status      string           `json:"status" validate:"oneof=prepare commit"`
	Train       *ModelDescStruct `json:"train" yaml:"train"`
	Eval        *ModelDescStruct `json:"eval" yaml:"eval"`
	StoragePath string           `json:"storagePath"`
	Extends     string           `json:"extends"`
	Tags        mapStructure     `json:"tags" yaml:"tags"`
	CreatedAt   common.UnixTime  `json:"createdAt"`
	UpdatedAt   common.UnixTime  `json:"updatedAt"`
	DeletedAt   gorm.DeletedAt   `json:"deletedAt"`
}

type VersionExtendsReadingStruct struct {
	Extends string `json:"extends" yaml:"extends"`
}

func CreateNewVersionForModel(ctx string, ModelID int64) (*ModelVersion, error) {
	var count int64
	err := databases.Db.
		Model(&ModelVersion{}).
		Where("register_ctx = ?", ctx).
		Count(&count).
		Error
	if err != nil {
		return nil, err
	}
	if count != 0 {
		var oldVersion ModelVersion
		err := databases.Db.
			Where("register_ctx = ?", ctx).
			First(&oldVersion).
			Error
		if err != nil {
			return nil, err
		}
		return &oldVersion, nil
	}
	err = databases.Db.
		Unscoped().
		Model(&ModelVersion{}).
		Where("model_id = ?", ModelID).
		Count(&count).
		Error
	if err != nil {
		return nil, err
	}

	u1, err := uuid.NewV4()
	if err != nil {
		return nil, err
	}
	// check if model has one version
	var modelVersion *ModelVersion
	if count == 0 {
		modelVersion = &ModelVersion{
			ModelID:     ModelID,
			Name:        common.FIRST_MODEL_VERSION,
			Locker:      u1.String(),
			RegisterCtx: ctx,
			Status:      common.MODEL_VERSION_STATUS_PREPARE,
		}
		result, err := CreateModelVersion(modelVersion)
		if err != nil {
			return nil, err
		}
		return result, nil
	}
	// get new version name
	newVersionName, err := GetModelLatestVersionName(int(ModelID))
	if err != nil {
		return nil, err
	}
	modelVersion = &ModelVersion{
		ModelID:     ModelID,
		Name:        newVersionName,
		Locker:      u1.String(),
		RegisterCtx: ctx,
		Status:      common.MODEL_VERSION_STATUS_PREPARE,
	}
	result, err := CreateModelVersion(modelVersion)
	if err != nil {
		return nil, err
	}
	return result, nil

}

func CreateModelVersion(version *ModelVersion) (*ModelVersion, error) {
	err := databases.Db.Create(version).Error
	if err != nil {
		return nil, err
	}

	return version, nil
}

func CommitVersion(locker string) error {
	targetVersion, err := GetModelVersionByLocker(locker)
	if err != nil {
		return err
	}
	err = databases.Db.
		Model(&ModelVersion{}).
		Where("locker = ?", locker).
		Update("status", common.MODEL_VERSION_STATUS_FREE).
		Error
	if err != nil {
		return err
	}

	err = databases.Db.
		Model(&ModelDbStruct{}).
		Where("id = ?", targetVersion.ModelID).
		Update("status", MODEL_READY_STATUS).
		Error
	if err != nil {
		return err
	}

	return nil
}

func GetModelVersion(modelVersionId int) (*ModelVersion, error) {
	fmt.Println(modelVersionId)
	var modelVersion ModelVersion
	err := databases.Db.
		Where("id = ?", modelVersionId).
		First(&modelVersion).
		Error
	if err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return nil, appErr.RECORD_NOT_FOUND_ERROR
		}
		return nil, err
	}

	return &modelVersion, nil
}

func GetModelVersionListByModel(modelId, pageNum, pageSize int) ([]ModelVersion, error) {
	var list []ModelVersion
	tempRes := databases.Db
	if pageSize != 0 {
		offset := pageSize * (pageNum - 1)
		limit := pageSize
		tempRes = tempRes.Offset(offset).Limit(limit)
	}
	err := tempRes.
		Where("model_id = ?", modelId).
		Where("status = ?", common.MODEL_VERSION_STATUS_FREE).
		Find(&list).
		Error
	if err != nil {
		return nil, err
	}

	return list, nil
}

func GetModelVersionByLocker(locker string) (*ModelVersion, error) {
	var version ModelVersion

	err := databases.Db.
		Where("locker = ?", locker).
		First(&version).
		Error
	if err != nil {
		return nil, err
	}
	return &version, nil
}

func UpdateModelVersion(modelversion *ModelVersion) (*ModelVersion, error) {

	err := databases.Db.
		Where("id = ?", modelversion.ID).
		Updates(modelversion).
		Error
	if err != nil {
		return nil, err
	}

	return modelversion, nil
}

func DeleteModelVersion(modelVersionId int) error {
	err := databases.Db.
		Where("id = ?", modelVersionId).
		Delete(&ModelVersion{}).
		Error
	if err != nil {
		return err
	}

	return nil
}

func DeleteVersionsByModelId(modelId int) error {
	err := databases.Db.
		Where("model_id = ?", modelId).
		Delete(&ModelVersion{}).
		Error
	if err != nil {
		return err
	}

	return nil
}

// carefully delete one version because it might need to delete model when no version of i
// this action might cause some lock problems
// func DeleteVersionById(modelVersionId int) error {
// 	err := databases.Db.
// 		Where("id = ?", modelVersionId).
// 		Update("status", common.MODEL_VERSION_STATUS_DELETING).
// 		Error
// 	if err != nil {
// 		return err
// 	}

// 	return nil
// }

func RollbackVersionById(modelVersionId int) error {
	err := databases.Db.
		Model(&ModelVersion{}).
		Where("id = ?", modelVersionId).
		Where("status", common.MODEL_VERSION_STATUS_PREPARE).
		Update("status", common.MODEL_VERSION_STATUS_DELETING).
		Error
	if err != nil {
		return err
	}

	return nil
}

func GetModelLatestVersionName(modelId int) (string, error) {
	var latestVersion ModelVersion
	err := databases.Db.
		Unscoped().
		Where("model_id = ?", modelId).
		Order("created_at desc").
		First(&latestVersion).
		Error
	if err != nil {
		return "", err
	}

	latestName, err := GetNextVersion(latestVersion.Name)
	if err != nil {
		return "", err
	}

	return latestName, nil
}

func GetNextVersion(formerVersion string) (string, error) {
	var newVersionName string

	// dump "1.0" to "10"
	tempString := strings.Replace(formerVersion, ".", "", -1)
	// dump "10" to 10
	formerVersionInt, err := strconv.Atoi(tempString)
	if err != nil {
		return newVersionName, err
	}
	// update 10 to 11
	tempInt := formerVersionInt + 1
	// dump 11 to "11"
	tempString = strconv.Itoa(tempInt)
	// dump "11" to "1.1"
	length := len(tempString)
	newVersionName = tempString[:length-1] + "." + tempString[length-1:]

	return newVersionName, nil
}

func GetLatestVersionByModelId(modelId int) (*ModelVersion, error) {
	var versionCount int64
	err := databases.Db.
		Model(&ModelVersion{}).
		Where("model_id = ?", modelId).
		Where("status = ?", common.MODEL_VERSION_STATUS_FREE).
		Count(&versionCount).
		Error
	if err != nil {
		return nil, err
	}
	if versionCount == 0 {
		return nil, nil
	}
	var latestVersion ModelVersion
	err = databases.Db.
		Where("model_id = ?", modelId).
		Where("status = ?", common.MODEL_VERSION_STATUS_FREE).
		Order("created_at desc").
		First(&latestVersion).
		Error
	if err != nil {
		return nil, err
	}

	return &latestVersion, nil
}

func GetVersionCommitCountByModelId(modelId int) (int, error) {
	var count int64
	var err error

	err = databases.Db.
		Model(&ModelVersion{}).
		Where("model_id = ?", modelId).
		Where("status = ?", common.MODEL_VERSION_STATUS_FREE).
		Count(&count).
		Error
	if err != nil {
		return int(count), err
	}

	return int(count), err
}
