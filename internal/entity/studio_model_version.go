/* ******************************************************************************
* 2019 - present Contributed by Apulis Technology (Shenzhen) Co. LTD
*
* This program and the accompanying materials are made available under the
* terms of the MIT License, which is available at
* https://www.opensource.org/licenses/MIT
*
* See the NOTICE file distributed with this work for additional
* information regarding copyright ownership.
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations
* under the License.
*
* SPDX-License-Identifier: MIT
******************************************************************************/
package entity

import (
	"database/sql/driver"
	"encoding/json"
)

type StudioModelVersion struct {
	GormModel

	RegisterCtx    string                `json:"registerCtx" gorm:"uniqueIndex:ctx"`
	ModelID        int                   `json:"model_id" gorm:"uniqueIndex:version"`
	Name           string                `json:"name" gorm:"uniqueIndex:version"`
	ModelName      string                `json:"modelName"`
	Dataset        string                `json:"dataset"`
	Percision      string                `json:"percision"`
	Parameters     string                `json:"parameters"`
	Publisher      string                `json:"publisher"`
	TrainTime      int                   `json:"trainTime"`
	PublishTime    UnixTime              `json:"publishTime"`
	Evaluation     StudioModelEvaluation `json:"evaluation"`
	Locker         string                `json:"locker" gorm:"uniqueIndex:locker"`
	Status         string                `json:"status" validate:"oneof=prepare commit"`
	ImagePath      string                `json:"imagePath"`
	TmpStoragePath string                `json:"tmpStoragePath"`
	StoragePath    string                `json:"storagePath"`
	Bind           string                `json:"bind"`
	UserGroup      string                `json:"userGroup"`

	// manifest.yaml inputs
	Platform             stringArray             `json:"platform"`
	Source               string                  `json:"source"`
	AutoDL               bool                    `json:"autoDL"`
	Logo                 string                  `json:"logo"`
	Redevelop            bool                    `json:"redevelop"`
	Props                StudioPropsArrayStruct  `json:"props"`
	JobId                string                  `json:"jobId"`
	Framework            string                  `json:"framework"`
	DistributedFramework string                  `json:"distributedFramework"`
	Backbone             string                  `json:"backbone"`
	Description          string                  `json:"description"`
	Engine               string                  `json:"engine"`
	Devices              StudioModelDevice       `json:"devices"`
	VerifyCommand        string                  `json:"verifyCommand"`
	Datasets             StudioModelDataset      `json:"datasets"`
	Field                string                  `json:"field"`
	Task                 string                  `json:"task"`
	ReadMe               string                  `json:"readMe"`
	RepoId               string                  `json:"repoId"`
	CommitId             string                  `json:"commitId"`
	DeviceTypeCPU        bool                    `json:"deviceTypeCPU"`
	DeviceTypeHuaweiNPU  bool                    `json:"deviceTypeHuaweiNPU"`
	DeviceTypeNvidiaGPU  bool                    `json:"deviceTypeNvidaGPU"`
	Train                *StudioModelTrainStruct `json:"train"`
	Eval                 *StudioModelEvalStruct  `json:"eval"`

	Tags TagsStruct `json:"tags"`
}

type StudioVersionEvalResult struct {
	Name       string                `json:"name"`
	Evaluation StudioModelEvaluation `json:"evaluation"`
}

type StudioCodeFile struct {
	CommitId string `json:"commitId"`
	RepoId   string `json:"repoId"`
}

type StudioModelEvaluation []StudioModelEvaluationStruct

type StudioModelEvaluationStruct struct {
	Key   string `json:"key"`
	Value string `json:"value"`
	Type  string `json:"type"`
	Desc  string `json:"desc"`
}

func (target StudioModelEvaluation) Value() (driver.Value, error) {
	result, err := json.Marshal(target)
	if err != nil {
		return nil, err
	}
	return result, nil
}

func (target *StudioModelEvaluation) Scan(data interface{}) error {
	return json.Unmarshal(data.([]byte), &target)
}
