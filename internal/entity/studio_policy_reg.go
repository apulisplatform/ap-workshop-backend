/* ******************************************************************************
* 2019 - present Contributed by Apulis Technology (Shenzhen) Co. LTD
*
* This program and the accompanying materials are made available under the
* terms of the MIT License, which is available at
* https://www.opensource.org/licenses/MIT
*
* See the NOTICE file distributed with this work for additional
* information regarding copyright ownership.
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations
* under the License.
*
* SPDX-License-Identifier: MIT
******************************************************************************/
package entity

type EndPointsAndPolicies struct {
	Module    Module       `json:"module"`
	EndPoints []EndPoint   `json:"endpoints"`
	Policies  InitPolicies `json:"policies"`
}

type Module struct {
	Account    string `json:"account"`
	ModuleName string `json:"moduleName"`
	Desc       string `json:"desc"`
}

type InitPolicies struct {
	Module    string         `json:"module"`
	SysAdmin  []StatementDto `json:"systemAdmin"`
	OrgAdmin  []StatementDto `json:"orgAdmin"`
	Developer []StatementDto `json:"developer"`
}

type EndPoint struct {
	Module       string `form:"module"`
	Desc         string `form:"desc"`
	Resource     string `form:"resource"`
	Action       string `form:"action"`
	HttpMethod   string `form:"httpMethod"`
	HttpEndpoint string `form:"httpEndpoint"`
}

type StatementDto struct {
	Actions   []string `form:"actions"`
	Resources []string `form:"resources"`
	Effect    string   `form:"effect" validate:"oneof=allow deny"`
	Role      string   `form:"role"`
}
