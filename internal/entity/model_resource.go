/* ******************************************************************************
* 2019 - present Contributed by Apulis Technology (Shenzhen) Co. LTD
*
* This program and the accompanying materials are made available under the
* terms of the MIT License, which is available at
* https://www.opensource.org/licenses/MIT
*
* See the NOTICE file distributed with this work for additional
* information regarding copyright ownership.
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations
* under the License.
*
* SPDX-License-Identifier: MIT
******************************************************************************/
package entity

import (
	"apworkshop/internal/common"
	"apworkshop/internal/databases"
	"database/sql/driver"
	"encoding/json"

	"gorm.io/gorm"
)

type ResourceFileStruct struct {
	ModelList []ResourceFileUnit `json:"modelList" yaml:"model_list"`
}

type ResourceFileUnit struct {
	Name                string                    `json:"name" yaml:"name"`
	ResourceRequirement ResourceRequirementStruct `json:"resourceRequirement" yaml:"resource_requirement"`
}

type ResourceRequirementStruct struct {
	Train ResourceRequirementDefineStruct `json:"train" yaml:"train"`
	Infer ResourceRequirementDefineStruct `json:"infer" yaml:"infer"`
}

type ResourceRequirementDefineStruct struct {
	CPU    int `json:"cpu" yaml:"cpu"`
	Memory int `json:"memory" yaml:"memory"`
	NPU    int `json:"npu" yaml:"npu"`
}

func (array ResourceRequirementDefineStruct) Value() (driver.Value, error) {
	result, err := json.Marshal(array)
	if err != nil {
		return nil, err
	}
	return result, nil
}

func (array *ResourceRequirementDefineStruct) Scan(data interface{}) error {
	return json.Unmarshal(data.([]byte), &array)
}

type ResourceDbStruct struct {
	ID        int64                           `gorm:"primary_key" json:"id"`
	ModelId   int64                           `json:"model_id"`
	Train     ResourceRequirementDefineStruct `json:"train" yaml:"train"`
	Infer     ResourceRequirementDefineStruct `json:"infer" yaml:"infer"`
	CreatedAt common.UnixTime                 `json:"createdAt"`
	UpdatedAt common.UnixTime                 `json:"updatedAt"`
	DeletedAt gorm.DeletedAt                  `json:"deletedAt"`
}

func CreateResourceDefine(target *ResourceDbStruct) error {
	err := databases.Db.
		Create(target).
		Error
	if err != nil {
		return err
	}

	return nil
}
