/* ******************************************************************************
* 2019 - present Contributed by Apulis Technology (Shenzhen) Co. LTD
*
* This program and the accompanying materials are made available under the
* terms of the MIT License, which is available at
* https://www.opensource.org/licenses/MIT
*
* See the NOTICE file distributed with this work for additional
* information regarding copyright ownership.
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations
* under the License.
*
* SPDX-License-Identifier: MIT
******************************************************************************/
package entity

import (
	"database/sql/driver"
	"encoding/json"
)

const (
	STUDIO_MODEL_PREPARE_STATUS = "prepare" // when this model has no version committed, it is "prepare"
	STUDIO_MODEL_READY_STATUS   = "ready"

	STUDIO_MODEL_ABILITY_CENTER_INFER = "centerInfer"
	STUDIO_MODEL_ABILITY_TRAIN        = "train"
	STUDIO_MODEL_ABILITY_EDGE_INFER   = "edgeInfer"
)

type StudioModel struct {
	GormModel
	Scope string `json:"scope" gorm:"uniqueIndex:scope"`

	Platform             stringArray             `json:"platform" yaml:"platform" validate:"required"`
	Source               string                  `json:"source" yaml:"source" validate:"required"`
	AutoDL               bool                    `json:"autoDL" yaml:"autodl"`
	Logo                 string                  `json:"logo" yaml:"logo"`
	Redevelop            bool                    `json:"redevelop" yaml:"redevelop"`
	Props                StudioPropsArrayStruct  `json:"props" yaml:"props"`
	JobId                string                  `json:"jobId" yaml:"job_id"`
	Name                 string                  `json:"name" yaml:"name" validate:"required" gorm:"uniqueIndex:scope"`
	Framework            string                  `json:"framework" yaml:"framework" validate:"required"`
	DistributedFramework string                  `json:"distributedFramework" yaml:"distributed_framework"`
	Backbone             string                  `json:"backbone" yaml:"backbone"`
	Description          string                  `json:"description" yaml:"description"`
	Engine               string                  `json:"engine" yaml:"engine"`
	Devices              StudioModelDevice       `json:"devices" yaml:"devices"`
	VerifyCommand        string                  `json:"verifyCommand" yaml:"verify_command"`
	Datasets             StudioModelDataset      `json:"datasets" yaml:"datasets"`
	Field                string                  `json:"field" yaml:"field" validate:"required"`
	Task                 string                  `json:"task" yaml:"task" validate:"required"`
	Ability              stringArray             `json:"ability"`
	CanTrain             bool                    `json:"canTrain"`
	CanCenterInfer       bool                    `json:"canCenterInfer"`
	CanEdgeInfer         bool                    `json:"canEdgeInfer"`
	IsPreload            bool                    `json:"isPreload"`
	Status               string                  `json:"status"`
	ReadMe               string                  `json:"readMe"`
	RepoId               string                  `json:"repoId"`
	CommitId             string                  `json:"commitId"`
	DeviceTypeCPU        bool                    `json:"deviceTypeCPU"`
	DeviceTypeHuaweiNPU  bool                    `json:"deviceTypeHuaweiNPU"`
	DeviceTypeNvidiaGPU  bool                    `json:"deviceTypeNvidaGPU"`
	UserGroup            string                  `json:"userGroup"`
	Train                *StudioModelTrainStruct `json:"train" yaml:"train" validate:"required"`
	Eval                 *StudioModelEvalStruct  `json:"eval" yaml:"eval" validate:"required"`

	Tags TagsStruct `json:"tags" yaml:"tags"`
}

type TagsStruct map[string]string

type StudioModelDevice []StudioModelDeviceStruct

type StudioModelDeviceStruct struct {
	Type      string `json:"type" yaml:"type" validate:"required"`
	Series    string `json:"series" yaml:"series"`
	DeviceNum int    `json:"deviceNum" yaml:"device_num"`
	CPU       int    `json:"cpu" yaml:"cpu"`
	Memory    int    `json:"memory" yaml:"memory"`
}

type StudioModelTrainStruct struct {
	Visualization   string                    `json:"visualization" yaml:"visualization"`
	PretrainedModel string                    `json:"pretrainedModel" yaml:"pretrained_model"`
	Entry           string                    `json:"entry" yaml:"entry" validate:"required"`
	UserParams      []StudioUserParamsStruct  `json:"userParams" yaml:"user_params"`
	SysParams       []StudioSystemParamStruct `json:"sysParams" yaml:"sys_params" validate:"required"`
}

type StudioModelEvalStruct struct {
	Entry      string                    `json:"entry" yaml:"entry" validate:"required"`
	UserParams []StudioUserParamsStruct  `json:"userParams" yaml:"user_params"`
	SysParams  []StudioSystemParamStruct `json:"sysParams" yaml:"sys_params" validate:"required"`
}

type StudioModelDataset []StudioModelDatasetStruct

type StudioModelDatasetStruct struct {
	Usage            string   `json:"usage" yaml:"usage"`
	FileFormat       string   `json:"fileFormat" yaml:"file_format"`
	Field            string   `json:"field" yaml:"field" validate:"required"`
	Task             []string `json:"task" yaml:"task" validate:"required"`
	AnnotationFormat string   `json:"annotationFormat" yaml:"annotation_format"`
	Name             string   `json:"name" yaml:"name" validate:"required"`
	Path             string   `json:"path" yaml:"path"`
}

type StudioPropsArrayStruct []StudioPropsStruct

type StudioPropsStruct struct {
	Key   string `json:"key" yaml:"key"`
	Type  string `json:"type" yaml:"type"`
	Value string `json:"value" yaml:"value"`
	Desc  string `json:"desc" yaml:"desc"`
}

type StudioUserParamsStruct struct {
	Name     string `json:"name" yaml:"name"`
	Type     string `json:"type" yaml:"type"`
	Default  string `json:"default" yaml:"default"`
	Required bool   `json:"required" yaml:"required"`
	Desc     string `json:"desc" yaml:"desc"`
	Editable bool   `json:"editable" yaml:"editable"`
}

type StudioSystemParamStruct struct {
	Name string `json:"name" yaml:"name"`
	Desc string `json:"desc" yaml:"desc"`
	Arg  string `json:"arg" yaml:"arg"`
}

func (target StudioPropsArrayStruct) Value() (driver.Value, error) {
	result, err := json.Marshal(target)
	if err != nil {
		return nil, err
	}
	return result, nil
}

func (target *StudioPropsArrayStruct) Scan(data interface{}) error {
	return json.Unmarshal(data.([]byte), &target)
}

func (target StudioModelDataset) Value() (driver.Value, error) {
	result, err := json.Marshal(target)
	if err != nil {
		return nil, err
	}
	return result, nil
}

func (target *StudioModelDataset) Scan(data interface{}) error {
	return json.Unmarshal(data.([]byte), &target)
}

func (target StudioModelTrainStruct) Value() (driver.Value, error) {
	result, err := json.Marshal(target)
	if err != nil {
		return nil, err
	}
	return result, nil
}

func (target *StudioModelTrainStruct) Scan(data interface{}) error {
	return json.Unmarshal(data.([]byte), &target)
}

func (target StudioModelEvalStruct) Value() (driver.Value, error) {
	result, err := json.Marshal(target)
	if err != nil {
		return nil, err
	}
	return result, nil
}

func (target *StudioModelEvalStruct) Scan(data interface{}) error {
	return json.Unmarshal(data.([]byte), &target)
}

func (target StudioModelDevice) Value() (driver.Value, error) {
	result, err := json.Marshal(target)
	if err != nil {
		return nil, err
	}
	return result, nil
}

func (target *StudioModelDevice) Scan(data interface{}) error {
	return json.Unmarshal(data.([]byte), &target)
}

func (target TagsStruct) Value() (driver.Value, error) {
	result, err := json.Marshal(target)
	if err != nil {
		return nil, err
	}
	return result, nil
}

func (target *TagsStruct) Scan(data interface{}) error {
	return json.Unmarshal(data.([]byte), &target)
}
