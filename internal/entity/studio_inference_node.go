/* ******************************************************************************
* 2019 - present Contributed by Apulis Technology (Shenzhen) Co. LTD
*
* This program and the accompanying materials are made available under the
* terms of the MIT License, which is available at
* https://www.opensource.org/licenses/MIT
*
* See the NOTICE file distributed with this work for additional
* information regarding copyright ownership.
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations
* under the License.
*
* SPDX-License-Identifier: MIT
******************************************************************************/
package entity

import (
	"database/sql/driver"
	"encoding/json"
)

type StudioInferenceNode struct {
	GormModel
	InferID      int               `json:"inferId"`
	NodeType     string            `json:"nodeType"`
	Framework    string            `json:"framework" yaml:"framework" validate:"required"`
	ModelPath    string            `json:"modelPath" yaml:"model_path" validate:"required"`
	Format       string            `json:"format" yaml:"format" validate:"required"`
	Percision    string            `json:"percision" yaml:"pereision"`
	VerifyScript string            `json:"verifyScript" yaml:"verify_script"`
	Engine       string            `json:"engine" yaml:"engine"`
	InputParams  InputParamsArray  `json:"inputParams" yaml:"input_params" validate:"required"`
	OutputParams OutputParamsArray `json:"outputParams" yaml:"output_params" validate:"required"`
	Devices      StudioModelDevice `json:"devices" yaml:"devices"`
}

type OutputParamsArray []OutputParamsStruct

type OutputParamsStruct struct {
	Name string `json:"name" yaml:"name"`
	Type string `json:"type" yaml:"type"`
	Desc string `json:"desc" yaml:"desc"`
}

type InputParamsArray []InputParamsStruct

type InputParamsStruct struct {
	Name     string `json:"name" yaml:"name"`
	Type     string `json:"type" yaml:"type"`
	Required bool   `json:"required" yaml:"required"`
	Desc     string `json:"desc" yaml:"desc"`
}

// type StudioVerifyStruct struct {
// 	Input  string `json:"input" yaml:"input"`
// 	Output string `json:"output" yaml:"output"`
// }

func (target OutputParamsArray) Value() (driver.Value, error) {
	result, err := json.Marshal(target)
	if err != nil {
		return nil, err
	}
	return result, nil
}

func (target *OutputParamsArray) Scan(data interface{}) error {
	return json.Unmarshal(data.([]byte), &target)
}

func (target InputParamsArray) Value() (driver.Value, error) {
	result, err := json.Marshal(target)
	if err != nil {
		return nil, err
	}
	return result, nil
}

func (target *InputParamsArray) Scan(data interface{}) error {
	return json.Unmarshal(data.([]byte), &target)
}

// func (target StudioVerifyStruct) Value() (driver.Value, error) {
// 	result, err := json.Marshal(target)
// 	if err != nil {
// 		return nil, err
// 	}
// 	return result, nil
// }

// func (target *StudioVerifyStruct) Scan(data interface{}) error {
// 	return json.Unmarshal(data.([]byte), &target)
// }
