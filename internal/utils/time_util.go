/* ******************************************************************************
* 2019 - present Contributed by Apulis Technology (Shenzhen) Co. LTD
*
* This program and the accompanying materials are made available under the
* terms of the MIT License, which is available at
* https://www.opensource.org/licenses/MIT
*
* See the NOTICE file distributed with this work for additional
* information regarding copyright ownership.
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations
* under the License.
*
* SPDX-License-Identifier: MIT
******************************************************************************/
package utils

import (
	"apworkshop/internal/entity"
	"time"
)

const (
	TIME_LAYOUT = "2006-01-02 15:04:05"
	timezonestr = "Asia/Shanghai"
)

func IntTimeToUnixTime(tm int) entity.UnixTime {
	times := time.Unix(0, int64(tm)*int64(time.Millisecond))
	return entity.UnixTime{Time: times}
}

func ParseTimeStr(timeStr string) (time.Time, error) {
	location, err := time.LoadLocation(timezonestr)
	if err != nil {
		return time.Time{}, err
	}
	return time.ParseInLocation(TIME_LAYOUT, timeStr, location)
}

func CurrentTime() time.Time {
	location, err := time.LoadLocation(timezonestr)
	if err != nil {
		return time.Time{}
	}
	return time.Now().Local().In(location)
}
