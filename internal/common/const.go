/* ******************************************************************************
* 2019 - present Contributed by Apulis Technology (Shenzhen) Co. LTD
*
* This program and the accompanying materials are made available under the
* terms of the MIT License, which is available at
* https://www.opensource.org/licenses/MIT
*
* See the NOTICE file distributed with this work for additional
* information regarding copyright ownership.
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
* License for the specific language governing permissions and limitations
* under the License.
*
* SPDX-License-Identifier: MIT
******************************************************************************/
package common

const (
	MODEL_STORAGE_DIR   = "/data/apworkshop"
	MODEL_PACK_TEMP_DIR = "/home/apworkshop/depress"
	// the product manager changed this directory for so many f* times, I pick it up to make updating more convinent
	MANIFEST_SUB_DIR = "/code"
	TRAIN_SUB_DIR    = "/code" // sample: "/code"
)

const (
	MANIFEST_DIR_NAME            = "code"
	MANIFEST_FILE_NAME           = "manifest.yml"
	INFERENCE_MANIFEST_FILE_NAME = "inference.yml"
)

const (
	PRODUCER_HUAWEI_NAME = "Huawei"
)

const (
	INFER_CENTER_NODE_TYPE = "center"
	INFER_EDGE_NODE_TYPE   = "edge"
)

const (
	FIRST_MODEL_VERSION = "1.0"
)

const (
	MODEL_COMMIT_STATUS   = "commit"
	MODEL_ROLLBACK_STATUS = "rollback"
)

const (
	MODEL_VERSION_STATUS_PREPARE  = "prepare"
	MODEL_VERSION_STATUS_FREE     = "free"
	MODEL_VERSION_STATUS_LOCK     = "lock"
	MODEL_VERSION_STATUS_DELETING = "deleting"
)

var STUDIO_MODEL_TYPES = []string{"source", "framework", "ability", "task", "field"}

// var STUDIO_MODEL_TYPES = []string{}

// manifest.yaml validation record
var STUDIO_MODEL_PALTFORM_TYPES = map[string]bool{"AIArts": true, "DataEyes": true, "AIAuto": true, "APFlow": true}

var STUDIO_MODEL_SOURCE_TYPES = map[string]bool{"publish": true, "preset": true, "custom": true}

var STUDIO_MODEL_FRAMEWORK_TYPES = map[string]bool{"TensorFlow": true, "PyTorch": true, "MindSpore": true, "MXNet": true}

var STUDIO_MODEL_DISTRIBUTED_FRAMEWORK_TYPES = map[string]bool{"Horovod": true, "MPI": true, "Ray": true}

var STUDIO_MODEL_DEVICE_TYPES = map[string]bool{"cpu": true, "nvidia_gpu": true, "huawei_npu": true}

var STUDIO_MODEL_DEVICE_MODEL_TYPES = map[string]bool{"cpu": true, "GPU": true, "a910": true, "a310": true}

var STUDIO_MODEL_DATASETS_USAGE_TYPES = map[string]bool{"all": true, "train": true, "test": true, "validation": true}

var STUDIO_MODEL_DATASETS_FILE_FORMAT_TYPES = map[string]bool{"tfrecord": true, "images": true, "video": true, "compressed": true, "mindrecord": true, "binary": true, "numpy": true, "txt": true, "json": true, "yaml": true}

var STUDIO_MODEL_TRAIN_VISUALIZATION_TYPES = map[string]bool{"TensorBoard": true, "MindInsight": true, "NNI": true}

// infer.yaml validation record
// infer -- center infer
var STUDIO_MODEL_INFER_CENTER_FRAMEWORK_TYPES = map[string]bool{"TensorFlow": true, "PyTorch": true, "MindSpore": true, "ACL": true, "TVM": true, "TensorRT": true, "ONNX": true, "TFLite": true, "MindsporeLite": true, "OpenVINO": true}

var STUDIO_MODEL_INFER_CENTER_FORMAT_TYPES = map[string]bool{"om": true, "saved_model": true, "pkl": true, "mindir": true, "onnx": true, "air": true, "pb": true, "pbtxt": true, "h5": true}

var STUDIO_MODEL_INFER_CENTER_PERCISION_TYPES = map[string]bool{"int8": true, "float32": true, "float16": true}

var STUDIO_MODEL_INFER_CENTER_DEVICE_TYPES = map[string]bool{"cpu": true, "nvidia_gpu": true, "huawei_npu": true}

var STUDIO_MODEL_INFER_CENTER_DEVICE_MODEL_TYPES = map[string]bool{"cpu": true, "gpu": true, "a910": true, "a310": true}

// infer -- edge infer
var STUDIO_MODEL_INFER_EDGE_FRAMEWORK_TYPES = map[string]bool{"TensorFlow": true, "PyTorch": true, "MindSpore": true, "ACL": true, "TVM": true, "TensorRT": true, "ONNX": true, "TFLite": true, "MindsporeLite": true, "OpenVINO": true}

var STUDIO_MODEL_INFER_EDGE_FORMAT_TYPES = map[string]bool{"om": true, "saved_model": true, "pkl": true, "mindir": true, "onnx": true, "tflite": true, "mslite": true, "air": true, "pb": true, "pbtxt": true, "h5": true}

var STUDIO_MODEL_INFER_EDGE_PERCISION_TYPES = map[string]bool{"int8": true, "float32": true, "float16": true}

var STUDIO_MODEL_INFER_EDGE_DEVICE_TYPES = map[string]bool{"cpu": true, "nvidia_gpu": true, "huawei_npu": true}

var STUDIO_MODEL_INFER_EDGE_DEVICE_MODEL_TYPES = map[string]bool{"cpu": true, "gpu": true, "a910": true, "a310": true}

// var ValidationJsonString = "{\"endpoints\":[{\"resource\":\"*\",\"action\":\"apworkshop:model:create\",\"httpMethod\":\"post\",\"httpEndpoint\":\"/apworkshop/api/v1/studioModelPack\",\"module\":\"apworkshop\",\"desc\":\"apworkshop\"},{\"resource\":\"*\",\"action\":\"apworkshop:model:prepare\",\"httpMethod\":\"post\",\"httpEndpoint\":\"/apworkshop/api/v1/prepareModel\",\"module\":\"apworkshop\",\"desc\":\"apworkshop\"},{\"resource\":\"*\",\"action\":\"apworkshop:overview:get\",\"httpMethod\":\"get\",\"httpEndpoint\":\"/apworkshop/api/v1/studioOverview\",\"module\":\"apworkshop\",\"desc\":\"apworkshop\"},{\"resource\":\"*\",\"action\":\"apworkshop:types:get\",\"httpMethod\":\"get\",\"httpEndpoint\":\"/apworkshop/api/v1/studioGetTypes\",\"module\":\"apworkshop\",\"desc\":\"apworkshop\"},{\"resource\":\"*\",\"action\":\"apworkshop:model:list\",\"httpMethod\":\"get\",\"httpEndpoint\":\"/apworkshop/api/v1/studioModels\",\"module\":\"apworkshop\",\"desc\":\"apworkshop\"},{\"resource\":\"*\",\"action\":\"apworkshop:model:describe\",\"httpMethod\":\"get\",\"httpEndpoint\":\"/apworkshop/api/v1/studioModel/{id}\",\"module\":\"apworkshop\",\"desc\":\"apworkshop\"},{\"resource\":\"*\",\"action\":\"apworkshop:model-version:get\",\"httpMethod\":\"get\",\"httpEndpoint\":\"/apworkshop/api/v1/studioModelVersion/{id}/{id}\",\"module\":\"apworkshop\",\"desc\":\"apworkshop\"},{\"resource\":\"*\",\"action\":\"apworkshop:model-version:list\",\"httpMethod\":\"get\",\"httpEndpoint\":\"/apworkshop/api/v1/studioModelVersionsByModel/{id}\",\"module\":\"apworkshop\",\"desc\":\"apworkshop\"},{\"resource\":\"*\",\"action\":\"apworkshop:model:commit\",\"httpMethod\":\"put\",\"httpEndpoint\":\"/apworkshop/api/v1/commitModel\",\"module\":\"apworkshop\",\"desc\":\"apworkshop\"},{\"resource\":\"*\",\"action\":\"apworkshop:model-version:delete\",\"httpMethod\":\"delete\",\"httpEndpoint\":\"/apworkshop/api/v1/studioModelVersion/{id}\",\"module\":\"apworkshop\",\"desc\":\"apworkshop\"},{\"resource\":\"*\",\"action\":\"apworkshop:model:delete\",\"httpMethod\":\"delete\",\"httpEndpoint\":\"/apworkshop/api/v1/studioModel/{id}\",\"module\":\"apworkshop\",\"desc\":\"apworkshop\"}],\"policies\":{\"module\":\"apworkshop\",\"systemAdmin\":[{\"actions\":[\"apworkshop:*\"],\"effect\":\"allow\"}],\"orgAdmin\":[{\"actions\":[\"apworkshop:model:prepare\",\"apworkshop:overview:get\",\"apworkshop:types:get\",\"apworkshop:model:list\",\"apworkshop:model:describe\",\"apworkshop:model-version:get\",\"apworkshop:model-version:list\",\"apworkshop:model:commit\"],\"effect\":\"allow\"}],\"developer\":[{\"actions\":[\"apworkshop:model:prepare\",\"apworkshop:overview:get\",\"apworkshop:types:get\",\"apworkshop:model:list\",\"apworkshop:model:describe\",\"apworkshop:model-version:get\",\"apworkshop:model-version:list\",\"apworkshop:model:commit\"],\"effect\":\"allow\"}]}}"

// var ValidationJsonString = "{\"endpoints\":[{\"resource\":\"*\",\"action\":\"apworkshop:model:create\",\"httpMethod\":\"post\",\"httpEndpoint\":\"/apworkshop/api/v1/studioModelPack\",\"module\":\"apworkshop\",\"desc\":\"apworkshop\"},{\"resource\":\"*\",\"action\":\"apworkshop:model:prepare\",\"httpMethod\":\"post\",\"httpEndpoint\":\"/apworkshop/api/v1/prepareModel\",\"module\":\"apworkshop\",\"desc\":\"apworkshop\"},{\"resource\":\"*\",\"action\":\"apworkshop:overview:get\",\"httpMethod\":\"get\",\"httpEndpoint\":\"/apworkshop/api/v1/studioOverview\",\"module\":\"apworkshop\",\"desc\":\"apworkshop\"},{\"resource\":\"*\",\"action\":\"apworkshop:types:get\",\"httpMethod\":\"get\",\"httpEndpoint\":\"/apworkshop/api/v1/studioGetTypes\",\"module\":\"apworkshop\",\"desc\":\"apworkshop\"},{\"resource\":\"*\",\"action\":\"apworkshop:model:list\",\"httpMethod\":\"get\",\"httpEndpoint\":\"/apworkshop/api/v1/studioModels\",\"module\":\"apworkshop\",\"desc\":\"apworkshop\"},{\"resource\":\"*\",\"action\":\"apworkshop:model:describe\",\"httpMethod\":\"get\",\"httpEndpoint\":\"/apworkshop/api/v1/studioModel/{id}\",\"module\":\"apworkshop\",\"desc\":\"apworkshop\"},{\"resource\":\"*\",\"action\":\"apworkshop:model-version:get\",\"httpMethod\":\"get\",\"httpEndpoint\":\"/apworkshop/api/v1/studioModelVersion/{id}/{id}\",\"module\":\"apworkshop\",\"desc\":\"apworkshop\"},{\"resource\":\"*\",\"action\":\"apworkshop:model-version:list\",\"httpMethod\":\"get\",\"httpEndpoint\":\"/apworkshop/api/v1/studioModelVersionsByModel/{id}\",\"module\":\"apworkshop\",\"desc\":\"apworkshop\"},{\"resource\":\"*\",\"action\":\"apworkshop:model:commit\",\"httpMethod\":\"put\",\"httpEndpoint\":\"/apworkshop/api/v1/commitModel\",\"module\":\"apworkshop\",\"desc\":\"apworkshop\"},{\"resource\":\"*\",\"action\":\"apworkshop:model-version:delete\",\"httpMethod\":\"delete\",\"httpEndpoint\":\"/apworkshop/api/v1/studioModelVersion/{id}\",\"module\":\"apworkshop\",\"desc\":\"apworkshop\"},{\"resource\":\"*\",\"action\":\"apworkshop:model:delete\",\"httpMethod\":\"delete\",\"httpEndpoint\":\"/apworkshop/api/v1/studioModel/{id}\",\"module\":\"apworkshop\",\"desc\":\"apworkshop\"}],\"policies\":{\"module\":\"apworkshop\",\"systemAdmin\":[{\"actions\":[\"apworkshop:*\"],\"effect\":\"allow\"}],\"orgAdmin\":[{\"actions\":[\"apworkshop:model:prepare\",\"apworkshop:overview:get\",\"apworkshop:types:get\",\"apworkshop:model:list\",\"apworkshop:model:describe\",\"apworkshop:model-version:get\",\"apworkshop:model-version:list\",\"apworkshop:model:commit\",\"apworkshop:model:create\"],\"effect\":\"allow\"}],\"developer\":[{\"actions\":[\"apworkshop:model:prepare\",\"apworkshop:overview:get\",\"apworkshop:types:get\",\"apworkshop:model:list\",\"apworkshop:model:describe\",\"apworkshop:model-version:get\",\"apworkshop:model-version:list\",\"apworkshop:model:commit\",\"apworkshop:model:create\"],\"effect\":\"allow\"}]}}"

// var ValidationJsonString = "{\"endpoints\":[{\"resource\":\"*\",\"action\":\"apworkshop:model:create\",\"httpMethod\":\"post\",\"httpEndpoint\":\"/apworkshop/api/v1/studioModelPack\",\"module\":\"apworkshop\",\"desc\":\"apworkshop\"},{\"resource\":\"*\",\"action\":\"apworkshop:model:prepare\",\"httpMethod\":\"post\",\"httpEndpoint\":\"/apworkshop/api/v1/prepareModel\",\"module\":\"apworkshop\",\"desc\":\"apworkshop\"},{\"resource\":\"*\",\"action\":\"apworkshop:overview:get\",\"httpMethod\":\"get\",\"httpEndpoint\":\"/apworkshop/api/v1/studioOverview\",\"module\":\"apworkshop\",\"desc\":\"apworkshop\"},{\"resource\":\"*\",\"action\":\"apworkshop:types:get\",\"httpMethod\":\"get\",\"httpEndpoint\":\"/apworkshop/api/v1/studioGetTypes\",\"module\":\"apworkshop\",\"desc\":\"apworkshop\"},{\"resource\":\"*\",\"action\":\"apworkshop:model:list\",\"httpMethod\":\"get\",\"httpEndpoint\":\"/apworkshop/api/v1/studioModels\",\"module\":\"apworkshop\",\"desc\":\"apworkshop\"},{\"resource\":\"*\",\"action\":\"apworkshop:model:describe\",\"httpMethod\":\"get\",\"httpEndpoint\":\"/apworkshop/api/v1/studioModel/{id}\",\"module\":\"apworkshop\",\"desc\":\"apworkshop\"},{\"resource\":\"*\",\"action\":\"apworkshop:model-version:get\",\"httpMethod\":\"get\",\"httpEndpoint\":\"/apworkshop/api/v1/studioModelVersion/{id}/{id}\",\"module\":\"apworkshop\",\"desc\":\"apworkshop\"},{\"resource\":\"*\",\"action\":\"apworkshop:model-version:list\",\"httpMethod\":\"get\",\"httpEndpoint\":\"/apworkshop/api/v1/studioModelVersionsByModel/{id}\",\"module\":\"apworkshop\",\"desc\":\"apworkshop\"},{\"resource\":\"*\",\"action\":\"apworkshop:model:commit\",\"httpMethod\":\"put\",\"httpEndpoint\":\"/apworkshop/api/v1/commitModel\",\"module\":\"apworkshop\",\"desc\":\"apworkshop\"},{\"resource\":\"*\",\"action\":\"apworkshop:model-version:delete\",\"httpMethod\":\"delete\",\"httpEndpoint\":\"/apworkshop/api/v1/studioModelVersion/{id}\",\"module\":\"apworkshop\",\"desc\":\"apworkshop\"},{\"resource\":\"*\",\"action\":\"apworkshop:model:delete\",\"httpMethod\":\"delete\",\"httpEndpoint\":\"/apworkshop/api/v1/studioModel/{id}\",\"module\":\"apworkshop\",\"desc\":\"apworkshop\"},{\"resource\":\"*\",\"action\":\"apworkshop:model-version-inference:get\",\"httpMethod\":\"get\",\"httpEndpoint\":\"/apworkshop/api/v1/studioModelVersionInference/{id}/{id}\",\"module\":\"apworkshop\",\"desc\":\"apworkshop\"}],\"policies\":{\"module\":\"apworkshop\",\"systemAdmin\":[{\"actions\":[\"apworkshop:*\"],\"effect\":\"allow\"}],\"orgAdmin\":[{\"actions\":[\"apworkshop:model:prepare\",\"apworkshop:overview:get\",\"apworkshop:types:get\",\"apworkshop:model:list\",\"apworkshop:model:describe\",\"apworkshop:model-version:get\",\"apworkshop:model-version:list\",\"apworkshop:model:commit\",\"apworkshop:model:create\",\"apworkshop:model-version-inference:get\"],\"effect\":\"allow\"}],\"developer\":[{\"actions\":[\"apworkshop:model:prepare\",\"apworkshop:overview:get\",\"apworkshop:types:get\",\"apworkshop:model:list\",\"apworkshop:model:describe\",\"apworkshop:model-version:get\",\"apworkshop:model-version:list\",\"apworkshop:model:commit\",\"apworkshop:model-version-inference:get\"],\"effect\":\"allow\"}]}}"

var ValidationJsonString = "{\"module\":{\"account\":\"apworkshop\",\"moduleName\":\"APWowkshop\",\"desc\":\"\u6a21\u578b\u5de5\u5382\"},\"endpoints\":[{\"resource\":\"*\",\"action\":\"apworkshop:model:create\",\"httpMethod\":\"post\",\"httpEndpoint\":\"/apworkshop/api/v1/studioModelPack\",\"module\":\"apworkshop\",\"desc\":\"\u521b\u5efa\u6a21\u578b\u4e0a\u4f20create model by upload\"},{\"resource\":\"*\",\"action\":\"apworkshop:model:prepare\",\"httpMethod\":\"post\",\"httpEndpoint\":\"/apworkshop/api/v1/prepareModel\",\"module\":\"apworkshop\",\"desc\":\"\u51c6\u5907\u6a21\u578b\u4e0a\u4f20preparing model upload\"},{\"resource\":\"*\",\"action\":\"apworkshop:overview:get\",\"httpMethod\":\"get\",\"httpEndpoint\":\"/apworkshop/api/v1/studioOverview\",\"module\":\"apworkshop\",\"desc\":\"\u83b7\u53d6\u6a21\u578b\u603b\u89c8get studio model overview\"},{\"resource\":\"*\",\"action\":\"apworkshop:types:all\",\"httpMethod\":\"get\",\"httpEndpoint\":\"/apworkshop/api/v1/studioGetRelatedTypes\",\"module\":\"apworkshop\",\"desc\":\"\u83b7\u53d6\u6a21\u578b\u7c7b\u522bget studio model all types\"},{\"resource\":\"*\",\"action\":\"apworkshop:types:get\",\"httpMethod\":\"get\",\"httpEndpoint\":\"/apworkshop/api/v1/studioGetRelatedTypes\",\"module\":\"apworkshop\",\"desc\":\"\u83b7\u53d6\u6a21\u578b\u8054\u52a8\u7c7b\u522bget studio model related types\"},{\"resource\":\"*\",\"action\":\"apworkshop:model:list\",\"httpMethod\":\"get\",\"httpEndpoint\":\"/apworkshop/api/v1/studioModels\",\"module\":\"apworkshop\",\"desc\":\"\u83b7\u53d6\u6a21\u578b\u5217\u8868get model list\"},{\"resource\":\"*\",\"action\":\"apworkshop:model:describe\",\"httpMethod\":\"get\",\"httpEndpoint\":\"/apworkshop/api/v1/studioModel/{id}\",\"module\":\"apworkshop\",\"desc\":\"\u83b7\u53d6\u6a21\u578b\u8be6\u60c5get specific model information\"},{\"resource\":\"*\",\"action\":\"apworkshop:model-version:get\",\"httpMethod\":\"get\",\"httpEndpoint\":\"/apworkshop/api/v1/studioModelVersion/{id}/{id}\",\"module\":\"apworkshop\",\"desc\":\"\u83b7\u53d6\u6a21\u578b\u7248\u672cget specific studio model version\"},{\"resource\":\"*\",\"action\":\"apworkshop:model-version:list\",\"httpMethod\":\"get\",\"httpEndpoint\":\"/apworkshop/api/v1/studioModelVersionsByModel/{id}\",\"module\":\"apworkshop\",\"desc\":\"\u83b7\u53d6\u6a21\u578b\u7248\u672c\u5217\u8868get studio model version list\"},{\"resource\":\"*\",\"action\":\"apworkshop:model:commit\",\"httpMethod\":\"put\",\"httpEndpoint\":\"/apworkshop/api/v1/commitModel\",\"module\":\"apworkshop\",\"desc\":\"\u786e\u8ba4\u4e0a\u4f20\u51c6\u5907\u4e2d\u7684\u6a21\u578bcommit a prepared model\"},{\"resource\":\"*\",\"action\":\"apworkshop:model-version:delete\",\"httpMethod\":\"delete\",\"httpEndpoint\":\"/apworkshop/api/v1/studioModelVersion/{id}\",\"module\":\"apworkshop\",\"desc\":\"\u5220\u9664\u6a21\u578b\u7248\u672cdelete a model version\"},{\"resource\":\"*\",\"action\":\"apworkshop:model:delete\",\"httpMethod\":\"delete\",\"httpEndpoint\":\"/apworkshop/api/v1/studioModel/{id}\",\"module\":\"apworkshop\",\"desc\":\"\u5220\u9664\u6a21\u578bdelete a model\"},{\"resource\":\"*\",\"action\":\"apworkshop:model-version-inference:get\",\"httpMethod\":\"get\",\"httpEndpoint\":\"/apworkshop/api/v1/studioModelVersionInference/{id}/{id}\",\"module\":\"apworkshop\",\"desc\":\"\u83b7\u53d6\u6a21\u578b\u63a8\u7406\u4fe1\u606fget studio model inference information\"}],\"policies\":{\"module\":\"apworkshop\",\"systemAdmin\":[{\"actions\":[\"apworkshop:*\"],\"effect\":\"allow\"}],\"orgAdmin\":[{\"actions\":[\"apworkshop:model:prepare\",\"apworkshop:overview:get\",\"apworkshop:types:all\",\"apworkshop:types:get\",\"apworkshop:model:list\",\"apworkshop:model:describe\",\"apworkshop:model-version:get\",\"apworkshop:model-version:list\",\"apworkshop:model:commit\",\"apworkshop:model:create\",\"apworkshop:model-version-inference:get\"],\"effect\":\"allow\"}],\"developer\":[{\"actions\":[\"apworkshop:model:prepare\",\"apworkshop:overview:get\",\"apworkshop:types:all\",\"apworkshop:types:get\",\"apworkshop:model:list\",\"apworkshop:model:describe\",\"apworkshop:model-version:get\",\"apworkshop:model-version:list\",\"apworkshop:model:commit\",\"apworkshop:model-version-inference:get\"],\"effect\":\"allow\"}],\"annotator\":[{\"actions\":[\"apworkshop:model:prepare\",\"apworkshop:overview:get\",\"apworkshop:types:all\",\"apworkshop:types:get\",\"apworkshop:model:list\",\"apworkshop:model:describe\",\"apworkshop:model-version:get\",\"apworkshop:model-version:list\",\"apworkshop:model:commit\",\"apworkshop:model-version-inference:get\"],\"effect\":\"allow\"}],\"public\":[{\"actions\":[\"apworkshop:model:prepare\",\"apworkshop:overview:get\",\"apworkshop:types:all\",\"apworkshop:types:get\",\"apworkshop:model:list\",\"apworkshop:model:describe\",\"apworkshop:model-version:get\",\"apworkshop:model-version:list\",\"apworkshop:model:commit\",\"apworkshop:model-version-inference:get\"],\"effect\":\"allow\"}]}}"

const (
	JOB_TYPE_MODEL_TRANSFORMER = "model-transformer"
)

const (
	JOB_STATUS_UNAPPROVE        = "unapprove"
	JOB_STATUS_QUEUEING         = "queueing"
	JOB_STATUS_SCHEDULING       = "scheduling"
	JOB_STATUS_RUNNING          = "running"
	JOB_STATUS_FINISHED         = "finish"
	JOB_STATUS_ERROR            = "error"
	JOB_STATUS_DELETE_JOB_ERROR = "deletejoberror"
)
