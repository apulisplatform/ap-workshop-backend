get-deps:
	git submodule init
	git submodule update
	go mod vendor

vet-check-all: get-deps
	go vet ./...

gosec-check-all: get-deps
	gosec ./...

build-bin: get-deps
	go build -v -buildmode=pie -o bin/apworkshop cmd/main.go cmd/apworshop.go
	strip bin/apworkshop

build-image: get-deps
	docker build -f build/Dockerfile . -t harbor.apulis.cn:8443/huawei630/apulistech/ap-workshop-backend:v0.1.0

gen-swagger:
	swag init -g cmd/main.go -o api

 # Regenerates OPA data from rego files
HAVE_GO_BINDATA := $(shell command -v go-bindata 2> /dev/null)
.PHONY: generate
generate:
ifndef HAVE_GO_BINDATA
	@echo "requires 'go-bindata' (go get -u github.com/kevinburke/go-bindata/go-bindata)"
	@exit 1 # fail
else
	go generate ./...
endif
